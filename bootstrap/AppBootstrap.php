<?php

namespace app\bootstrap;

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use Yii;
use yii\helpers\Html;


/**
 * Class AppBootstrap
 * @package app\bootstrap
 */
class AppBootstrap implements \yii\base\BootstrapInterface
{

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \yii\base\Application $app the application currently running
     */
    public function bootstrap($app)
    {
        Yii::$container->set(GridView::className(), [
            'containerOptions' => ['style' => 'width: 99%;'],
            'responsive' => true,
        ]);
//        <i data-feather="eye" class="wd-20 mg-r-5"></i>
        Yii::$container->set(ActionColumn::class, [
            'buttons' => [
                'update' => function($url, $model, $key){
                    return Html::a('<i data-feather="edit" class="wd-20 mg-r-5"></i>', $url, ['data-pjax' => '0']);
                },
                'view' => function($url, $model, $key){
                    // return Html::a('<i data-feather="eye" class="wd-20 mg-r-5"></i>', $url, ['role' => 'modal-remote']);
                },
                'delete' => function($url, $model, $key){
                    return Html::a('<i data-feather="trash" class="wd-20 mg-r-5"></i>', $url, ['role' => 'modal-remote', 'title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию']);
                },
            ],
        ]);
    }
}