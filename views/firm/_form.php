<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;
use app\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Firm */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить фирму";
} else {
    $this->title = "Изменить фирму";
}


?>
<div class="firm-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
                                         <div id="div-logo" class="col-md-12" style=" ">            
             <?= $form->field($model, 'fileLogo')->widget(FileInput::class, [
                'previewPath' => $model->logo,
             ])  ?>
        </div>
        <div id="div-name" class="col-md-6" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name_en')->textInput()  ?>
        </div>
        <div id="div-licenziya" class="col-md-4" style=" ">        
             <?= $form->field($model, 'licenziya')->textInput()  ?>
        </div>
        <div id="div-organizaciya" class="col-md-4" style=" ">        
             <?= $form->field($model, 'organizaciya')->textInput()  ?>
        </div>
        <div id="div-create-at" class="col-md-4" style=" ">        
             <?= $form->field($model, 'create_at')->input('date')  ?>
        </div>
        <div id="div-srok" class="col-md-4" style=" ">        
             <?= $form->field($model, 'srok')->textInput()  ?>
        </div>
        <div id="div-address" class="col-md-4" style=" ">        
             <?= $form->field($model, 'address')->textInput()  ?>
        </div>
        <div id="div-tel" class="col-md-4" style=" ">        
             <?= $form->field($model, 'tel')->textInput()  ?>
        </div>
        <div id="div-sayt" class="col-md-12" style=" ">        
             <?= $form->field($model, 'sayt')->textInput()  ?>
        </div>
        <div id="div-license" class="col-md-4" style=" ">        
             <?= $form->field($model, 'license')->textInput()  ?>
        </div>
        <div id="div-date" class="col-md-4" style=" ">        
             <?= $form->field($model, 'date')->textInput()  ?>
        </div>
        <div id="div-adress" class="col-md-4" style=" ">        
             <?= $form->field($model, 'adress')->textInput()  ?>
        </div>
        <div id="div-phone" class="col-md-4" style=" ">        
             <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999-99-99'
             ])->label('Телефон')  ?>
        </div>
        <div id="div-site" class="col-md-12" style=" ">        
             <?= $form->field($model, 'site')->textInput()  ?>
        </div>
        <div id="div-pechat" class="col-md-12" style=" ">            
             <?= $form->field($model, 'filePechat')->widget(FileInput::class, [
                'previewPath' => $model->pechat,
             ])  ?>
        </div>
        <div id="div-pechat-kvitanciyu" class="col-md-12" style=" ">            
             <?= $form->field($model, 'filePechatKvitanciyu')->widget(FileInput::class, [
                'previewPath' => $model->pechat_kvitanciyu,
             ])  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

