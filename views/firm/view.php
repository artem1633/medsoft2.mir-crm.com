<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Firm */
?>
<div class="firm-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'logo',
            'name',
            'licenziya',
            'organizaciya',
            'create_at',
            'srok',
            'address',
            'tel',
            'sayt',
            'license',
            'date',
            'name',
            'adress',
            'phone',
            'site',
            'pechat',
            'pechat_kvitanciyu',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/help/index.php", [
            'searchModel' => $helpSearchModel,
            'dataProvider' => $helpDataProvider,
            'additionalLinkParams' => ['Help[firm_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/branches/index.php", [
            'searchModel' => $branchesSearchModel,
            'dataProvider' => $branchesDataProvider,
            'additionalLinkParams' => ['Branches[firm_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
