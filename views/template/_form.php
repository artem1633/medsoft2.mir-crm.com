<?php
use app\models\Template;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Template */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $this->title = "Добавить шаблон";
} else {
    $this->title = "Изменить шаблон";
}


?>

<div class="template-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList(Template::typeLabels()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'text')->widget(\mihaildev\ckeditor\CKEditor::class, []) ?>
        </div>
    </div>

    <div id="example-template">
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
