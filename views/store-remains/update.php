<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model StoreRemains */
?>
<div class="store-remains-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
