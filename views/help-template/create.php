<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model HelpTemplate */

?>
<div class="help-template-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
