<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
use \app\models\Products;
use \app\models\Machine;
use \app\models\Route;
use \app\models\StoreSalvage;
use \app\models\StoreSalvageItem;
use \app\models\Cell;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use \app\models\StoreRemains;
use \app\models\StoreIn;
use \app\models\StoreInItem;
use \app\models\RoutWorks;
use \app\models\LocationGroups;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 

CrudAsset::register($this);/* @var $this yii\web\View */
/* @var $model StoreData */
?>
<div class="store-data-view">
                       <div class="client-tg-view">
<style type="text/css">
	.select2{
		width: 30%!important;
	}

    .modal-dialog {
        width: 80% !important;
    }
	.panel-heading .select2-container {
		float: right;
		margin-top: -7px;
	}
	.panel-body{
		height: 300px;
	}
</style>

        <div class="col-md-6">
            <div class="panel panel-inverse">
                <div class="panel-heading" style="">
                    <h4 class="panel-title">Товары</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                    </div>
                </div>
                <div class="panel-body" style="overflow: auto;     border: 1px solid #cecece;">
                    <div class="row">
                        <div class="col-md-12">
                        	<table class="table table-bordered">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Кол-во</th>
							      <th scope="col">Сумма</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <th scope="row">На складе</th>
							      <td>
							      	<?php $StoreCount = 0; $StoreSum = 0;
								      foreach (StoreRemains::find()->all() as $value) {
								      		$StoreCount = $StoreCount + $value->amount;
								       		$StoreSum = $StoreSum + $value->amount * $value->price;
								       }  echo $StoreCount;
								     ?>
							      </td>
							      <td><?= $StoreSum;?></td>
							    </tr>
							    <tr>
							      <th scope="row">В пути</th>
							      <td>
								     <?php $goStoreCount = 0;$goStoreSum = 0;
								      foreach (StoreIn::find()->where(['status' => 'В движение'])->all() as $value) {
								      	foreach (StoreInItem::find()->where(['store_in' => $value->id])->all() as $item) {
								      		$goStoreCount = $goStoreCount + $item->amount;
								       		$goStoreSum = $goStoreSum + $item->amount * $item->the_cost;
								      	}
								       	
								       }  echo $goStoreCount;
								     ?>
							     </td>
							      <td><?= $goStoreSum;?></td>
							    </tr>
							    <tr>
							      <th scope="row">Пред заказ</th>
							      <td>
								     <?php $infoStoreCount = 0;$infoStoreSum = 0;
								      foreach (StoreIn::find()->where(['status' => 'Предзаказ'])->all() as $value) {
								      	foreach (StoreInItem::find()->where(['store_in' => $value->id])->all() as $item) {
								      		$infoStoreCount = $infoStoreCount + $item->amount;
								       		$infoStoreSum = $infoStoreSum + $item->amount * $item->the_cost;
								      	}
								       	
								       }  echo $infoStoreCount;
								     ?>
							      </td>
							      <td><?= $infoStoreSum;?></td>
							    </tr>
							  </tbody>
							</table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

	    


        <div class="col-md-6">
            <div class="panel panel-inverse">

                <div class="panel-heading" style="">

                    <h4 class="panel-title float left"> Заказы</h4>


                    <div class="panel-heading-btn" style="margin-top: -20px;">
                    </div>
                </div>

                <div class="panel-body" style="overflow: auto;     border: 1px solid #cecece;">
                    <div class="row">
                        <div class="col-md-12">
                        	<table class="table table-bordered">
							  <thead>
							    <tr>
							      <th scope="col">№</th>
							      <th scope="col">Поставщик</th>
							      <th scope="col">Позиций</th>
							      <th scope="col">Статус</th>
							      <th scope="col">Дата создания</th>
							    </tr>
							  </thead>
							  <tbody>
							    <?php 
								    	foreach (StoreIn::find()->where(['!=','status','Доставлено'])->orderBy(['id' => SORT_DESC])->limit(20)->all() as $value):?>
							    <tr>
									<th scope="row"><?=$value->nomer_zakaza?></th>
								    <td><?=$value->the_supplier_id ? $value->theSupplier->name : ''?></td>
								    <td><?=StoreInItem::find()->where(['store_in' => $value->id])->count()?></td>
								    <td><?=$value->status?></td>
								    <td><?=$value->create_at?></td>
							    </tr>
							    <?php endforeach;?>
							  </tbody>
							</table>
                        </div>
                    </div>

                </div>
            </div>
        </div>




	    <?php Pjax::begin(['id' => 'pj2']); ?>
        <div class="col-md-6">
            <div class="panel panel-inverse">

                <div class="panel-heading" style="">

                	<?php  $form = ActiveForm::begin(['id' => 'search-form2', 'method' => 'POST','options' => ['data-pjax' => true]]); ?>
                    <h4 class="panel-title float left"> Таблица товаров <?= \kartik\select2\Select2::widget([
													    'name' => 'all_products',
													    'options' => ['placeholder' => 'Выберите продукт'],
													    'value' => isset($_POST['all_products']) ? $_POST['all_products'] : '',
													    'data' => ArrayHelper::map(Products::find()->all(),'id','name'),
													]);?></h4>
					<?php ActiveForm::end() ?>


                    <div class="panel-heading-btn" style="margin-top: -20px;">
                    </div>
                </div>

                <div class="panel-body" style="overflow: auto;     border: 1px solid #cecece;">
                    <div class="row">
                        <div class="col-md-12">
                        	<table class="table table-bordered">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Склад</th>
							      <th scope="col">Транзит</th>
							      <th scope="col">Общее</th>
							    </tr>
							  </thead>
							  <tbody>
							    <?php 
							    	$ascDate = '';
							    	$ascCount = '';
							    	$ascStatus = '';
							    	  $pr = Products::findOne(isset($_POST['all_products']) ? $_POST['all_products'] : '');
								      $storeRemains = StoreRemains::find()->where(['tovar_id' => isset($_POST['all_products']) ? $_POST['all_products'] : ''])->one();
								      $goStoreCount = 0;$goStoreSum = 0;
								      foreach (StoreIn::find()->where(['!=','status','Доставлено'])->orderBy(['id' => SORT_ASC])->all() as $value) {
								      	foreach (StoreInItem::find()->where(['store_in' => $value->id])->andWhere(['tovar_id' => isset($_POST['all_products']) ? $_POST['all_products'] : ''])->all() as $item) {
								      		$ascDate = $value->create_at ;
								      		$ascCount = $item->amount ;
								      		$ascStatus = $value->status ;
								      		$goStoreCount = $goStoreCount + $item->amount;
								       		$goStoreSum = $goStoreSum + $item->amount * $item->the_cost;
								      	}
								       	
								       }  

						    	?>
							    <tr>
									<th scope="row">Кол-во</th>
								    <td><?=$storeRemains ? $storeRemains->amount : 0?></td>
								    <td><?=$goStoreCount?></td>
								    <td><?=$storeRemains ? $storeRemains->amount : 0 + $goStoreCount  ?></td>
							    </tr>
							    <tr>
									<th scope="row">Стоимость</th>
								    <td><?=$storeRemains ? $storeRemains->amount * $storeRemains->price : 0?></td>
								    <td><?=$goStoreSum?></td>
								    <td><?=$storeRemains ? $storeRemains->amount * $storeRemains->price : 0 + $goStoreSum  ?></td>
							    </tr>
							  </tbody>
							</table>
							<p>Критическое значание: <?=$pr ? $pr->kriticheskoe_kol : ''?></p>
							<p>Последний заказ: Дата: <?=$ascDate?>  /  Кол-во: <?=$ascCount?> /  Статус: <?=$ascStatus?> </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

<?php

$script = <<< JS

$('#search-form2 select').change(function(){
    $('#search-form2').submit();
});


JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
	    <?php Pjax::end(); ?>


	    <div class="col-md-6">
            <div class="panel panel-inverse">

                <div class="panel-heading" style="">

                    <h4 class="panel-title float left"> Остатки товаров</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                    </div>
                </div>

                <div class="panel-body" style="overflow: auto;     border: 1px solid #cecece;">
                    <div class="row">
                        <div class="col-md-12">
                        	<table class="table table-bordered">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Критич зн</th>
							      <th scope="col">Место</th>
							      <th scope="col"></th>
							    </tr>
							  </thead>
							  <tbody>
							  	<?php $StoreCount = 0; $StoreSum = 0;
								      foreach (StoreRemains::find()->all() as $value):
								      		$StoreCount = $StoreCount + $value->amount;
								       		$StoreSum = $StoreSum + $value->amount * $value->price;
								     ?>
							    <tr>
									<th scope="row"><?=$value->tovar->name?></th>
								    <td><?=$value->tovar ? $value->tovar->kriticheskoe_kol : 0?></td>
								    <td><?=$value->mesto_na_sklade ? $value->mestoNaSklade->name : ''?></td>
								    <td><?=Html::a('<i class="fa fa-plus"></i>', ['/store-in/create'],
                        ['role'=>'modal-remote','title'=>  Yii::t('app', "Добавить"),'class'=>'btn btn-success'])?></td>
							    </tr>
							    <?php endforeach;?>
							  </tbody>
							</table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
    </div>
</div>
