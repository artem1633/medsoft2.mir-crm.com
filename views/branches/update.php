<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Branches */
?>
<div class="branches-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
