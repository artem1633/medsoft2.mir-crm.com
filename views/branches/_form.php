<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;
use app\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Branches */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить филиал";
} else {
    $this->title = "Изменить филиал";
}


?>
<div class="branches-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div id="div-logo" class="col-md-12" style=" ">            
             <?= $form->field($model, 'fileLogo')->widget(FileInput::class, [
                'previewPath' => $model->logo,
             ])  ?>
        </div>
        <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-tel-1" class="col-md-12" style=" ">        
             <?= $form->field($model, 'tel_1')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999-99-99'
             ])  ?>
        </div>
        <div id="div-tel-2" class="col-md-12" style=" ">        
             <?= $form->field($model, 'tel_2')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999-99-99'
             ])  ?>
        </div>
        <div id="div-address" class="col-md-12" style=" ">        
             <?= $form->field($model, 'address')->textInput()  ?>
        </div>
        <div id="div-firm-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'firm_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Firm::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-grafik-raboty" class="col-md-12" style=" ">        
             <?= $form->field($model, 'grafik_raboty')->textInput()  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

