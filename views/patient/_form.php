<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Patient */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
    $this->title = "Изменить пациента";
} else {
    $this->title = "Добавить пациента";
}



?>
<div class="patient-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item"><a href="#">Пациенты</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Создать</li>
                </ol>
              </nav>
              <h4 class="mg-b-0 tx-spacing--1">Добавить пациента</h4>
            </div>
          </div>

    <div class="row">
        <div class="col-md-12">
            <fieldset class="form-fieldset form-group">
                <legend>Информация</legend>
    <div class="row">
                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'last_name')->textInput()  ?>
        </div>
                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'patronymic')->textInput()  ?>
        </div>

                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name_en')->textInput()  ?>
        </div>
        <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'passport')->textInput()  ?>
        </div>
        <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'address')->textInput()  ?>
        </div>
        <div id="div-pol" class="col-md-12" style=" ">        
         <?= $form->field($model, 'pol')->dropDownList(app\models\Patient::polLabels(), ['prompt' => 'Выберите вариант']) ?>
      
        </div>
        <div id="div-god-rojdeniya" class="col-md-12" style=" ">        
             <?= $form->field($model, 'god_rojdeniya')->input('date')  ?>
        </div>
        <div id="div-phone" class="col-md-12" style=" ">        
             <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999-99-99'
             ])  ?>
        </div>
        <div id="div-posledniy-vizit" class="col-md-12" style="display: none;">          
             <?= $form->field($model, 'posledniy_vizit')->input('datetime-local')  ?>
        </div>
        <div id="div-branche-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'branche_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
    </div>

            </fieldset>
        </div>
    </div>



	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

