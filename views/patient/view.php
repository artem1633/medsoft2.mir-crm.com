<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Patient */
?>
<div class="patient-view">
    <div class="row">
        <div class="col-md-12">

        <a href="#" class="btn btn-success" onclick="event.preventDefault(); $('#detail-view').slideToggle();">Информация</a>

        <div id="detail-view" style="display: none;">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
            'pol',
            'god_rojdeniya',
            'phone',
            'posledniy_vizit',
            'zaregistrirovan',
            'branche_id',
                                ],
                            ]) ?>            
        </div>

        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/help/index.php", [
            'searchModel' => $helpSearchModel,
            'dataProvider' => $helpDataProvider,
            'additionalLinkParams' => ['Help[patient_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
