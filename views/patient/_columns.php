<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],



   
 
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'name',
         'visible'=>1,
    ],
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'last_name',
         'visible'=>1,
    ],
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'patronymic',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pol',
        'visible'=>1,
        'filter'=> \app\models\Patient::polLabels(),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'god_rojdeniya',
        'visible'=>1,
        'format'=> ['date', 'php:d.m.Y'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'phone',
         'visible'=>1,
    ],
    // [
    //      'class'=>'\kartik\grid\DataColumn',
    //     'filterType' => GridView::FILTER_DATE_RANGE,
    //     'filterWidgetOptions' => [
    //       'convertFormat'=>true,
    //       'pluginEvents' => [
    //           'cancel.daterangepicker'=>'function(ev, picker) {}'
    //        ],
    //        'pluginOptions' => [
    //           'opens'=>'right',
    //           'locale' => [
    //               'cancelLabel' => 'Clear',
    //               'format' => 'Y-m-d',
    //            ]
    //        ]
    //      ],
    //     'attribute'=>'posledniy_vizit',
    //     'visible'=>1,
    //     'format'=> ['date', 'php:d.m.Y H:i'],
    // ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'zaregistrirovan',
        'visible'=>1,
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'branche_id',
        'visible'=>1,
        'value'=>'branche.name',
        'filter'=> ArrayHelper::map(\app\models\Branches::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["patient"."/".$action,'id'=>$key]);
        },
        'template' => '{download} {update} {delete}',
        'buttons' => [
            'delete' => function($url, $model, $key){
              if(Yii::$app->user->identity->isSuperAdmin()){
                return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить', 
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    // 'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы уверены что хотите удалить эту запись'
                  ]);
              }
            },
            'update' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-pencil-alt"></i>', $url, ['target' => '_blank','data-pjax'=>'0',]);
            },
            'download' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-download"></i>', $url, ['target' => '_blank','data-pjax'=>'0',]);
            },
        ],
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['data-pjax'=>'0','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
];   

