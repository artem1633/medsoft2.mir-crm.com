<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Patient */

$this->title = 'Редактирование пацента';

?>
<div class="patient-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
