<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить пользователя";
} else {
    $this->title = "Изменить пользователя";
}

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item"><a href="#">Пользователи</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Создать</li>
                </ol>
              </nav>
              <h4 class="mg-b-0 tx-spacing--1">Добавить пользователя</h4>
            </div>
          </div>

    <div class="row">
        <div class="col-md-9">
            <fieldset class="form-fieldset form-group">
                <legend>Основаная информация</legend>
    <div class="row">
                                  <div id="div-login" class="col-md-6" style=" ">        
             <?= $form->field($model, 'login')->textInput()  ?>
        </div>
        <div id="div-role" class="col-md-6" style=" ">        
             <?= $form->field($model, 'role')->textInput()  ?>
        </div>
        <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-phone" class="col-md-12" style=" ">        
             <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7(999)999-99-99',
            ])  ?>
        </div>
        <div id="div-access" class="col-md-12" style=" ">      
             <?= $form->field($model, 'access')->checkbox()  ?>
        </div>
        <div id="div-pay-amount" class="col-md-12" style=" ">        
             <?= $form->field($model, 'pay_amount')->textInput()  ?>
        </div>
        <div id="div-branch-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'branch_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'role_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Role::find()->all(), 'id', 'name')) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip'])->label('Пароль') ?>
    </div>


    </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-fieldset form-group">
                <legend>Аватар</legend>
                <div class="row">
                    <div class="col-md-12">
                         <?php
                            echo $form->field($model, 'avatarFile')->widget(\app\widgets\FileInput::class, [
                                'previewPath' => $model->avatar, 
                            ])->label(false)
                        ?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

