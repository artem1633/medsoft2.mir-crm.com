<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Suppliers */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить поставщика";
} else {
    $this->title = "Изменить поставщика";
}


?>
<div class="suppliers-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                           <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-address" class="col-md-12" style=" ">        
             <?= $form->field($model, 'address')->textInput()  ?>
        </div>
        <div id="div-phone" class="col-md-12" style=" ">        
             <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999-99-99'
             ])  ?>
        </div>
        <div id="div-phone" class="col-md-12" style=" ">        
             <?= $form->field($model, 'site')->textInput()  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

