<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Suppliers */

?>
<div class="suppliers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
