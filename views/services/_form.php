<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить услугу";
} else {
    $this->title = "Изменить услугу";
}


?>
<div class="services-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-artikul" class="col-md-12" style=" ">        
             <?= $form->field($model, 'artikul')->textInput()  ?>
        </div>
        <div id="div-kod-804h" class="col-md-12" style=" ">        
             <?= $form->field($model, 'kod_804h')->textInput()  ?>
        </div>
        <div id="div-category-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'category_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\ServiceCategory::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-specialnost" class="col-md-12" style=" ">                
         <?= $form->field($model, 'specialnost')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Specialization::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-dlitelnost" class="col-md-12" style=" ">        
             <?= $form->field($model, 'dlitelnost')->textInput(['type' => 'number'])  ?>
        </div>
        <div id="div-price" class="col-md-12" style=" ">        
             <?= $form->field($model, 'price')->textInput(['type' => 'number'])  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

