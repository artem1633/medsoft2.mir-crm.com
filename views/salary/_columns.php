<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],



   
 
        [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible'=>\app\models\Salary::isVisibleAttr('user_id'),
        'value'=>'user.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'amounts',
        'visible' => \app\models\Salary::isVisibleAttr('amounts'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
          'convertFormat'=>true,
          'pluginEvents' => [
              'cancel.daterangepicker'=>'function(ev, picker) {}'
           ],
           'pluginOptions' => [
              'opens'=>'right',
              'locale' => [
                  'cancelLabel' => 'Clear',
                  'format' => 'Y-m-d',
               ]
           ]
         ],
        'attribute'=>'create_at',
        'visible'=>\app\models\Salary::isVisibleAttr('create_at'),
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'way',
        'visible'=>\app\models\Salary::isVisibleAttr('way'),
        'filter'=> \app\models\Salary::wayLabels(),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){
            if($model->status == \app\models\Salary::DONE){
                return '<i class="fa fa-check text-success fz-18"></i>';
            } else {
                return '<i class="fa fa-times text-danger fz-18"></i>';
            }
        },
        'hAlign' => GridView::ALIGN_CENTER,
        'visible'=>\app\models\Salary::isVisibleAttr('status'),
        'filter'=> \app\models\Salary::statusLabels(),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],

    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'branch_id',
        'visible'=>0,
        'value'=>'branch.name',
        'filter'=> ArrayHelper::map(\app\models\Branches::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["salary"."/".$action,'id'=>$key]);
        },
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
];   

