<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Salary */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить запись";
} else {
    $this->title = "Изменить запись";
}

?>
<div class="salary-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-12">
         <?= $form->field($model, 'user_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginEvents' => [
                        'change' => 'function(){ $.get("/user/view-ajax?id="+$(this).val(), function(response){ $("#salary-amounts").val(response.pay_amount); }); }'
                    ],
                ]) ?>            
        </div>
                                              

                
        <div class="col-md-12">
             <?= $form->field($model, 'amounts')->textInput(['type' => 'number'])  ?>
            
        </div>                    
    <div class="col-md-12">
         <?= $form->field($model, 'way')->dropDownList(app\models\Salary::wayLabels(), ['prompt' => 'Выберите вариант']) ?>
        
    </div>      
                    
    <div class="col-md-12">
         <?= $form->field($model, 'status')->dropDownList(app\models\Salary::statusLabels(), ['prompt' => 'Выберите вариант']) ?>
        
    </div>      
    

	<?php if (!Yii::$app->request->isAjax){ ?>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

