<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model TemplateQr */
?>
<div class="template-qr-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
