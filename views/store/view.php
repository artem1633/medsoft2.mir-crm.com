<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Store */
?>
<div class="store-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-in/index.php", [
            'searchModel' => $storeInSearchModel,
            'dataProvider' => $storeInDataProvider,
            'additionalLinkParams' => ['StoreIn[warehouse_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-remains/index.php", [
            'searchModel' => $storeRemainsSearchModel,
            'dataProvider' => $storeRemainsDataProvider,
            'additionalLinkParams' => ['StoreRemains[warehouse_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-salvage/index.php", [
            'searchModel' => $storeSalvageSearchModel,
            'dataProvider' => $storeSalvageDataProvider,
            'additionalLinkParams' => ['StoreSalvage[warehouse_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
