<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Store */

?>
<div class="store-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
