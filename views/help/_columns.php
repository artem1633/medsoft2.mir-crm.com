<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

$patients = \app\models\Patient::find()->all();

$patientsData = [];

foreach ($patients as $patient) {
	$patientsData[$patient->id] = $patient->getFio();
}

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],



 
         [
         'class'=>'\kartik\grid\DataColumn',
         'label'=>'Тестирование',
         'visible'=>0,
         'content'=> function($model){
                        },
     ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'qr',
         'visible'=>0,
         'content' => function($model){
          return "<img src='/{$model->qr}' style='height: 100px; width: 100px; object-fit: contain;'>";         }    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'patient_id',
        'visible'=>1,
        'value'=>function($model){

            $patronymic = ArrayHelper::getValue($model, 'patient.patronymic');

            if($patronymic == '-'){
              $patronymic = null;
            }

          $name = [ArrayHelper::getValue($model, 'patient.last_name'), ArrayHelper::getValue($model, 'patient.name'), $patronymic];

          return implode(' ', $name);
        },
        'filter'=> $patientsData,
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type_id',
        'visible'=>1,
        'value'=>'type.name',
        'filter'=> ArrayHelper::map(\app\models\TypesCertificates::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
          'convertFormat'=>true,
          'pluginEvents' => [
              'cancel.daterangepicker'=>'function(ev, picker) {}'
           ],
           'pluginOptions' => [
              'opens'=>'right',
              'locale' => [
                  'cancelLabel' => 'Clear',
                  'format' => 'Y-m-d',
               ]
           ]
         ],
        'attribute'=>'create_at',
        'visible'=>1,
        'label' => 'Дата',
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible'=>1,
        'value'=>'user.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'agent_id',
        'visible'=>1,
        'value'=>function($model){
        	if($model->agent_id == null){
        		return 'Клиника';
        	}

        	return ArrayHelper::getValue($model, 'agent.name');
        },
        'filter'=> ArrayHelper::map(\app\models\Agents::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    // [
    //      'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'amounts',
    //     'visible' => 1,
    //     'format' =>['decimal', 2],
    //     'pageSummary' => true, 
    // ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data_vzyatiya_biomateriala',
        'visible'=>0,
        'format'=> ['date', 'php:d.m.Y'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vremya_vzyatiya_biomateriala',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data_vydachi',
        'visible'=>0,
        'format'=> ['date', 'php:d.m.Y'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vremya_vydachi',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vrach_id',
        'visible'=>0,
        'value'=>'vrach.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'srok',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'zaklyuchenie',
        'visible'=>0,
        'value'=>'zaklyuchenie.zaklyuchenie',
        'filter'=> ArrayHelper::map(\app\models\TypesCertificates::find()->asArray()->all(), 'id', 'zaklyuchenie'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kolichestvo',
         'visible'=>0,
    ],
    // [
    //      'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'firm_id',
    //     'visible'=>1,
    //     'value'=>'firm.name',
    //     'filter'=> ArrayHelper::map(\app\models\Firm::find()->asArray()->all(), 'id', 'name'),
    //     'filterType'=> GridView::FILTER_SELECT2,
    //     'filterWidgetOptions'=> [
    //            'options' => ['prompt' => '', 'multiple' => true],
    //            'pluginOptions' => [
    //                   'allowClear' => true,
    //                   'tags' => false,
    //                   'tokenSeparators' => [','],
    //            ]
    //     ]
    // ],
    // [
    //      'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'branche_id',
    //     'visible'=>1,
    //     'value'=>'branche.name',
    //     'filter'=> ArrayHelper::map(\app\models\Branches::find()->asArray()->all(), 'id', 'name'),
    //     'filterType'=> GridView::FILTER_SELECT2,
    //     'filterWidgetOptions'=> [
    //            'options' => ['prompt' => '', 'multiple' => true],
    //            'pluginOptions' => [
    //                   'allowClear' => true,
    //                   'tags' => false,
    //                   'tokenSeparators' => [','],
    //            ]
    //     ]
    // ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'oplata',
         'content' => function($model){
            if($model->oplata){
              return "<i class='fa fa-check text-success fz-18'></i>";
            } else {
              return "<i class='fa fa-times text-danger fz-18'></i>";
            }
         },
        'filter'=> [0 => 'Нет', 1 => 'Да'],
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      // 'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ],
         'width' => "10px",
         'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
         'visible'=>1,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["help"."/".$action,'id'=>$key]);
        },
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'template' => '{download} {delete} {view} {update}',
        'buttons' => [
           'paper' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-file"></i>', 'http://medsoft.mir-crm.com/api/check/help?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
            'delete' => function($url, $model, $key){
              if(Yii::$app->user->identity->isSuperAdmin()){
                return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить', 
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    // 'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы уверены что хотите удалить эту запись'
                  ]);
              }
            },
            'qr' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-qrcode"></i>', 'http://medsoft.mir-crm.com/api/check/info?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
            'print' => function($url, $model, $key){
                // return \yii\helpers\Html::a('<i class="fa fa-print"></i>', 'http://medsoft.mir-crm.com/api/check/print?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
                return \yii\helpers\Html::a('<i class="fa fa-print"></i>', '#', ['target' => '_blank','data-pjax'=>'0',]);
            },
            'down' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-open"></i>', 'http://medsoft.mir-crm.com/api/check/down?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
            'update' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-pencil-alt"></i>', $url, ['target' => '_blank','data-pjax'=>'0',]);
            },
            'view' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, ['target' => '_blank','data-pjax'=>'0',]);
            },
            'download' => function($url, $model, $key){
                return \yii\helpers\Html::a('<i class="fa fa-download"></i>', $url, ['target' => '_blank','data-pjax'=>'0',]);
            },
        ],
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту запись?'], 
    ],      
];   

