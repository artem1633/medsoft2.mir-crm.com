<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Help */
?>
<div class="help-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelPatient' => $modelPatient,
    ]) ?>

</div>
