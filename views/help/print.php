<html>
	<head>
		<title>Результат теста: </title>
		
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;800&display=swap" rel="stylesheet">
<link rel="icon" href="https://drclinica.ru/wp-content/uploads/2021/06/cropped-logo-1-32x32.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://klinika-aspirin.ru/wp-content/uploads/2020/12/favicon.png" />
<meta name="msapplication-TileImage" content="http://klinika-aspirin.ru/wp-content/uploads/2020/12/favicon.png" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap" rel="stylesheet">

<style type="text/css">
body {background-color: #9e9e9e;
    background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1IiBoZWlnaHQ9IjUiPgo8cmVjdCB3aWR0aD0iNSIgaGVpZ2h0PSI1IiBmaWxsPSIjOWU5ZTllIj48L3JlY3Q+CjxwYXRoIGQ9Ik0wIDVMNSAwWk02IDRMNCA2Wk0tMSAxTDEgLTFaIiBzdHJva2U9IiM4ODgiIHN0cm9rZS13aWR0aD0iMSI+PC9wYXRoPgo8L3N2Zz4=);
    -webkit-transition: left 500ms;
    transition: left 500ms;
    
font-family: 'Montserrat', sans-serif; font-size: 14px;}
    
.container {position: relative; background: white; width: 676px; min-height: 900px; padding: 20px; margin: 0 auto; 
box-sizing: border-box;}
.logo {width: 220px; margin: 0 auto; padding-top: 20px;}
.logo img {width: 220px; }
.cnt {width: 100%; margin: 50px 0;}
.dhd {width: 100%;}
.dleft {width: 55%; float: left;} 
.dright {width: 40%; float: right;}
.dleft2 {width: 65%; float: left;} 
.dright2 {width: 30%; float: right; text-align: right;}
h3 {text-align: center; font-weight: 500; margin-bottom: 50px;}  
p {font-size: 12px; line-height: 15px;} 
b {font-weight: 600;}
table {width: 100%; font-size: 12px; margin: 50px 0; border-collapse: collapse; text-align: center;}
table td {border: 1px solid black; height: 30px; padding: 5px;}
.qr {position: absolute; top:50px; right: 35px;}
.check {background-color: #f7f7f7; border-color: #f7f7f7; box-shadow: 0 5px 15px #ccc;}
.check:after {content: '';
	position: relative;
    display: block;
    width: 100%;
    height: 10px;
    bottom: -10px;
    left: 0;
    background-image: linear-gradient(
45deg
, rgba(0, 0, 0, 0) 33.333%, #f7f7f7 33.333%, #f7f7f7 66.667%, rgba(0, 0, 0, 0) 66.667%), linear-gradient(
-45deg
, rgba(0, 0, 0, 0) 33.333%, #f7f7f7 33.333%, #f7f7f7 66.667%, rgba(0, 0, 0, 0) 66.667%);
    background-size: 20px 40px;
    background-position: 50% -30px;
    background-repeat: repeat-x;
    z-index: 1;}
.incheck {font-size: 12px; line-height: 17px; padding: 15px;}    
</style>
<title></title>
</head>
<body>
<?=$content?>
</body>
</html>