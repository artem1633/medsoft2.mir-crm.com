<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Help */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}


if($model->isNewRecord == false){
    $this->title = "Изменить медотвод";
} else {
    $this->title = "Добавить медотвод";
}


?>
<style>
    .field-help-oplata {
        text-align: right;
    }

    .field-help-amounts {
        position: relative;
        display: flex;
        /*flex-wrap: wrap;*/
        align-items: stretch;
        width: 100%;
    }

    .field-help-amounts input {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    .field-help-amounts .input-group-append {
        display: flex;
    }

</style>

<div class="help-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item"><a href="#">Главная</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Справки</li>
                </ol>
              </nav>
              <h4 class="mg-b-0 tx-spacing--1"><?= $this->title ?></h4>
            </div>
            <div class="d-none d-md-block">
              <button class="btn btn-sm pd-x-15 btn-white btn-uppercase"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download wd-10 mg-r-5"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg> Скачать PDF</button>
              <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-printer wd-10 mg-r-5"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg> Печать</button>
            </div>
          </div>

<div class="row">
    <div class="col-md-9">
    <ul class="nav nav-line nav-line-profile mg-b-30">
                <li class="nav-item">
                  <a href="" class="nav-link d-flex align-items-center active">Заполните данные</a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link">Проверить готовый документ</a>
                </li>
                <li class="nav-item d-none d-sm-block">
                  <a href="" class="nav-link">Проверить страницу QR</a>
                </li>
              </ul>
    <div>
        <fieldset class="form-fieldset form-group">
        <legend>Данные клиента</legend>

    <div class="row">
        <div id="div-test" class="cust-fld col-md-4" style=" ">        
                <?= $form->field($modelPatient, 'last_name')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Patient::find()->all(), 'last_name', 'last_name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginOptions' => [
                    	'tags' => true,
                    ],
                ]) ?>
        </div>
        <div id="div-test" class="cust-fld col-md-4" style=" ">        
                <?= $form->field($modelPatient, 'name')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Patient::find()->all(), 'name', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginOptions' => [
                    	'tags' => true,
                    ],
                ]) ?>
        </div>
        <div id="div-test" class="cust-fld col-md-4" style=" ">        
                <?= $form->field($modelPatient, 'patronymic')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Patient::find()->all(), 'patronymic', 'patronymic'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginOptions' => [
                    	'tags' => true,
                    ],
                ]) ?>
        </div>
        <div id="div-type-id" class="cust-fld col-md-4" style=" ">                
<?= $form->field($modelPatient, 'god_rojdeniya')->input('date') ?>
    </div>
        <div class="cust-fld col-md-4">
                <?= $form->field($model, 'vrach_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ])->label('Врач') ?>
        </div>
        <div class="cust-fld col-md-4">
                <?= $form->field($model, 'conclusion')->widget(Select2::class, [
                    'data' => [
                        1 => 'МКБ-?',
                        2 => 'МКБ-Т66',
                        3 => 'МКБ-Т78.4',
                        4 => 'Результат анализов ТТГ положительный',
                        5 => 'C76 - C80',
                        6 => 'Т78.3',
                        7 => 'Q21.3 Тетрада Фалло',
                        8 => 'L23',
                    ],
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
        </div>
        <div id="div-data-vydachi" class="cust-fld col-md-4" style=" ">        
             <?= $form->field($model, 'srok')->widget(Select2::class, [
                    'data' => [
                        'на 6 (шесть) месяцев' => 'на 6 (шесть) месяцев',
                        'на 1 (один) год' => 'на 1 (один) год',
                        'на 7 дней' => 'на 7 дней',
                        'на 14 дней' => 'на 14 дней',
                    ],
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ])  ?>
        </div>
        <div id="div-data-vydachi" class="cust-fld col-md-4" style=" ">        
             <?= $form->field($model, 'data_vydachi')->input('date')  ?>
        </div>
        <div id="div-vremya-vydachi" class="cust-fld col-md-12" style=" ">        
             <?= $form->field($model, 'has_lider')->checkbox()  ?>
        </div>

        
    </div>
            
        </fieldset>
    </div>
    </div>
    <div class="col-md-3">
    <div class="d-flex align-items-center justify-content-between mg-b-20">
                    <h6 class="tx-13 tx-spacing-1 tx-uppercase tx-semibold mg-b-0">Настройки</h6>
                  </div>
        <fieldset class="form-fieldset form-group">
            <legend>Оплата</legend>
            <?= $form->field($model, 'amounts', [
                'template' => '{label}{input}<div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">00. Руб.</span>
  </div>{hint}{error}',
            ])->textInput(['placeholder' => 'Сумма'])->label(false)  ?>
            <?= $form->field($model, 'oplata')->checkbox()->label(false)  ?>
            <?= $form->field($model, 'payment_type')->dropDownList(\app\models\Help::paymentTypeLabels())  ?>
        </fieldset>
        <fieldset class="form-fieldset form-group">
            <legend>Агент</legend>
            <?= $form->field($model, 'agent_id')->dropDownList(ArrayHelper::map(\app\models\Agents::find()->all(), 'id', 'name'), ['prompt' => 'Клиника'])->label(false)->hint('Укажите контрагента')  ?>
        </fieldset>
        <fieldset class="form-fieldset form-group">
            <legend>Фирма</legend>
            <?= $form->field($modelPatient, 'branche_id')->dropDownList(ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'name'))->label(false)->hint('Укажите фирму')  ?>
        </fieldset>
    </div>
</div>



	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<script src="/libs/jquery.maskedinput.min.js"></script>

<script>$("#help-vremya_vzyatiya_biomateriala").mask("99:99");</script>
<script>$("#help-vremya_vydachi").mask("99:99");</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->



<?php

$script = <<< JS


$("#patient-last_name, #patient-name, #patient-patronymic").change(function(){
	var lastName = $("#patient-last_name").val();
	var name = $("#patient-name").val();
	var patronymic = $("#patient-patronymic").val();

	$.get("/patient/view-ajax?name="+name+"&last_name="+lastName+"&patronymic="+patronymic, function(response){
		if(response){
			$("#patient-god_rojdeniya").val(response.god_rojdeniya);	
			$("#patient-branch_id").val(response.branch_id);	
			$("#patient-pol input[value='"+response.pol+"']").attr("checked", true);	
		}
	});
});

JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>