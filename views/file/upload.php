<?php
use app\models\File;
use app\models\FileAwait;
use kato\DropZone;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="user-form">

    <?php ActiveForm::begin() ?>

    <?= DropZone::widget([
        'id'        => 'dzImage',
        'uploadUrl' => Url::toRoute([ 'file/upload-file']),
        'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
        'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
        'clientEvents' => [
            'queuecomplete' => "function(file){
                // var value = $('[name=\"folderId\"]').val();
                // $.get('/file/await-files?folderId='+value, function(response){ $('#await-wrapper').html(response) });
            }",
        ],
        'options' => [
            'init' => new JsExpression("function() {
    this.on(\"processing\", function(file) {
        console.log($('[name=\"folderId\"]').val());
      this.options.url = '/file/upload-file?folderId='+$('[name=\"folderId\"] option:selected').val();
    });
  }"),
        ],
    ]);?>



    <div class="hidden">
        <input type="hidden" name="files">
    </div>

    <?php ActiveForm::end() ?>


</div>

