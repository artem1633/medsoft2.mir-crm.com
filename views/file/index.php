<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use kartik\dynagrid\DynaGrid;

/* @var $this yii\web\View */
/* @var $searchModel FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Файлы";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(isset($additionalLinkParams)){
    $createUrl = ArrayHelper::merge(['file/create'], $additionalLinkParams);
    $createUrl = ArrayHelper::merge($createUrl, ['display' => false]);
} else {
    $createUrl = ['file/create'];
}

?>

<style type="text/css">
    .panel-controls {
        border: 2px solid #0168fa;
        border-radius: 1.5rem;
    }

    .panel-controls .body-content {
        background: #fff;
    }

    .text-primary {
        color: #0168fa;
    }
</style>

<?php \yii\widgets\Pjax::begin(['id' => 'crud-datatable-pjax', 'enablePushState' => true]) ?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">
        
        </h4>

    </div>
    <div class="row">
            <?php foreach($dataProvider->models as $model): ?>

                <div class="col-md-3">
                    <div class="panel panel-inverse panel-controls" style="margin-top: 20px; background: #4f4f4f !important;">
                        <div class="panel-body" style="background: #4f4f4f !important; border-radius: 1.5rem !important">
                            <?php
                                $controlls = '<div class="control-panel" style="background: rgba(79, 79, 79, 0.8) !important;position: absolute;width: 100%;bottom: 0;left: 0;"><p style="font-size: 25px;margin-bottom: 0;text-align: right;">';

                            if($model->type == \app\models\File::TYPE_FILE){
                                $downloadUrl = ['file/download', 'id' => $model->id];
                            } else {
                                $downloadUrl = ['file/download-folder', 'id' => $model->id];
                            }

                                    $controlls .= Html::a('<i class="fa fa-download text-primary" style="font-size: 16px;"></i>', $downloadUrl, [
                                       'role' => 'modal-remote', 'title'=>'Скачать',
                                    ]);

                            if($model->isText() || $model->isImg()){
                                $controlls .= Html::a('<i class="fa fa-print fa-purple text-primary" style="font-size: 16px;"></i>', ['file/print', 'id' => $model->id], [
                                    'data-pjax' => 0, 'title'=>'Печать', 'target' => '_blank'
                                ]);
                            }

                            if($model->extension == 'pdf'){
                                if($model->type == \app\models\File::TYPE_FILE){
                                    $controlls .= Html::a('<i class="fa fa-eye text-primary" style="font-size: 16px;"></i>', ['view', 'id' => $model->id], [
                                        'role'=>'modal-remote', 'title'=>'Посмотреть',
                                    ]);
                                }
                            }

                            if($model->isText()){
                                $controlls .= Html::a('<i class="fa fa-eye text-primary" style="font-size: 16px;"></i>', ['view-text', 'id' => $model->id], [
                                    'role'=>'modal-remote', 'title'=>'Посмотреть',
                                ]);
                            }


                                    if($model->type == \app\models\File::TYPE_FILE){
                                        $controlls .= Html::a('<i class="fa fa-clipboard text-primary" style="font-size: 16px;"></i>', ['copy', 'id' => $model->id], [
                                            'role'=>'modal-remote', 'title'=>'Копировать',
                                        ]);
                                    }


                                    $controlls .= Html::a('<i class="fa fa-arrow-circle-left text-primary" style="font-size: 16px;"></i>', ['move', 'id' => $model->id], [
                                        'role'=>'modal-remote', 'title'=>'Переместить',
                                    ]);
                                


                                    $controlls .=  Html::a('<i class="fa fa-trash text-primary" style="font-size: 16px;"></i>', ['delete', 'id' => $model->id], [
                                        'role'=>'modal-remote', 'title'=>'Удалить',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите удалить файл?'
                                    ]);
                                

                                if($model->type == \app\models\File::TYPE_FOLDER) {
                                    $controlls .= Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', ['update', 'id' => $model->id], [
                                        'role'=>'modal-remote', 'title'=>'Изменить',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                    ]);
                                }

                            $controlls .= '</p></div>';


                            ?>
                            <?php if($model->isImg()): ?>
                                <div style="overflow: hidden; border-radius: 1.5rem; position: relative; letter-spacing: 15px;"><a class="image-popup-vertical-fit" data-name="<?=$model->name?>" href="/<?= $model->path ?>"><img src="/<?= $model->path ?>" alt="" style="width: 100%; height: 200px; object-fit: cover;"></a><?=$controlls?></div>
                            <?php elseif($model->isVideo()): ?>
                                <div style="overflow: hidden; border-radius: 1.5rem; position: relative; width: 100%; height: 200px; background: #3b3b3b !important; text-align: center; font-size: 70px; letter-spacing: 15px; color: #8f5b5b;"><span style="display: inline-block; margin-top: 9%;"><a class="video-popup-vertical-fit" data-name="<?=$model->name?>" href="/<?= $model->path ?>"><i class="fa fa-video-camera text-info"></i></a></span><?=$controlls?></div>
                            <?php elseif($model->extension == 'pdf'): ?>
                                <div style="overflow: hidden; border-radius: 1.5rem; position: relative; width: 100%; height: 200px; background: #3b3b3b !important; text-align: center; font-size: 70px; letter-spacing: 15px; color: #8f5b5b;"><span style="display: inline-block; margin-top: 9%;"><i class="fa fa-file-pdf-o"></i></span><?=$controlls?></div>
                            <?php elseif($model->extension != null): ?>
                                <div style="overflow: hidden; border-radius: 1.5rem; position: relative; width: 100%; height: 200px; background: #3b3b3b !important; text-align: center; font-size: 70px; letter-spacing: 15px; color: #4d4d4d;"><span style="display: inline-block; margin-top: 9%;"><?=mb_strtoupper($model->extension)?></span><?=$controlls?></div>
                            <?php else: ?>
                                <div style="overflow: hidden; border-radius: 1.5rem; position: relative; width: 100%; height: 200px; background: #3b3b3b !important; text-align: center; font-size: 100px; letter-spacing: 15px; color: #4d4d4d;"><span style="display: inline-block; margin-top: 9%;"><i class="fa fa-folder"></i></span><?=$controlls?></div>
                            <?php endif; ?>

                            <div class="body-content" style="padding: 13px !important; border-bottom-right-radius: 1.5rem; border-bottom-left-radius: 1.5rem;">
                                <p style="margin-bottom: 5px;font-size: 16px;text-overflow: ellipsis;width: 100%;white-space: nowrap;overflow: hidden;"><?php
                                    if($model->type == \app\models\File::TYPE_FILE){
                                        echo "{$model->name}";
                                    } else if($model->type == \app\models\File::TYPE_FOLDER) {

                                        $p = \yii\helpers\ArrayHelper::getValue($_GET, 'p').'/'.$model->name;

                                        echo Html::a("<i class='fa fa-folder'></i> {$model->name}", ['index', 'folderId' => $model->id], ['class' => 'text-warning']);
                                    }
                                    ?>
                                </p>
                                <p style="font-size: 13px; margin-bottom: 0;">
                                    <span><i class="fa fa-clock"></i> <?= Yii::$app->formatter->asDate($model->datetime, 'php:d.m.Y H:i') ?></span>
                                        <span style="float: right;"><i class="fa fa-expand"></i> <?= number_format(round($model->size / 1000000, 2), 2, '.', ' ') ?> Мбайт</span>
                                </p>
                                <?php

                                if(isset($_GET['search']) && $_GET['search'] != null){
                                    $folders = \yii\helpers\ArrayHelper::getColumn(\app\models\File::find()->where(['id' => $model->getFolderPath()])->andWhere(['!=', 'id', $model->id])->all(), 'name');
                                    echo '<p style="font-size: 13px; margin-bottom: 0; margin-top: 10px;" class="text-info"><i class=\'fa fa-search\'></i> '.implode(' / ', \yii\helpers\ArrayHelper::merge(['>'], $folders)).'</p>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
    </div>
</div>


    <?= Html::a('<i class="fa fa-folder" style="position: absolute; top: 38%; left: 37%;"></i>', ['file/add-folder'], ['role' => 'modal-remote', 'class' => 'btn btn-warning', 'style' => 'position: fixed;right: 160px;bottom: 80px;border-radius: 100%;height: 50px;width: 50px;']) ?>
    <?= Html::a('<i class="fa fa-plus" style="position: absolute; top: 38%; left: 40%;"></i>', ['file/upload'], ['role' => 'modal-remote', 'id' => 'upload-btn', 'class' => 'btn btn-success', 'style' => 'position: fixed;right: 80px;bottom: 80px;border-radius: 100%;height: 50px;width: 50px;']) ?>


<div class="hidden" style="display: none;">
    <form id="upload-form" enctype="multipart/form-data" action="<?= \yii\helpers\Url::toRoute(['file/upload']) ?>" method="post">
        <input id="file-input-control" type="file" name="file">
        <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
    </form>
</div>

<?php

$script = <<< JS
// $('#upload-btn').click(function(e){
//     e.preventDefault();
//    
//     $('#file-input-control').trigger('click');
// });
//
// $('#file-input-control').change(function(){
//     $('#upload-form').submit();
// });


$(document).ready(function() {

	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true,
			 markup: '<div class="mfp-figure">'+
            '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
              '<a class="img-download-btn" style="display: inline-block; margin-top: 5px; font-size: 15px;"><i class="fa fa-download"></i> Скачать</a>'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
            '</div>'+
          '</div>',
		},
		 closeOnContentClick: false,
		 callbacks: {
            open: function() {
              $('.img-download-btn').attr('href', this.currItem.src);
              $('.img-download-btn').attr('download', this.currItem.el.data('name'));
            },
            close: function() {
              // Will fire when popup is closed
            }
            // e.t.c.
          }
		
	});

	
    $('.video-popup-vertical-fit').magnificPopup({
		type: 'iframe',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true,
			 markup: '<div class="mfp-figure">'+
            '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
              '<a class="img-download-btn" style="display: inline-block; margin-top: 5px; font-size: 15px;"><i class="fa fa-download"></i> Скачать</a>'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
            '</div>'+
          '</div>',
		},
		 closeOnContentClick: false,
		 callbacks: {
            open: function() {
              $('.img-download-btn').attr('href', this.currItem.src);
              $('.img-download-btn').attr('download', this.currItem.el.data('name'));
            },
            close: function() {
              // Will fire when popup is closed
            }
            // e.t.c.
          }
		
	});

});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>

<?php \yii\widgets\Pjax::end() ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-wide'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
