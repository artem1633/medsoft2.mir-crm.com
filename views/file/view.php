<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model File */
?>
<div class="file-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'folder',
            'subfolder',
            'size',
            'extension',
            'datetime',
            'opisanie_sten',
            'path',
            'name',
            'type',
            'user_id',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
    

      </div>

    
</div>
