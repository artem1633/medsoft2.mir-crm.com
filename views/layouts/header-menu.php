<?php

use yii\helpers\Url;


$noPhotoPath = 'https://icon-library.com/images/no-user-image-icon/no-user-image-icon-27.jpg';

?>

<aside class="aside aside-fixed">
    <div class="aside-header">
        <a href="<?= Url::toRoute(['/patient/index']) ?>" class="aside-logo">med<span>soft</span></a>
        <a href="" class="aside-menu-link">
            <i data-feather="menu"></i>
            <i data-feather="x"></i>
        </a>
    </div>
    <div class="aside-body">
        <div class="aside-loggedin">
            <div class="d-flex align-items-center justify-content-start">
                <a href="" class="avatar"><img src="<?= Yii::$app->user->identity->avatar ? '/'.Yii::$app->user->identity->avatar : $noPhotoPath ?>" class="rounded-circle" alt=""></a>
                <div class="aside-alert-link">
                    <a href="<?= Url::toRoute(['site/logout']) ?>" data-method="post" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
                </div>
            </div>
            <div class="aside-loggedin-user">
                <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                    <h6 class="tx-semibold mg-b-0"><?= Yii::$app->user->identity->name ?></h6>
                    <i data-feather="chevron-down"></i>
                </a>
                <p class="tx-color-03 tx-12 mg-b-0"><?= \yii\helpers\ArrayHelper::getValue(Yii::$app->user->identity->role_id, \yii\helpers\ArrayHelper::map(\app\models\Role::find()->all(), 'id', 'name')) ?></p>
            </div>
            <div class="collapse" id="loggedinMenu">
                <ul class="nav nav-aside mg-b-0">
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="edit"></i> <span>Редактирование</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="log-out"></i> <span>Выход</span></a></li>
                </ul>
            </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
            <li class="nav-label">Панель управления</li>
            <li class="nav-item"><a href="<?= Url::toRoute(['doctor-office/index']) ?>" class="nav-link"><i data-feather="home"></i> <span>Кабинет врача</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['patient/index']) ?>" class="nav-link"><i data-feather="users"></i> <span>Пациенты</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['help/index']) ?>" class="nav-link"><i data-feather="file-text"></i> <span>Справки</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['box-office/index']) ?>" class="nav-link"><i data-feather="shopping-cart"></i> <span>Касса</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['salary/index']) ?>" class="nav-link"><i data-feather="dollar-sign"></i> <span>Зарплата</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['user/index']) ?>" class="nav-link"><i data-feather="user-plus"></i> <span>Пользователи</span></a></li>
            <li class="nav-item"><a href="<?= '#' ?>" class="nav-link"><i data-feather="calendar"></i> <span>Расписание</span></a></li>
            <li class="nav-item"><a href="<?= '#'  ?>" class="nav-link"><i data-feather="thermometer"></i> <span>Анализы</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['file/index']) ?>" class="nav-link"><i data-feather="paperclip"></i> <span>Файлы</span></a></li>
            <li class="nav-label mg-t-25">Создать</li>
            <li class="nav-item"><a href="<?= Url::toRoute(['help/create']) ?>" class="nav-link"><i data-feather="plus"></i> <span>ПЦР</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['help/create-out']) ?>" class="nav-link"><i data-feather="plus"></i> <span>Медотвод</span></a></li>
            <li class="nav-item"><a href="<?= '#' ?>" class="nav-link"><i data-feather="plus"></i> <span>Антитела</span></a></li>
            <li class="nav-label mg-t-25">Администрация</li>
            <li class="nav-item"><a href="<?= '#' ?>" class="nav-link"><i data-feather="pie-chart"></i> <span>Отчет</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['agents/index']) ?>" class="nav-link"><i data-feather="users"></i> <span>Агенты</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['store-manager/index']) ?>" class="nav-link"><i data-feather="archive"></i> <span>Склад</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['help-template/index']) ?>" class="nav-link"><i data-feather="file"></i> <span>Шаблоны справок</span></a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['template-qr/index']) ?>" class="nav-link"><i data-feather="file"></i> <span>Шаблоны QR</span></a></li>
        </ul>

    </div>
</aside>
