<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

//$isGuest = Yii::$app->user->isGuest;
//if($isGuest)
//    $theme = 'default';
//else
//    $theme = Yii::$app->user->identity->theme;


if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it.
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
//        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

//    app\assets\ColorAdminAsset::register($this);

    // dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <?=$this->head()?>
        <!-- ================== END BASE CSS STYLE ================== -->
        <!--        <script src="/assets/pace/pace.min.js"></script>-->
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_ru" type="text/javascript"></script>
    </head>
    <body>

    <?php $this->beginBody() ?>


    <?= $this->render(
        'header-menu.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>


    <div class="content ht-100v pd-0">
        <div class="content-header">
            <?= $this->render(
                'header.php',
                ['directoryAsset' => $directoryAsset]
            ) ?>
        </div><!-- content-header -->

        <div class="content-body">
            <div class="container pd-x-0">
<!--                <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">-->
<!--                    <div>-->
<!--                        <nav aria-label="breadcrumb">-->
<!--                            <ol class="breadcrumb breadcrumb-style1 mg-b-10">-->
<!--                                <li class="breadcrumb-item"><a href="#">Главная</a></li>-->
<!--                                <li class="breadcrumb-item active" aria-current="page">Sales Monitoring</li>-->
<!--                            </ol>-->
<!--                        </nav>-->
<!--                        <h4 class="mg-b-0 tx-spacing--1">Добрый день, Аскер!</h4>-->
<!--                    </div>-->
<!--                    <div class="d-none d-md-block">-->
<!--                        <button class="btn btn-sm pd-x-15 btn-white btn-uppercase"><i data-feather="mail" class="wd-10 mg-r-5"></i> Email</button>-->
<!--                        <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i data-feather="printer" class="wd-10 mg-r-5"></i> Печать</button>-->
<!--                        <button class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="file" class="wd-10 mg-r-5"></i> Создать отчет</button>-->
<!--                    </div>-->
<!--                </div>-->

                <?= $this->render(
                    'content.php',
                    ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>

            </div><!-- container -->
        </div>
    </div>





    <?php $this->endBody() ?>

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>



    <?= $this->render(
        'main-script.php'
    ) ?>
    <style>
        [title='Выберите формат файла для экспорта']{
            padding: 3px !important;
            padding-bottom: 2px !important;
        }
    </style>
    <script>

    </script>

    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>

