<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Products */

?>
<div class="products-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
