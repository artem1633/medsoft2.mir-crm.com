<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Products */
?>
<div class="products-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'code',
            'name',
            'edinica_zapravki',
            'artikul',
            'kriticheskoe_kol',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-in-item/index.php", [
            'searchModel' => $storeInItemSearchModel,
            'dataProvider' => $storeInItemDataProvider,
            'additionalLinkParams' => ['StoreInItem[tovar_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-remains/index.php", [
            'searchModel' => $storeRemainsSearchModel,
            'dataProvider' => $storeRemainsDataProvider,
            'additionalLinkParams' => ['StoreRemains[tovar_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-salvage-item/index.php", [
            'searchModel' => $storeSalvageItemSearchModel,
            'dataProvider' => $storeSalvageItemDataProvider,
            'additionalLinkParams' => ['StoreSalvageItem[tovar_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
