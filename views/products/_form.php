<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}

if($model->isNewRecord){
    $this->title = "Добавить продукт";
} else {
    $this->title = "Изменить продукт";
}


?>
<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                             <div id="div-name" class="col-md-6" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-edinica-zapravki" class="col-md-6" style=" ">                
         <?= $form->field($model, 'edinica_zapravki')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Ed::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-artikul" class="col-md-6" style=" ">        
             <?= $form->field($model, 'artikul')->textInput()  ?>
        </div>
        <div id="div-kriticheskoe-kol" class="col-md-6" style=" ">        
             <?= $form->field($model, 'kriticheskoe_kol')->textInput(['type' => 'number'])  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

