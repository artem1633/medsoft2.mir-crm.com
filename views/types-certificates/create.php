<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model TypesCertificates */

?>
<div class="types-certificates-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
