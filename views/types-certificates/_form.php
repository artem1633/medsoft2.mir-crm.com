<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\TypesCertificates */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
	$model->zaklyuchenie = explode(',', $model->zaklyuchenie);
}

if($model->isNewRecord){
    $this->title = "Добавить тип";
} else {
    $this->title = "Изменить тип";
}


?>
<div class="types-certificates-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-price" class="col-md-12" style=" ">        
             <?= $form->field($model, 'price')->textInput(['type' => 'number'])  ?>
        </div>
        <div id="div-agentu" class="col-md-12" style=" ">        
             <?= $form->field($model, 'agentu')->textInput(['type' => 'number'])  ?>
        </div>
        <div id="div-zaklyuchenie" class="col-md-12" style="display: none;">        
         <?= $form->field($model, 'zaklyuchenie')->widget(Select2::class, [
                'data' => [],
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'tags' => true,
                ],
        ]) ?>

            </div>
        <div id="div-shablon-qr-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'shablon_qr_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\TemplateQr::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-shablon-spravki-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'shablon_spravki_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\HelpTemplate::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

