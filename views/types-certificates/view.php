<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model TypesCertificates */
?>
<div class="types-certificates-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
            'price',
            'agentu',
            'zaklyuchenie',
            'shablon_qr_id',
            'shablon_spravki_id',
            'vozmojnost_udalyat',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/help/index.php", [
            'searchModel' => $helpSearchModel,
            'dataProvider' => $helpDataProvider,
            'additionalLinkParams' => ['Help[zaklyuchenie]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
