<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="row">
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Кабинет врача</h5>
                </div>
            </div>
            <div class="row">


            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Пациент</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'patient_name')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_pol')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_god_rojdeniya')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_phone')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_posledniy_vizit')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_zaregistrirovan')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_branche_id')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Справки</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'help_test')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_qr')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_patient_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_type_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_user_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_agent_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_amounts')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_data_vzyatiya_biomateriala')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_vremya_vzyatiya_biomateriala')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_data_vydachi')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_vremya_vydachi')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_vrach_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_srok')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_zaklyuchenie')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_kolichestvo')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_firm_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_branche_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_oplata')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Мед книжки</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_name')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_data_rojdeniya')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_city')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_ulica')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_home_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_obekt')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_role')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Управление складом</h5>
                </div>
            </div>
            <div class="row">


            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Поступления</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_nomer_zakaza')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_warehouse_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_status')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_the_supplier_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_vkladka')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_position')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Позиции поступления</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_tovar_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_amount')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_prinyato')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_the_cost')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_store_in')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_mesto_na_sklade')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Остатки</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_vkladka')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_warehouse_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_tovar_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_amount')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_price')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_mesto_na_sklade')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Сводные данные</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_vkladka')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Списание</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_warehouse_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_status')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_vkladka')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_position')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Позиции списания</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_tovar_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_mesto_na_sklade')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_amount')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_store_in')->checkbox() ?>
                </div>

            </div>
        </div>
    </div>

    <?php  if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?=  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php  } ?>

    <?php  ActiveForm::end(); ?>
</div>
