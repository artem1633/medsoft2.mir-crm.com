<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Ed */
?>
<div class="ed-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
