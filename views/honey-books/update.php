<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model HoneyBooks */
?>
<div class="honey-books-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
