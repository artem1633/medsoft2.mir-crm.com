<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model HoneyBooks */

?>
<div class="honey-books-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
