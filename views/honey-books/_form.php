<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\HoneyBooks */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="honey-books-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                               <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-data-rojdeniya" class="col-md-12" style=" ">        
             <?= $form->field($model, 'data_rojdeniya')->input('date')  ?>
        </div>
        <div id="div-city" class="col-md-12" style=" ">        
             <?= $form->field($model, 'city')->textInput()  ?>
        </div>
        <div id="div-ulica" class="col-md-12" style=" ">        
             <?= $form->field($model, 'ulica')->textInput()  ?>
        </div>
        <div id="div-home-id" class="col-md-12" style=" ">        
             <?= $form->field($model, 'home_id')->textInput()  ?>
        </div>
        <div id="div-obekt" class="col-md-12" style=" ">        
             <?= $form->field($model, 'obekt')->textInput()  ?>
        </div>
        <div id="div-role" class="col-md-12" style=" ">        
             <?= $form->field($model, 'role')->textInput()  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

