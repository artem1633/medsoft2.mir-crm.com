<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Agents */

?>
<div class="agents-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
