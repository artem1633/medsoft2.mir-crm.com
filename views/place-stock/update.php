<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model PlaceStock */
?>
<div class="place-stock-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
