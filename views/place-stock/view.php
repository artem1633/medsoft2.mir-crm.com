<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model PlaceStock */
?>
<div class="place-stock-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-in-item/index.php", [
            'searchModel' => $storeInItemSearchModel,
            'dataProvider' => $storeInItemDataProvider,
            'additionalLinkParams' => ['StoreInItem[mesto_na_sklade]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-remains/index.php", [
            'searchModel' => $storeRemainsSearchModel,
            'dataProvider' => $storeRemainsDataProvider,
            'additionalLinkParams' => ['StoreRemains[mesto_na_sklade]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/store-salvage-item/index.php", [
            'searchModel' => $storeSalvageItemSearchModel,
            'dataProvider' => $storeSalvageItemDataProvider,
            'additionalLinkParams' => ['StoreSalvageItem[mesto_na_sklade]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
