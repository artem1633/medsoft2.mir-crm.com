<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\BoxOffice */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}


$patients = \app\models\Patient::find()->all();

$patientIdData = [];

foreach ($patients as $patient) {

    $fio = [];

    if($patient->last_name){
        $fio[] = $patient->last_name;
    }

    if($patient->name){
        $fio[] = $patient->name;
    }

    if($patient->patronymic){
        $fio[] = $patient->patronymic;
    }

    $patientIdData[$patient->id] = implode(' ', $fio);
}

if($model->isNewRecord){
    $admin = \app\models\User::find()->where(['id' => 1])->one();
    if($admin){
        $model->specialist_id = $admin->id;
    }
}

if($model->isNewRecord){
    $this->title = "Добавить запись";
} else {
    $this->title = "Изменить запись";
}

?>

        <fieldset class="form-fieldset form-group">
        <legend>Данные клиента</legend>


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                   
    <div class="col-md-6">
         <?= $form->field($model, 'patient_id')->widget(Select2::class, [
                    'data' => $patientIdData,
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>        
    </div>

                
    <div class="col-md-6">
         <?= $form->field($model, 'type')->dropDownList(app\models\BoxOffice::typeLabels(), ['prompt' => 'Выберите вариант']) ?>
    </div>      
                            
        <div class="col-md-6">
            <?= $form->field($model, 'service_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Services::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginEvents' => [
                        'change' => 'function(){ $.get("/services/view-ajax?id="+$(this).val(), function(response) { $("#boxoffice-amounts").val(response.price); }); }',
                    ],
                ]) ?>
        </div>                

        <div class="col-md-6">
             <?= $form->field($model, 'amounts')->textInput(['type' => 'number'])  ?>
        </div>

<div class="col-md-6">
         <?= $form->field($model, 'specialist_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\User::find()->all(), 'id', 'name'), ['prompt' => 'Выберите вариант']) ?>
        </div>                       

        <div class="col-md-6">
         <?= $form->field($model, 'status')->dropDownList(app\models\BoxOffice::statusLabels(), ['prompt' => 'Выберите вариант']) ?>
        </div>   
        <div class="col-md-6">
         <?= $form->field($model, 'payment_type')->dropDownList(app\models\Help::paymentTypeLabels(), ['prompt' => 'Выберите вариант']) ?>
        </div>      

        <div class="col-md-6">
             <?= $form->field($model, 'comment')->textInput()  ?>
        </div>
                    
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


</fieldset>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

