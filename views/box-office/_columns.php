<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

$patientsData = [];

foreach (\app\models\Patient::find()->all() as $patient) {
    $patientsData[$patient->id] = $patient->getFio();
}

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],


 
        [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'patient_id',
          'label' => 'Пациент',
        'visible'=>\app\models\BoxOffice::isVisibleAttr('patient_id'),
        'value'=>function($model){
          $patient = \app\models\Patient::findOne($model->patient_id);


          if($patient) {
            return $patient->getFio();
          }
        },
        'filter'=> $patientsData,
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'amounts',
        // 'visible' => \app\models\BoxOffice::isVisibleAttr('amounts'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
          'convertFormat'=>true,
          'pluginEvents' => [
              'cancel.daterangepicker'=>'function(ev, picker) {}'
           ],
           'pluginOptions' => [
              'opens'=>'right',
              'locale' => [
                  'cancelLabel' => 'Clear',
                  'format' => 'Y-m-d',
               ]
           ]
         ],
        'attribute'=>'created_at',
        'label' => 'Дата и время',
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('created_at'),
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
      [
        'label' => 'Специалист',
        'attribute' => 'specialist_id',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ],
        'value' => function($model){
          if($model->specialist_id){
            $user = \app\models\User::findOne($model->specialist_id);

            if($user){
              return $user->name;
            }
          }

          return null;
        }
      ],
    // [
    //      'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'type',
    //     // 'visible'=>\app\models\BoxOffice::isVisibleAttr('type'),
    //     'filter'=> \app\models\BoxOffice::typeLabels(),
    //     'filterType'=> GridView::FILTER_SELECT2,
    //     'filterWidgetOptions'=> [
    //            'options' => ['prompt' => ''],
    //            'pluginOptions' => [
    //                   'allowClear' => true,
    //                   'tags' => false,
    //                   'tokenSeparators' => [','],
    //            ]
    //     ]
    // ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'service_id',
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('service_id'),
        // 'value'=>function($model){
        //   $help = \app\models\Help::findOne($model->help_id);

        //   if($help){
        //     if($help->type_id == 1){
        //       return "ПЦР";
        //     } elseif($help->type_id == 2){
        //       return "Медотвод";
        //     }
        //   }
        // },
        'value' => 'service.name',
        'filter'=> ArrayHelper::map(\app\models\Services::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_id',
        'label' => 'Оператор',
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('created_id'),
        'value'=>'created.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){
          if($model->status){
            return $model->status == "Оплачен" ? '<i class="fa fa-check text-success fz-18"></i>' : '<i class="fa fa-times text-danger fz-18"></i>';
          }
        },
        'hAlign' => GridView::ALIGN_CENTER,
        'filter'=> ['Оплачен' => 'Оплачен', 'Не оплачен' => 'Не оплачен'],
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'branch_id',
        'visible'=>0,
        'value'=>'branch.name',
        'filter'=> ArrayHelper::map(\app\models\Branches::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],

    [
      'attribute' => 'payment_type',
      'label' => 'Способ оплаты',
      'filter'=> \app\models\Help::paymentTypeLabels(),
      'filterType'=> GridView::FILTER_SELECT2,
      'filterWidgetOptions' => [
        'options' => ['prompt' => ''],
        'pluginOptions' => [
          'allowClear' => true,
        ],
      ],
      'value' => function($model){
        // $help = \app\models\Help::findOne($model->help_id);

        if($model){
          return ArrayHelper::getValue(\app\models\Help::paymentTypeLabels(), $model->payment_type);
        }

      }
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{download}{update}{delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["box-office"."/".$action,'id'=>$key]);
        },
        'buttons' => [
                'update' => function($url, $model, $key){
                    return Html::a('<i data-feather="edit" class="wd-20 mg-r-5"></i>', $url, ['data-pjax' => '0']);
                },
                'download' => function($url, $model, $key){
                  return Html::a('<i data-feather="download" class="wd-20 mg-r-5"></i>', $url, ['data-pjax' => '0']);
                },
                'view' => function($url, $model, $key){
                    // return Html::a('<i data-feather="eye" class="wd-20 mg-r-5"></i>', $url, ['role' => 'modal-remote']);
                },
                'delete' => function($url, $model, $key){
                    return Html::a('<i data-feather="trash" class="wd-20 mg-r-5"></i>', $url, ['role' => 'modal-remote', 'title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию']);
                },
        ],
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
   
];   

