<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

$patientsData = [];

foreach (\app\models\Patient::find()->all() as $patient) {
    $patientsData[$patient->id] = $patient->getFio();
}

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],


 
        [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'patient_id',
          'label' => 'Пациент',
        'visible'=>\app\models\BoxOffice::isVisibleAttr('patient_id'),
        'value'=>function($model){
          $patient = \app\models\Patient::findOne($model->patient_id);


          if($patient) {
            return $patient->getFio();
          }
        },
        'filter'=> $patientsData,
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'amounts',
        'label' => "Сумма",
        // 'visible' => \app\models\BoxOffice::isVisibleAttr('amounts'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
          'convertFormat'=>true,
          'pluginEvents' => [
              'cancel.daterangepicker'=>'function(ev, picker) {}'
           ],
           'pluginOptions' => [
              'opens'=>'right',
              'locale' => [
                  'cancelLabel' => 'Clear',
                  'format' => 'Y-m-d',
               ]
           ]
         ],
        'attribute'=>'created_at',
        'label' => 'Дата и время',
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('created_at'),
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
      [
        'label' => 'Специалист',
        'attribute' => 'specialist_id',
        'label' => 'Специалист',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ],
        'value' => function($model){
          if($model->specialist_id){
            $user = \app\models\User::findOne($model->specialist_id);

            if($user){
              return $user->name;
            }
          }

          return null;
        }
      ],
    // [
    //      'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'type',
    //     // 'visible'=>\app\models\BoxOffice::isVisibleAttr('type'),
    //     'filter'=> \app\models\BoxOffice::typeLabels(),
    //     'filterType'=> GridView::FILTER_SELECT2,
    //     'filterWidgetOptions'=> [
    //            'options' => ['prompt' => ''],
    //            'pluginOptions' => [
    //                   'allowClear' => true,
    //                   'tags' => false,
    //                   'tokenSeparators' => [','],
    //            ]
    //     ]
    // ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'service_id',
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('service_id'),
        // 'value'=>function($model){
        //   $help = \app\models\Help::findOne($model->help_id);

        //   if($help){
        //     if($help->type_id == 1){
        //       return "ПЦР";
        //     } elseif($help->type_id == 2){
        //       return "Медотвод";
        //     }
        //   }
        // },
        'value' => 'service.name',
        'label' => 'Услуга',
        'filter'=> ArrayHelper::map(\app\models\Services::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_id',
        'label' => 'Оператор',
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('created_id'),
        'value'=>'created.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'label' => "Статус",
        'content' => function($model){
          // $help = \app\models\Help::findOne($model->help_id);

          if($model->status){
            return $model->status == "Оплачен" ? 'Оплачен' : 'Не оплачен';
          }
        },
        'hAlign' => GridView::ALIGN_CENTER,
        // 'visible'=>\app\models\BoxOffice::isVisibleAttr('status'),
        // 'filter'=> \app\models\BoxOffice::statusLabels(),
        'filter'=> [1 => 'Оплачено', 0 => 'Не оплачено'],
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],

    [
      'attribute' => 'payment_type',
      'label' => 'Способ оплаты',
      'filter'=> \app\models\Help::paymentTypeLabels(),
      'filterType'=> GridView::FILTER_SELECT2,
      'filterWidgetOptions' => [
        'options' => ['prompt' => ''],
        'pluginOptions' => [
          'allowClear' => true,
        ],
      ],
      'value' => function($model){
        // $help = \app\models\Help::findOne($model->help_id);

        if($model){
          return ArrayHelper::getValue(\app\models\Help::paymentTypeLabels(), $model->payment_type);
        }

      }
    ],

];   

