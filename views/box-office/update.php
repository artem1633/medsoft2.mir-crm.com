<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model BoxOffice */
?>
<div class="box-office-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
