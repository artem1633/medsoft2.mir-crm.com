<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use kartik\dynagrid\DynaGrid;
use \yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel BoxOfficeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Касса";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(isset($additionalLinkParams)){
    $createUrl = ArrayHelper::merge(['box-office/create'], $additionalLinkParams);
    $createUrl = ArrayHelper::merge($createUrl, ['display' => false]);
} else {
    $createUrl = ['box-office/create'];
}


$arr = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декабря'
];


$monthStart = date('n', strtotime($dateStart))-1;
$monthEnd = date('n', strtotime($dateEnd))-1;

?>

<style>
    #ajaxCrudDatatable .panel-info>.panel-heading {
        display: none!important;
    }
    #ajaxCrudDatatable .panel-info>.kv-panel-before>.pull-right {
        float: left!important;
    }
    #ajaxCrudDatatable .panel-info>.table-responsive {
        padding:  0px !important;
    }
</style>

<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item"><a href="#">Главная</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Касса</li>
                </ol>
              </nav>
              <h4 class="mg-b-0 tx-spacing--1">Касса — Оплата за <?= Yii::$app->formatter->asDate($dateStart, 'php:d '.$arr[$monthStart].' Y') ?> <i class="fa fa-arrow-right"></i>  <?= Yii::$app->formatter->asDate($dateEnd, 'php:d '.$arr[$monthEnd].' Y') ?> </h4>
            </div>
            <div class="d-none d-md-block">
              <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="<?= Url::toRoute(ArrayHelper::merge(['box-office/export'], Yii::$app->request->queryParams)) ?>"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file wd-10 mg-r-5"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg> Создать отчет</a>
            </div>
          </div>

            <p>
                <a href="<?= Url::toRoute(['box-office/create']) ?>" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" data-pjax="0">Создать</a>
            </p>
          <?php $form = ActiveForm::begin(['id' => 'search-form', 'action' => ['box-office/index'], 'method' => 'GET']) ?>
          <div class="row">
              <div class="col-md-12">
                    <div class="form-group">
                        <label>Дата</label>
                        <?= DateRangePicker::widget([
                            'id' => 'fld-date_range',
                            'name'=>'selectDate',
                            'value'=>$selectDate,
                            'convertFormat'=>true,
                            'pluginOptions'=>[
                                'locale'=>['format'=>'Y-m-d']
                            ]
                        ]); ?>
                    </div> 
              </div>
          </div>
          <?php ActiveForm::end() ?>



                        <?=GridView::widget([
                    'id'=>'crud-datatable-box_office',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columns.php'),
                             
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,  
                    'responsiveWrap' => false,        
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
 
                    ]
                ])?>

                <?php

                  $payed = 0;
                  $notPayed = 0;
                  foreach($dataProvider->models as $model)
                  {
                    // if($model->status == \app\models\BoxOffice::DONE){
                    if($model->status == "Оплачен"){
                      $payed = $payed + $model->amounts;
                    } else {
                      $notPayed = $notPayed + $model->amounts;
                    }
                  }

                ?>
            
                <p><b>Оплачено:</b> <?= Yii::$app->formatter->asDecimal($payed) ?>, <b>Неоплачено:</b> <?= Yii::$app->formatter->asDecimal($notPayed) ?></p>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS

$('#fld-date_range').change(function(){
  $('#search-form').submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>