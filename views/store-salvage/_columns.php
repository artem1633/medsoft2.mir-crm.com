<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["store-salvage"."/".$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
   
 
        [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED; //ROW_EXPANDED
            },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true,
        'detail' => function ($model, $key, $index, $column) {
            $filesSearchModel = new \app\models\StoreSalvageItemSearch();
            $filesDataProvider = $filesSearchModel->search([]);
            $filesDataProvider->query->andWhere(['store_in' => $model->id]);
            return GridView::widget([
                    'id'=>'crud-datatable-expand-store_salvage_item-'.$model->id,
                    'dataProvider' => $filesDataProvider,
                    'pjax'=>true, 
                    'columns' => require(__DIR__.'/../store-salvage-item/_columns.php'),
                    'panelBeforeTemplate' => '',
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                ]); 
        },
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'warehouse_id',
        'visible'=>1,
        'value'=>'warehouse.name',
        'filter'=> ArrayHelper::map(\app\models\Store::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'visible'=>1,
        'filter'=> \app\models\StoreSalvage::statusLabels(),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
          'convertFormat'=>true,
          'pluginEvents' => [
              'cancel.daterangepicker'=>'function(ev, picker) {}'
           ],
           'pluginOptions' => [
              'opens'=>'right',
              'locale' => [
                  'cancelLabel' => 'Clear',
                  'format' => 'Y-m-d',
               ]
           ]
         ],
        'attribute'=>'create_at',
        'visible'=>1,
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vkladka',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'position',
         'visible'=>0,
    ],

];   

