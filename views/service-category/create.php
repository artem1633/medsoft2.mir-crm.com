<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model ServiceCategory */

?>
<div class="service-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
