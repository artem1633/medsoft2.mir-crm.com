<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model ServiceCategory */
?>
<div class="service-category-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/services/index.php", [
            'searchModel' => $servicesSearchModel,
            'dataProvider' => $servicesDataProvider,
            'additionalLinkParams' => ['Services[category_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
