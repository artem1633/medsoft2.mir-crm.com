<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model ServiceCategory */
?>
<div class="service-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
