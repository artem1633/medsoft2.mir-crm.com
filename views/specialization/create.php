<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Specialization */

?>
<div class="specialization-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
