<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model StoreInItem */
?>
<div class="store-in-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
