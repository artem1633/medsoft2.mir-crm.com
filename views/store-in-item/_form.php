<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\StoreInItem */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="store-in-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                              <div id="div-tovar-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'tovar_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Products::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-amount" class="col-md-12" style=" ">        
             <?= $form->field($model, 'amount')->textInput(['type' => 'number'])  ?>
        </div>
        <div id="div-prinyato" class="col-md-12" style=" ">        
             <?= $form->field($model, 'prinyato')->textInput(['type' => 'number'])  ?>
        </div>
        <div id="div-the-cost" class="col-md-12" style=" ">        
             <?= $form->field($model, 'the_cost')->textInput(['type' => 'number'])  ?>
        </div>
        <div id="div-mesto-na-sklade" class="col-md-12" style=" ">                
         <?= $form->field($model, 'mesto_na_sklade')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\PlaceStock::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

