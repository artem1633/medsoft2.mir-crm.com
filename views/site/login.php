<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\forms\LoginForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
///* @var $model \common\models\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

?>
<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> <?=Yii::$app->name ?>            <small>Заполните данные для авторизации</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>

        <div style="display: none;">
            <?= $form->field($model, 'scenario')->hiddenInput()->label(false) ?>
        </div>

        <div id="scenario-default"<?= $model->scenario != LoginForm::SCENARIO_DEFAULT ? ' style="display: none;"' : null ?>>
            
        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->widget(\yii\widgets\MaskedInput::class,
                [
                    'mask' => '+7(999)999-99-99',
                    'options' => ['placeholder' => 'Телефон', 'class' => 'form-control input-lg inverse-mode no-border'],
                ]) ?>

            <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => 'Пароль', 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <div class="login-buttons">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>        </div>
        <div style="text-align: center; margin-top: 22px;">
            <?= Html::a('Забыли пароль?', ['site/forget-password']) ?>        </div>
        </div>


        <div id="scenario-phone"<?= $model->scenario != LoginForm::SCENARIO_PHONE ? ' style="display: none;"' : null ?>>
            <p>На номер <?=$model->username?> был отправлен код подтверждения</p>
            <?= $form
            ->field($model, 'code', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => 'Код', 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <div class="login-buttons">
            <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>        </div>
        </div>
        <?php ActiveForm::end(); ?>    </div>
</div>

