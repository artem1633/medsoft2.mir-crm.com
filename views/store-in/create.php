<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model StoreIn */

?>
<div class="store-in-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
