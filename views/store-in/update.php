<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model StoreIn */
?>
<div class="store-in-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
