<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\StoreIn */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
	$model->position = \app\models\StoreInItem::find()->where(['store_in' => $model->id])->asArray()->all();
	for ($i=0; $i < count($model->position); $i++) {
		$row = $model->position[$i];
		foreach($row as $key => $value){
		}
	}
}
?>
<div class="store-in-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                               <div id="div-nomer-zakaza" class="col-md-6" style=" ">        
             <?= $form->field($model, 'nomer_zakaza')->textInput()  ?>
        </div>
        <div id="div-warehouse-id" class="col-md-6" style=" ">                
         <?= $form->field($model, 'warehouse_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Store::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-status" class="col-md-6" style=" ">        
         <?= $form->field($model, 'status')->dropDownList(app\models\StoreIn::statusLabels(), ['prompt' => 'Выберите вариант']) ?>
      
        </div>
        <div id="div-the-supplier-id" class="col-md-6" style=" ">                
         <?= $form->field($model, 'the_supplier_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Suppliers::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div id="div-position" class="col-md-12" style=" ">              
             <?= $form->field($model, 'position')->widget(unclead\multipleinput\MultipleInput::className(), [
                'id' => 'my_idposition',
                'min' => 0,
                'columns' => [

                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden'
                        ]
                    ],
                    [
                        'name' => 'tovar_id',
                        'title' => Yii::t('app','Товар'),
                        'enableError' => true,
							'type' => Select2::class,
							'options' => [ 'data' => \yii\helpers\ArrayHelper::map(\app\models\Products::find()->all(),'id','name')]
                    ],
                    [
                        'name' => 'amount',
                        'title' => Yii::t('app','Кол-во'),
                        'enableError' => true,
                    ],
                    [
                        'name' => 'prinyato',
                        'title' => Yii::t('app','Принято'),
                        'enableError' => true,
                    ],
                    [
                        'name' => 'the_cost',
                        'title' => Yii::t('app','Стоимость'),
                        'enableError' => true,
                    ],
                    [
                        'name' => 'mesto_na_sklade',
                        'title' => Yii::t('app','Место на складе'),
                        'enableError' => true,
							'type' => Select2::class,
							'options' => [ 'data' => \yii\helpers\ArrayHelper::map(\app\models\PlaceStock::find()->all(),'id','name')]
                    ],
                    ],
                ])  ?>

        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

