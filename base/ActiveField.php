<?php

namespace app\base;

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class ActiveField extends \yii\widgets\ActiveField
{
	public $cols = null;

	public $colsOptionsStr = '';

	/**
     * {@inheritdoc}
     */
    public function render($content = null)
    {
        if ($content === null) {
            if (!isset($this->parts['{input}'])) {
                $this->textInput();
            }
            if (!isset($this->parts['{label}'])) {
                $this->label();
            }
            if (!isset($this->parts['{error}'])) {
                $this->error();
            }
            if (!isset($this->parts['{hint}'])) {
                $this->hint(null);
            }
            $content = strtr($this->template, $this->parts);
        } elseif (!is_string($content)) {
            $content = call_user_func($content, $this);
        }

        // $className = str_replace(['{', '}', '%'], ['', '', ''], );

        if($this->model instanceof \app\base\ActiveRecord){
            $allow = $this->model::isVisibleAttr($this->attribute);

            if($allow == false){
                return '';
            }
        }



        if($this->cols == null){
	        return $this->begin() . "\n" . $content . "\n" . $this->end();
        } else {
        	// if(isset($this->colsOptions['id']) == false){
        		// $this->colsOptions['id'] = 'div-'.Inflector::camel2id(StringHelper::basename($this->attribute));;
        	// }
        	return "<div class='col-md-{$this->cols}' ".$this->colsOptionsStr.">".$this->begin() . "\n" . $content . "\n" . $this->end().'</div>'; 
        }
    }
}