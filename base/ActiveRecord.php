<?php

namespace app\base;

use app\models\Role;

 /**
 * Базовая модель
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
	/**
	 * Проверяет доступен ли атрибут у пользователя
	 * @param string $attr
	 * @return boolean
	 */
    public static function isVisibleAttr($attr)
    {
        return true;

        $role = Role::findOne(\Yii::$app->user->identity->role_id);

        if($role){
            $fldAttr = str_replace(['{', '}', '%'], ['', '', ''], self::tableName())."_disallow_fields";
            $flds = explode(',', $role->$fldAttr);
            return !in_array($attr, $flds);
        }

        return true;
    }
}


?>