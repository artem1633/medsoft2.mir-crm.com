// $('#btn-dropdown_header').click(function(){
//     if($(".dropdown-menu").is(':visible')){
//         $(".dropdown-menu ").hide();
//     } else {
//         $(".dropdown-menu ").show();
//     }
// });

// initTabletPicker();

// $('.multiple-input').on('afterAddRow', function(){
//     initTabletPicker();
// });

// $('#ajaxCrudModal').on('shown.bs.modal', function() {
//     setTimeout(function(){
//         initTabletPicker();

//         $('.multiple-input').on('afterAddRow', function(){
//             initTabletPicker();
//         });
//     }, 500);
// });


// function initTabletPicker()
// {
//     $('[type="date"]').each(function(){
//         $(this).data('value', $(this).val());
//         $(this).attr('placeholder', 'Выберите дату');
//     });

//     $('[type="date"]').pickadate({
//         format: 'dd.mm.yyyy',
//         formatSubmit: 'yyyy-mm-dd'
//     });

//     $('[type="datetime-local"]').each(function(){
//         $(this).data('value', $(this).val());
//         $(this).attr('placeholder', 'Выберите дату');
//     });

//     $('[type="datetime-local"]').pickadate({
//         format: 'dd.mm.yyyy',
//         formatSubmit: 'yyyy-mm-dd'
//     });
// }

// function typeInTextarea(el, newText) {
//     var start = el.prop("selectionStart")
//     var end = el.prop("selectionEnd")
//     var text = el.val()
//     var before = text.substring(0, start)
//     var after  = text.substring(end, text.length)
//     el.val(before + newText + after)
//     el[0].selectionStart = el[0].selectionEnd = start + newText.length
//     el.focus()
// }

$('.aside-header .aside-menu-link').click(function(event){
    event.preventDefault();

    if($('aside').hasClass('aside-active')){
        $('aside').removeClass('aside-active');
        if(window.innerWidth > 1000){
            $('aside').removeClass('minimize');
        }
        $('aside').find('.aside-menu-link .feather.feather-menu').show();
        $('aside').find('.aside-menu-link .feather.feather-x').hide();
    } else {
        $('aside').addClass('aside-active');
        if(window.innerWidth > 1000){
            $('aside').addClass('minimize');
        }
        $('aside').find('.aside-menu-link .feather.feather-menu').hide();
        $('aside').find('.aside-menu-link .feather.feather-x').show();
    }    
});



function headerMenuMobileInit()
{
    if(window.innerWidth < 1000){
        $('.nav.navbar-menu .nav-item').click(function(e){
            e.preventDefault();
            // $('.nav.navbar-menu .nav-item').removeClass('show');
            $(this).toggleClass('show');
        });
        $('.nav.navbar-menu .nav-item .navbar-menu-sub a').click(function(e){
            e.preventDefault();

            window.location.href = $(this).attr('href');
        });
    }
}

$(document).on('pjax:complete', function (event, xhr, textStatus, options) {
    feather.replace()
});



headerMenuMobileInit();