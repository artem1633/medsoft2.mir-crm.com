<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'libs/datepicker/datepicker.min.css',
        // 'libs/pickadate/lib/themes/default.css',
        // 'libs/pickadate/lib/themes/classic.date.css',
         'new_theme/lib/@fortawesome/fontawesome-free/css/all.min.css',
         'new_theme/lib/ionicons/css/ionicons.min.css',
         'new_theme/lib/jqvmap/jqvmap.min.css',
         'new_theme/assets/css/dashforge.css',
         'new_theme/assets/css/dashforge.dashboard.css',
        'css/site.css',
//        'libs/flatpickr-master/dist/flatpickr.min.css'
    ];
    public $js = [
//        'libs/flatpickr-master/dist/flatpickr.min.js',
//        'libs/flatpickr-master/dist/I10n/ru.js',
//        'libs/cleave/cleave.min.js',
        // 'libs/datepicker/datepicker.min.js',
        // 'libs/pickadate/lib/legacy.js',
        // 'libs/pickadate/lib/picker.js',
        // 'libs/pickadate/lib/picker.date.js',
        // 'libs/pickadate/lib/translations/ru_RU.js',
//        'js/common.js'
//        'new_theme/lib/jquery/jquery.min.js',
        'new_theme/lib/bootstrap/js/bootstrap.bundle.min.js',
        'new_theme/lib/feather-icons/feather.min.js',
        'new_theme/perfect-scrollbar/perfect-scrollbar.min.js',
        'new_theme/jquery.flot/jquery.flot.js',
        'new_theme/lib/jquery.flot/jquery.flot.stack.js',
        'new_theme/lib/jquery.flot/jquery.flot.resize.js',
        'new_theme/lib/chart.js/Chart.bundle.min.js',
        'new_theme/lib/jqvmap/jquery.vmap.min.js',
        'new_theme/lib/jqvmap/maps/jquery.vmap.usa.js',
        'new_theme/assets/js/dashforge.js',
        'new_theme/assets/js/dashforge.aside.js',
        'new_theme/assets/js/dashforge.sampledata.js',
        'new_theme/assets/js/dashboard-one.js',
        'new_theme/lib/js-cookie/js.cookie.js',
        'new_theme/assets/js/dashforge.settings.js',
        'js/common.js',
//        'new_theme/',
//        'new_theme/',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
//<script src="lib/jquery/jquery.min.js"></script>
//    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
//    <script src="lib/feather-icons/feather.min.js"></script>
//    <script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
//    <script src="lib/jquery.flot/jquery.flot.js"></script>
//    <script src="lib/jquery.flot/jquery.flot.stack.js"></script>
//    <script src="lib/jquery.flot/jquery.flot.resize.js"></script>
//    <script src="lib/chart.js/Chart.bundle.min.js"></script>
//    <script src="lib/jqvmap/jquery.vmap.min.js"></script>
//    <script src="lib/jqvmap/maps/jquery.vmap.usa.js"></script>
//
//    <script src="assets/js/dashforge.js"></script>
//    <script src="assets/js/dashforge.aside.js"></script>
//    <script src="assets/js/dashforge.sampledata.js"></script>
//    <script src="assets/js/dashboard-one.js"></script>
//
//    <!-- append theme customizer -->
//    <script src="lib/js-cookie/js.cookie.js"></script>
//    <script src="assets/js/dashforge.settings.js"></script>
