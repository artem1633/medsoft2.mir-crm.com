<?php

namespace app\widgets;

use kartik\base\InputWidget;

class FileInput extends InputWidget {

	public $previewPath;

	public function run()
	{
		return $this->render('file-input', [
			'widget' => $this,
			'form' => $this->field->form,
			'model' => $this->model,
			'attribute' => $this->attribute,
		]);
	}

}

?>