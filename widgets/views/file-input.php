<?php

use \yii\helpers\Html;

$previewPath = '/img/no-photo.png';

if($widget->previewPath){
	if(substr($widget->previewPath, 0, 1) == '/'){
		$previewPath = $widget->previewPath;
	} else {
		$previewPath = '/'.$widget->previewPath;
	}
}

?>

<div>
	<img id="<?=$widget->id?>-preview" src="<?= $previewPath ?>" style="display: inline-block; height: 100px; width: 70px; border: 1px solid #cecece; object-fit: contain;">
	<?= Html::a("Загрузить фото", '#', [
		'class' => 'btn btn-sm btn-success',
		'style' => 'vertical-align: bottom;',
		'onclick' => "event.preventDefault(); $('#{$widget->id}-wrapper input').trigger('click');",
	]) ?>
</div>

<div id="<?=$widget->id?>-wrapper" style="display: none;">
	<?= $form->field($model, $attribute)->fileInput() ?>
</div>

<?php

$script = <<< JS

$("#{$widget->id}-wrapper input").change(function(){
	var url = URL.createObjectURL($(this)[0].files[0]);
	$("#{$widget->id}-preview").attr("src", url);
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>