<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell;
use app\behaviors\RoleBehavior;
use app\models\Products;
use app\models\ProductsSearch;
use yii\web\Controller;
use app\models\Template;
use app\components\TagsHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['application-form'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'instanceQuery' => \app\models\Products::find(),
                'actions' => [
                    'create' => 'books',
                    'update' => 'books',
                    'view' => 'books',
                    'delete' => 'books',
                    'index' => 'books',
                ],
            ],
        ];
    }
    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {    
        
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Products model.
     * 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);

            $storeInItemSearchModel = new \app\models\StoreInItemSearch();
        $storeInItemDataProvider = $storeInItemSearchModel->search(Yii::$app->request->queryParams);
        $storeInItemDataProvider->query->andWhere(['tovar_id' => $id]);
            $storeRemainsSearchModel = new \app\models\StoreRemainsSearch();
        $storeRemainsDataProvider = $storeRemainsSearchModel->search(Yii::$app->request->queryParams);
        $storeRemainsDataProvider->query->andWhere(['tovar_id' => $id]);
            $storeSalvageItemSearchModel = new \app\models\StoreSalvageItemSearch();
        $storeSalvageItemDataProvider = $storeSalvageItemSearchModel->search(Yii::$app->request->queryParams);
        $storeSalvageItemDataProvider->query->andWhere(['tovar_id' => $id]);




        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Продукты #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
			'storeInItemSearchModel'=>$storeInItemSearchModel,
			'storeInItemDataProvider'=>$storeInItemDataProvider,
			'storeRemainsSearchModel'=>$storeRemainsSearchModel,
			'storeRemainsDataProvider'=>$storeRemainsDataProvider,
			'storeSalvageItemSearchModel'=>$storeSalvageItemSearchModel,
			'storeSalvageItemDataProvider'=>$storeSalvageItemDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
			'storeInItemSearchModel'=>$storeInItemSearchModel,
			'storeInItemDataProvider'=>$storeInItemDataProvider,
			'storeRemainsSearchModel'=>$storeRemainsSearchModel,
			'storeRemainsDataProvider'=>$storeRemainsDataProvider,
			'storeSalvageItemSearchModel'=>$storeSalvageItemSearchModel,
			'storeSalvageItemDataProvider'=>$storeSalvageItemDataProvider,
            ]);
        }
    }



    /**
     * Creates a new Products model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Products();

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Продукты",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-products-pjax',
                    'title'=> "Добавить Продукты",
                    'content'=>'<span class="text-success">Создание Продукты успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ]; 
        
            }else{           
                return [
                    'title'=> "Добавить Продукты",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Продукты #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-products-pjax',
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Продукты #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
 
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }
    
   
   
   
   
   

    public function actionAdd()
    {
        $request=Yii::$app->request;
        $model = new Products();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->fileUploading = UploadedFile::getInstance($model, 'fileUploading');
            $error = 0;
            $success = 0;
            if (!empty($model->fileUploading)) {
                $filename = 'uploads/'.$model->fileUploading;
                $model->fileUploading->saveAs($filename);
                $file = fopen($filename, 'r');
                if($file) {
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                    $worksheetTitle     = $worksheet->getTitle();
                    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
                    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    for ($row = 2; $row <= $highestRow; ++ $row) {

                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        if (!$cell->getValue()) {
                            continue;
                        }
                        $newModel = new Products();
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $newModel->name  = (string)$cell->getValue();
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $newModel->artikul  = (string)$cell->getValue();
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $newModel->kriticheskoe_kol  = $cell->getValue();
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $newModel->edinica_zapravki  = $cell->getValue();
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $newModel->code  = (string)$cell->getValue();
                        if (!$newModel->save()) {
                            $error++;
                        } else {
                            $success++;
                        }
                    }
                }

                    return [
                        'forceReload'=>'#crud-datatable-products-pjax',
                        'title'=> "Загружения",
                        'content'=>"Удачно загруженно: {$success} <br/> Ошибка загрузки: {$error}",
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                    // exit;
                    return [
                        'forceReload'=>'#crud-datatable-products-pjax',
                        'forceClose'=>true,
                    ];   
                } else {
                    return [
                        'forceReload'=>'#crud-datatable-products-pjax',
                        'title'=> "Загружения",
                        'content'=>"<span class='text-danger'>Ошибка при загрузке файла</span>",
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            } else {
                return [
                    'title'=> "<span class='text-danger'>Выберите файл</span>",
                    'size'=>'normal',
                    'content'=>$this->renderAjax('add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                    ];
            }
        }
    }


    /**
     * Temp an existing Nomenclature model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionTemp()
    {
        $model = new Products();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();

        $i = 0;
        foreach ($model->attributeLabels() as $attr){
            $objGet->setCellValueByColumnAndRow($i, 1 , $attr);
            $i++;
        }

        $filename = 'temp.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('temp.xlsx');

        Yii::$app->response->sendFile('temp.xlsx');
    }
    

    /**
     * Delete an existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-products-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-products-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
