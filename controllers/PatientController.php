<?php

namespace app\controllers;

use Yii;
use app\behaviors\RoleBehavior;
use app\models\Patient;
use app\models\PatientSearch;
use yii\web\Controller;
use app\models\Template;
use app\components\TagsHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PatientController implements the CRUD actions for Patient model.
 */
class PatientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['application-form'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'instanceQuery' => \app\models\Patient::find(),
                'actions' => [
                    'create' => 'patient_create',
                    'update' => 'patient_update',
                    'view' => 'patient_view',
                    'delete' => 'patient_delete',
                    'bulk-delete' => 'patient_delete',
                    'index' => ['patient_view', 'patient_view_all'],
                ],
            ],
        ];
    }
    /**
     * Lists all Patient models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Patient model.
     * 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);

            $helpSearchModel = new \app\models\HelpSearch();
        $helpDataProvider = $helpSearchModel->search(Yii::$app->request->queryParams);
        $helpDataProvider->query->andWhere(['patient_id' => $id]);




        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Пациент #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
			'helpSearchModel'=>$helpSearchModel,
			'helpDataProvider'=>$helpDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
			'helpSearchModel'=>$helpSearchModel,
			'helpDataProvider'=>$helpDataProvider,
            ]);
        }
    }

    public function actionViewAjax($name = null, $last_name = null, $patronymic = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // if($name == null && $last_name == null && $patronymic == null){
            // return null;
        // }

        $patient = Patient::find()->where(['name' => $name ? $name : null, 'last_name' => $last_name ? $last_name : null, 'patronymic' => $patronymic ? $patronymic : null])->one();

        return $patient;
    }

    public function actionViewAjaxById($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $patient = Patient::findOne($id);

        return $patient;
    }


    /**
     * Creates a new Patient model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Patient();

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Пациент",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-patient-pjax',
                    'title'=> "Добавить Пациент",
                    'content'=>'<span class="text-success">Создание Пациент успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ]; 
        
            }else{
            	\Yii::warning($model->errors, 'Validation errors');           
                return [
                    'title'=> "Добавить Пациент",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
            	\Yii::warning($model->errors, 'Validation errors');           
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Patient model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Пациент #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-patient-pjax',
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Пациент #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
 
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }
    
   
   
   
 /**
     * Creates a new Help model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionDownload($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $content = $model->tags(file_get_contents(__DIR__.'/../contract.html'));

        return $this->renderPartial('print', ['content' => $content]);
        // if($template)
        // {
        //     // $content = '123';

        //     // $content = '<style>* { font-family:  DejaVu Sans !important; }</style>'.$this->renderPartial('@app/views/_print/print.php', [
        //         // 'content' => $content,
        //     // ]);

        //     $dompdf = new \Dompdf\Dompdf();

        //     $dompdf->load_html($content, 'utf-8');
        //     $dompdf->render();

        //     $pdf = $dompdf->output(); 
        //     file_put_contents(__DIR__.'/../web/schet-10.pdf', $pdf);

        //     $fileName = 'untitled.pdf';

        //     if($patient && $certificate){
        //         $fio = $patient->getFio();
        //         $typeName = $certificate->name;

        //         $fileName = "Рузальтат {$typeName} — {$fio}.pdf";
        //     }

        //     Yii::$app->response->sendFile(__DIR__.'/../web/schet-10.pdf', $fileName);
        // }

    }
   
   
   
    

    /**
     * Delete an existing Patient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-patient-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Patient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-patient-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
