<?php

namespace app\controllers;

use Yii;
use app\behaviors\RoleBehavior;
use app\models\BoxOffice;
use app\models\BoxOfficeSearch;
use yii\web\Controller;
use app\models\Template;
use app\components\TagsHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * BoxOfficeController implements the CRUD actions for BoxOffice model.
 */
class BoxOfficeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['application-form'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            // 'role' => [
            //     'class' => RoleBehavior::class,
            //     'instanceQuery' => \app\models\BoxOffice::find(),
            //     'actions' => [
            //         'create' => 'box_office_create',
            //         'update' => 'box_office_update',
            //         'view' => 'box_office_view',
            //         'delete' => 'box_office_delete',
            //         'bulk-delete' => 'box_office_delete',
            //         'index' => ['box_office_view', 'box_office_view_all'],
            //     ],
            // ],
        ];
    }
    
    /**
     * Lists all BoxOffice models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new BoxOfficeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        $selectDate = isset($_GET['selectDate']) ? $_GET['selectDate'] : date('Y-m-d') . ' - ' . date('Y-m-d');

        $selectDateArr = explode(' - ', $selectDate);

        $dateStart = $selectDateArr[0];
        $dateEnd = $selectDateArr[1];

        $dataProvider->query->andWhere(['between', 'created_at', $dateStart.' 00:00:00', $dateEnd.' 23:59:59']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'selectDate' => $selectDate,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    public function actionExport()
    {
        $request = Yii::$app->request;


        $searchModel = new BoxOfficeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        $selectDate = isset($_GET['selectDate']) ? $_GET['selectDate'] : date('Y-m-d') . ' - ' . date('Y-m-d');

        $selectDateArr = explode(' - ', $selectDate);

        $dateStart = $selectDateArr[0];
        $dateEnd = $selectDateArr[1];

        $dataProvider->query->andWhere(['between', 'created_at', $dateStart.' 00:00:00', $dateEnd.' 23:59:59']);

        $columns = require(__DIR__.'/../views/box-office/_columns_excel.php');

        $data = [];


        foreach ($columns as $column){
            $output = $column['label'];

            $data[0][] = $output;
        }

        foreach($dataProvider->models as $model)
        {
            $row = [];

           foreach ($columns as $column) {
                $attr = isset($column['attribute']) ? $column['attribute'] : null;
               if(isset($column['value'])){
                if(is_callable($column['value'])){
                    $row[] = call_user_func($column['value'], $model);
                } else {
                    $row[] = \yii\helpers\ArrayHelper::getValue($model, $column['value']);
                }
               } elseif(isset($column['content'])){
                if(is_callable($column['content'])){
                    $row[] = call_user_func($column['content'], $model);
                } else {
                    $row[] = \yii\helpers\ArrayHelper::getValue($model, $column['content']);
                }
               } else {
                   $row[] = $model->$attr;

               }
           }

            $data[] = $row;
        }

        // \yii\helpers\VarDumper::dump($data, 10, true);
    
        // Генерация Excel
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();

        $styleForHeaders = array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => '000000')));

//        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);

//        $objGet->setCellValueByColumnAndRow(0, 1, 'Привет как дела');

        $objGet->getColumnDimension('A')->setWidth(30);
        $objGet->getColumnDimension('B')->setWidth(30);
        $objGet->getColumnDimension('C')->setWidth(30);
        $objGet->getColumnDimension('D')->setWidth(30);
        $objGet->getColumnDimension('E')->setWidth(30);
        $objGet->getColumnDimension('F')->setWidth(30);
        $objGet->getColumnDimension('G')->setWidth(30);
        $objGet->getColumnDimension('H')->setWidth(30);
        $objGet->getColumnDimension('I')->setWidth(30);
        $objGet->getColumnDimension('J')->setWidth(30);
        $objGet->getColumnDimension('L')->setWidth(30);
        $objGet->getColumnDimension('M')->setWidth(30);
        $objGet->getColumnDimension('N')->setWidth(30);
        $objGet->getColumnDimension('O')->setWidth(30);
        $objGet->getColumnDimension('P')->setWidth(30);
        $objGet->getColumnDimension('Q')->setWidth(30);
        $objGet->getColumnDimension('R')->setWidth(30);
        $objGet->getColumnDimension('S')->setWidth(30);
        $objGet->getColumnDimension('T')->setWidth(30);
        $objGet->getColumnDimension('U')->setWidth(30);
        $objGet->getColumnDimension('V')->setWidth(30);
        $objGet->getColumnDimension('W')->setWidth(30);
        $objGet->getColumnDimension('X')->setWidth(30);
        $objGet->getColumnDimension('Y')->setWidth(30);
        $objGet->getColumnDimension('Z')->setWidth(30);
        $objGet->getColumnDimension('AA')->setWidth(30);
        $objGet->getColumnDimension('AB')->setWidth(30);
        $objGet->getColumnDimension('AC')->setWidth(30);

        for ($i = 0; $i <= count($data); $i++)
        {
            if(isset($data[$i]) == false){
                continue;
            }

            $row = $data[$i];

            for ($j = 0; $j <= count($row); $j++)
            {
                if(isset($row[$j])){
                    $value = $row[$j];

                    $objGet->setCellValueByColumnAndRow($j, ($i + 1), $value);
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->getAlignment()->setWrapText(true);
                }

                if($i == 0){
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->applyFromArray($styleForHeaders);
                }
            }
        }

        $filename = 'test.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('test.xlsx');

        Yii::$app->response->sendFile('test.xlsx');
    }

    /**
     * Displays a single BoxOffice model.
     * 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);





        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Касса #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }


    public function actionDownload($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $patient = \app\models\Patient::findOne($model->patient_id);
        // $help = \app\models\Patient::findOne($model->help_id);
        
        $fileName = "Безымянный.pdf";

        if($patient)
        {
            $fio = [];

            if($patient->last_name){
                $fio[] = $patient->last_name;
            }

            if($patient->name){
                $fio[] = $patient->name;
            }

            if($patient->patronymic){
                $fio[] = $patient->patronymic;
            }

            $fio = implode(' ', $fio);

            $data = [
                1 => 'РНК НЕ ОБНАРУЖЕНА',
                2 => 'ОБНАРУЖЕНО',
            ];

            $fileName = (isset($data[$patient->discovered]) ? $data[$patient->discovered] : null)." — {$fio}.pdf";
        }

        \Yii::$app->response->sendFile("test.pdf", $fileName);
    }



    /**
     * Creates a new BoxOffice model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new BoxOffice();

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Кассу",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-box_office-pjax',
                    'title'=> "Добавить Кассу",
                    'content'=>'<span class="text-success">Создание Кассы успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ]; 
        
            }else{           
                return [
                    'title'=> "Добавить Кассу",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing BoxOffice model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Кассу #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){

                    

                return [
                    'forceReload'=>'#crud-datatable-box_office-pjax',
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Кассу #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
 
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }
    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    

    /**
     * Delete an existing BoxOffice model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-box_office-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing BoxOffice model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-box_office-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the BoxOffice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @return BoxOffice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BoxOffice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
