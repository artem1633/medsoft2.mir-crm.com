<?php

namespace app\controllers;

use Yii;
use app\behaviors\RoleBehavior;
use app\models\Help;
use app\models\HelpSearch;
use yii\web\Controller;
use app\models\Template;
use app\components\TagsHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use \app\models\Patient;

/**
 * HelpController implements the CRUD actions for Help model.
 */
class HelpController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['application-form'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'instanceQuery' => \app\models\Help::find(),
                'actions' => [
                    'create' => 'help_create',
                    'update' => 'help_update',
                    'view' => 'help_view',
                    'delete' => 'help_delete',
                    'bulk-delete' => 'help_delete',
                    'index' => ['help_view', 'help_view_all'],
                ],
            ],
        ];
    }
    /**
     * Lists all Help models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new HelpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
         /**
     * Creates a new Help model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPrint($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $certificate = \app\models\TypesCertificates::findOne($model->type_id);

        $template = \app\models\HelpTemplate::findOne($certificate->shablon_spravki_id);

        if($template)
        {
            $content = $model->tags($template->verstka);
            return $this->renderPartial('@app/views/_print/print.php', [
                'content' => $content,
            ]);
        }

    }


 /**
     * Creates a new Help model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionDownload($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $certificate = \app\models\TypesCertificates::findOne($model->type_id);
        
        $patient = \app\models\Patient::findOne($model->patient_id);

        $template = \app\models\HelpTemplate::findOne($certificate->shablon_spravki_id);



        $template = \app\models\Help::findOne($id);

        if (!isset($template->type->shablon_spravki_id)) {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
        $templateHelp = \app\models\HelpTemplate::findOne($template->type->shablon_spravki_id);

        $content = $template->tags($templateHelp->verstka);

        return $this->renderPartial('print',['content' => $content]);




        // if($template)
        // {
        //     // $content = '123';

        //     // $content = '<style>* { font-family:  DejaVu Sans !important; }</style>'.$this->renderPartial('@app/views/_print/print.php', [
        //         // 'content' => $content,
        //     // ]);

        //     $dompdf = new \Dompdf\Dompdf();

        //     $dompdf->load_html($content, 'utf-8');
        //     $dompdf->render();

        //     $pdf = $dompdf->output(); 
        //     file_put_contents(__DIR__.'/../web/schet-10.pdf', $pdf);

        //     $fileName = 'untitled.pdf';

        //     if($patient && $certificate){
        //         $fio = $patient->getFio();
        //         $typeName = $certificate->name;

        //         $fileName = "Рузальтат {$typeName} — {$fio}.pdf";
        //     }

        //     Yii::$app->response->sendFile(__DIR__.'/../web/schet-10.pdf', $fileName);
        // }

    }



    /**
     * Displays a single Help model.
     * 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);





        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Справки #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }



    /**
     * Creates a new Help model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Help();
        $modelPatient = new Patient();

        $model->type_id = 1; // Тип ПЦР тест

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Справки",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelPatient' => $modelPatient,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){


                if($modelPatient->load($request->post()) && $modelPatient->save()){

                }
                
                return [
                    'forceReload'=>'#crud-datatable-help-pjax',
                    'title'=> "Добавить Справки",
                    'content'=>'<span class="text-success">Создание Справки успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ]; 
        
            }else{           
                return [
                    'title'=> "Добавить Справки",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelPatient' => $modelPatient,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($request->isPost) {

                $model->load($request->post());

                if($model->patient_id){
                    $modelPatient = Patient::findOne($model->patient_id);
                    $modelPatient->load($request->post());
                    if($modelPatient->save() == false){
                        \Yii::warning($modelPatient->errors);

                        return $this->render('create', [
                            'model' => $model,
                            'modelPatient' => $modelPatient,
                        ]);
                    }
                    $model->patient_id = $modelPatient->id;
                } else {
                    $modelPatient->load($request->post());
                    if($modelPatient->save() == false){
                        return $this->render('create', [
                            'model' => $model,
                            'modelPatient' => $modelPatient,
                        ]);
                    }
                    $model->patient_id = $modelPatient->id;
                }

                $model->save(false);

                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelPatient' => $modelPatient,
                ]);
            }
        }
       
    }

    /**
     * Creates a new Help model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateOut()
    {
        $request = Yii::$app->request;
        $model = new Help();
        $modelPatient = new Patient();

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }

        $model->type_id = 2; // Тип медоотвод

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Справки",
                    'content'=>$this->renderAjax('_form-out', [
                        'model' => $model,
                        'modelPatient' => $modelPatient,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-help-pjax',
                    'title'=> "Добавить Справки",
                    'content'=>'<span class="text-success">Создание Справки успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ]; 
        
            }else{           
                return [
                    'title'=> "Добавить Справки",
                    'content'=>$this->renderAjax('_form-out', [
                        'model' => $model,
                        'modelPatient' => $modelPatient,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $modelPatient->load($request->post())) {

                if($model->patient_id == null){
                    if($modelPatient->save()){
                        $model->patient_id = $modelPatient->id;
                        if($model->save() == false){
                            return $this->render('_form-out', [
                                'model' => $model,
                                'modelPatient' => $modelPatient,
                            ]);
                        }
                    } else {
                        return $this->render('_form-out', [
                            'model' => $model,
                            'modelPatient' => $modelPatient,
                        ]);
                    }
                } else {
                    $modelPatient->save(false);
                }

                return $this->redirect(['index']);
            } else {
                return $this->render('_form-out', [
                    'model' => $model,
                    'modelPatient' => $modelPatient,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Help model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        $modelPatient = \app\models\Patient::findOne($model->patient_id);

        if($modelPatient == null){
            $modelPatient = new \app\models\Patient();
        }

        if($model->type_id == 1){
            $view = 'update';
        } elseif($model->type_id == 2){
            $view = '_form-out';
        } 

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Справки #".$id,
                    'content'=>$this->renderAjax($view, [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){


                if($modelPatient->load($request->post()) && $modelPatient->save()){

                }

                return [
                    'forceReload'=>'#crud-datatable-help-pjax',
                    'forceClose' => true,
                ];
            }else{
                 return [
                    'title'=> "Изменить Справки #".$id,
                    'content'=>$this->renderAjax($view, [
                        'model' => $model,
                        'modelPatient' => $modelPatient,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($request->isPost) {

                $model->load($request->post());

                // if($model->patient_id){

                // } else {
                    $modelPatient->load($request->post());
                    if($modelPatient->save() == false){
                        return $this->render('create', [
                            'model' => $model,
                            'modelPatient' => $modelPatient,
                        ]);
                    }
                    $model->patient_id = $modelPatient->id;
                // }

                $model->save(false);

                return $this->redirect(['index']);
            } else {
                return $this->render($view, [
                    'model' => $model,
                    'modelPatient' => $modelPatient,
                ]);
            }
        }
    }
    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    

    /**
     * Delete an existing Help model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-help-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Help model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-help-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Help model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @return Help the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Help::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
