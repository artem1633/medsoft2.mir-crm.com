<?php 

namespace app\controllers;

use app\generators\ControllerGeneratorOld;
use app\generators\Generator;
use app\generators\ModelGenerator;
use app\generators\SearchModelGenerator;
use app\models\ApplicationFormStage;
use app\models\forms\ForgetPasswordForm;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use app\models\Question;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;
use app\helpers\TagHelper;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//     public function actionTest()
//     {
//         $url = "https://www.ozon.ru/search/?from_global=true&text=%D0%BA%D0%BB%D1%8E%D1%87%D0%BD%D0%B8%D1%86%D0%B0";

//         $curl = curl_init();

//         curl_setopt_array($curl, array(
//             CURLOPT_URL => $url,
//             CURLOPT_RETURNTRANSFER => true,
//             CURLOPT_ENCODING => '',
//             CURLOPT_MAXREDIRS => 10,
//             CURLOPT_TIMEOUT => 0,
//             CURLOPT_FOLLOWLOCATION => true,
//             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//             CURLOPT_CUSTOMREQUEST => 'GET',
//         ));

//         $response = curl_exec($curl);

//         curl_close($curl);
// //        $response;
//         var_dump($response);
//     }

    public function actionTest()
    {
        $content = $this->renderPartial('@app/web/help-out.php');

        return $content;
    }

    public function actionTestPcr()
    {
        $content = $this->renderPartial('@app/web/help-pcr.php');

        return $content;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest == false){
            if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
                return $this->redirect(['candidate/index']);
            }

            if(Yii::$app->user->identity->role == User::ROLE_CANDIDATE){
                return $this->redirect(['user/profile']);
            }
        }

        return $this->render('index');
    }

    public function actionForgetPassword()
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = Yii::$app->request;
        $model = new ForgetPasswordForm();


        if($model->load($request->post()) && $model->changePassword()){
            Yii::$app->session->setFlash('success', 'A new password has been sent to your email address');

            return $this->redirect('login');
        } else {
            if ($model->hasErrors()){
                Yii::$app->session->setFlash('error', 'Email not found');
            }
            return $this->render('forget-password', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cancel', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Update', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cancel', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Update', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }

        return [];
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {

            $model->scenario = \yii\helpers\ArrayHelper::getValue($_POST, 'LoginForm.scenario');


            $session = Yii::$app->session;

            if($model->scenario == LoginForm::SCENARIO_DEFAULT){
                if($model->validate()){
                    $model->scenario = LoginForm::SCENARIO_PHONE;

                    $code = $this->generateCode();
                    $session->set('security-login-code', $code);

                    $ch = curl_init("https://sms.ru/sms/send");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                        "api_id" => "DAC6A55F-C86B-BD2A-3865-B36CBEAA1BCD",
                        "to" => $model->username,
                        "msg" => "Ваш код подтверждения: ".$code,
                        "json" => 1
                    )));
                    $body = curl_exec($ch);
                    \Yii::warning($body, 'Response from sms.ru API server');
                    curl_close($ch);

                    return $this->render('login', [
                            'model' => $model,
                        ]);
                }
            } elseif($model->scenario == LoginForm::SCENARIO_PHONE){
                if($model->login()){
                    $session->remove('securite-login-code');
                    return $this->goBack();
                } else {
                    return $this->render('login', [
                        'model' => $model,
                    ]);
                }
            }


                // $model->login()

            //Кандидату пользователя проставляем дату входа
            // return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    private function generateCode()
    {
        $code = (string) rand(0, 9999);

        if(strlen($code) < 4){
            for ($i=0; $i < 4 - strlen($code); $i++) { 
                $code = '0'.$code;
            }
        }

        return $code;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionRegister()
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Вы успешно зарегистрировались');
            (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();
            return $this->goHome();
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }
    
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    /**
//     * Displays contact page.
//     *
//     * @return string
//     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    /*
     * Смена локали
     */
    public function actionChangeLocale()
    {
        $locale = Yii::$app->request->get('locale');
        Yii::$app->session->set('language', $locale);

        $locale = Yii::$app->session->get('language');
        if ($locale != null)
            Yii::$app->language = $locale;

        return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }

    public function actionHelp()
    {
        return $this->render('help');
    }

    public function actionUpdateHelp()
    {
        $request = Yii::$app->request;

        $model = Settings::findByKey('help_text');


        if ($model->load($request->post()) && $model->save()){
            return $this->redirect('help');
        }

        return $this->render('_help_form', [
            'model' => $model,
        ]);

    }

   }
