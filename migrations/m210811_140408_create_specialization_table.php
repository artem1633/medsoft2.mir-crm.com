<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_140408_create_specialization_table`.
 */
class m210811_140408_create_specialization_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('specialization', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
        ]);

        

$this->insert('specialization', [ 'name' => 'Уролог' ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('specialization');
    }
}
