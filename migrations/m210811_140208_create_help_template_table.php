<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_140208_create_help_template_table`.
 */
class m210811_140208_create_help_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('help_template', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'verstka' => $this->binary()->comment('Верстка'),
        ]);

        

$this->insert('help_template', [ 'name' => 'Справка ПЦР', 'verstka' => '
<div class="container">
<div class="qr">
	<img width="80px" class="QRImage" />
    <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js"></script>

  <script src=\'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\'></script>
      <script id="rendered-js" >
$(".QRImage").attr("src", "https://chart.apis.google.com/chart?cht=qr&chl=http://medsoft.mir-crm.com/api/check/info?id={id}&chs=150x150&chld=H|0");
//# sourceURL=pen.js
    </script>

</div>	
	
	
	<div class="logo"><img src="/{firm.logo}"></div>
<div class="cnt">	
<h3>Результат исследования <b>№00{id}</b> от {data_vydachi}</h3>	
	<div class="dhd">
	<p><b>ЛО-77-01-014944 от 04.10.2017 ООО “Лидер-1”</b><br>
срок действия лицензии бессрочно<br>
г. Москва, ул. Рудневка, 4<br>
Тел.: +7 (495) 902-77-88<br>
www.drclinica.ru</p>
</div>	
<div class="dleft">
	<p>Пациент: <b>{patient.name}</b><br>
Дата рождения: <b>{patient.god_rojdeniya}</b><br>
Пол: <b>{patient.pol}</b><br>
Дата регистрации: <b>{patient.zaregistrirovan}</b><br>
Дата взятия биоматериала: <b>{data_vzyatiya_biomateriala}</b><br>
Время взятия биоматериала: <b>{vremya_vzyatiya_biomateriala}</b><br>
Биоматериал: <b>Мазок из зева и носа</b>
</p>
</div>
<div class="dright">
	<p>Отделение:<br> 
Врач/№ карты: <br>
Страховая компания: <br>
№ страхового полиса:
</p>
</div>
<div style="clear: both;"></div>
<table>
	<tr>
		<td><b>Исследование</b></td>
		<td><b>Результат</b></td>
		<td><b>Норма</b></td>
	</tr>
	<tr>
		<td>Коронавирус, PHK (SARS-Cov-2, ПЦР)</td>
		<td>НЕ ОБНАРУЖЕНО</td>
		<td>НЕ ОБНАРУЖЕНО</td>
	</tr>
</table>

<div class="dleft2">
<p><b>Комментарий:</b><br>
Исследования проводил: Непомнящая Ю.М<br>
Выпускающий врач: Непомнящая Ю.М., Пронькина О.В.

</p>
</div>
<div class="dright2">
	<p>Дата: {data_vydachi}<br>
Время: {vremya_vydachi}
</p>
</div>
</div>
<div style="clear: both; position: absolute; bottom: 80px; right: 80px; transform: rotate( 
-0.02turn
 );">
<img src="https://drclinica.ru/data/lider1.png" width="240px"></div>
</div>

<div style="margin: 30px 0"></div>
<!-- English -->
<div class="container">
	
<div class="qr">
	<img width="80px" class="QRImage" />
    <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js"></script>

  <script src=\'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\'></script>
      <script id="rendered-js" >
$(".QRImage").attr("src", "https://chart.apis.google.com/chart?cht=qr&chl=https%3A%2F%2Fdrclinica.ru%2Fpcrblank%2F{id}>&chs=150x150&chld=H|0");
//# sourceURL=pen.js
    </script>

</div>	


	
	<div class="logo"><img src="/{firm.logo}"></div>
<div class="cnt">	
<h3>Results of research <b>№00{id}</b> from {data_vydachi}</h3>	
	<div class="dhd">
	<p><b>LO-77-01-014944 from 04.10.2017 LLC “LIDER-1”</b><br>
Moscow, st. Rudnevka, 4<br>
Phone: +7 (495) 902-77-88<br>
www.drclinica.ru</p>
</div>	
<div class="dleft">
	<p>A patient: <b></b><br>
Date of birth: <b></b><br>
Sex: <b></b><br>
Date of registration: <b>{patient.zaregistrirovan}</b><br>
Date of taking biomaterial: <b>{data_vzyatiya_biomateriala}</b><br>
Time of taking biomaterial: <b>{vremya_vzyatiya_biomateriala}</b><br>
Biomaterial: <b>Swab from throat and nose</b>
</p>
</div>
<div class="dright">
	<p>Organization:<br>
Doctor/card number:<br>
Insurance Company:

</p>
</div>
<div style="clear: both;"></div>
<table>
	<tr>
		<td><b>Test</b></td>
		<td><b>Result</b></td>
		<td><b>Standart</b></td>
	</tr>
	<tr>
		<td>Coronavirus, RNA (SARS-Cov-2, PCR)</td>
		<td>NEGATIVE</td>
		<td>NEGATIVE</td>
	</tr>
</table>

<div class="dleft2">
<p><b>Comments:</b><br>
Research conducted by: Nepomnyashchaya Yu.M<br>
Graduating physician: Nepomnyashchaya Yu.M., Pronkina O.V.

</p>
</div>
<div class="dright2">
	<p>Date of issue: {data_vzyatiya_biomateriala}<br>
Time: {vremya_vzyatiya_biomateriala}</p>
</div>
</div>
<div style="clear: both; position: absolute; bottom: 80px; right: 80px; transform: rotate( 
-0.025turn
 );">
<img src="https://drclinica.ru/data/lider1.png" width="240px"></div>
</div>

<div style="margin: 30px 0"></div>
<!-- Check -->
<div class="container" style="padding: 30px; ">
<div style="clear: both; position: absolute; top: 230px; right: 280px; transform: rotate( -0.012turn );">
<img src="https://drclinica.ru/data/lider2.png" width="130px"></div>
		<div class="check">
		<div class="incheck">
ООО Фирма “ЛИДЕР-1”<br>
г. Москва, ул. Рудневка, 4<br>
Тел.: +7 (495) 902-77-88</p>
<br><br>
<b>КВИТАНЦИЯ</b><br>
к приходному кассому ордеру №{id}<br>
от {data_vydachi}<br>
Принято от: {patient.name}<br>
Основание: Анализ ПЦР<br>
Сумма: {amounts} Р.<br><br><br>
М.П (штампа)<br>
Гл.бухгалтер _____________________________<br>
Кассир. __________________________________<br>
</div></div>

</div>
' ]);


$this->insert('help_template', [ 'name' => 'Медотвод', 'verstka' => '
<div class="container">
<div class="qr">
	<img width="80px" class="QRImage" />
    <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js"></script>

  <script src=\'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\'></script>
      <script id="rendered-js" >
$(".QRImage").attr("src", "https://chart.apis.google.com/chart?cht=qr&chl=http://medsoft.mir-crm.com/api/check/info?id={id}&chs=150x150&chld=H|0");
//# sourceURL=pen.js
    </script>

</div>	
	

<div class="logo"><img src="/{firm.logo}"></div>
<div class="dhd">
	<b>ЛО-77-01-014944 от 04.10.2017 ООО “Лидер-1”</b><br>
срок действия лицензии бессрочно<br>
г. Москва, ул. Рудневка, 4<br>
Тел.: +7 (495) 902-77-88<br>
www.drclinica.ru</p>
</div>	
<div class="cnt">	
<h3>Справка <b style=" border-bottom: 1px solid; ">№ 00{id}</b></h3>	

<div class="dddd">
<table class="tableform">
    <tr>
        <td colspan="4"><b>Ф.И.О</b> <span style="width: 577px;">{patient.name}</span></td>
    </tr>
    <tr>
        <td colspan="3" width="40%;"><b>Возраст</b> <span style="width: 175px;">{patient.god_rojdeniya}</span></td>
        <td><b>Выдана</b> <span style="width: 302px;">по месту требования</span></td>
    </tr>
</table>
<table class="tableform" style="width: 635px; margin-top: 50px;">
	<tr>
        <td colspan="4"><b>Заключение и рекомендации</b></td>        
    </tr>
    <tr>
	    <td colspan="4"><p>?????</p></td>
    </tr>
    <tr>
	    <td colspan="4"><p>Мед.отвод ?????? от вакцинации.</p></td>
    </tr>
</table>
<table class="tableform" style="margin-top: 50px;">
    <tr>
		<td colspan="1" width="60%;"><b>Дата</b> <span style="width: 150px;">{data_vzyatiya_biomateriala}</span></td>
<td colspan="1" width="60%;"><b>Врач</b> <span style="width: 195px;">{user.name}</span></td>
    </tr>
</table>

<table class="tableform" style="margin-top: 50px;">
    <tr>
        <td colspan="4"><b>М.П</td>
        
    </tr>
</table>
<div style="clear: both; position: absolute; bottom: 50px; right: 80px;">
<img src="https://drclinica.ru/data/lider1.png" width="240px"></div>
</div>
</div>
</div>
<div style="margin: 30px 0"></div>
<!-- Check -->
<div class="container" style="padding: 30px; ">
<div style="clear: both; position: absolute; top: 230px; right: 280px; transform: rotate( -0.012turn );">
<img src="https://drclinica.ru/data/lider2.png" width="130px"></div>
		<div class="check">
		<div class="incheck">
ООО Фирма “ЛИДЕР-1”<br>
г. Москва, ул. Рудневка, 4<br>
Тел.: +7 (495) 902-77-88</p>
<br><br>
<b>КВИТАНЦИЯ</b><br>
к приходному кассому ордеру №{id}<br>
от {data_vydachi}<br>
Принято от: {patient.name}<br>
Основание: Анализ ПЦР<br>
Сумма: {amounts} Р.<br><br><br>
М.П (штампа)<br>
Гл.бухгалтер _____________________________<br>
Кассир. __________________________________<br>
</div></div>

</div>
' ]);



$this->insert('help_template', [ 'name' => 'Справка Антитела', 'verstka' => '
<div class="container">
<div class="qr">
	<img width="80px" class="QRImage" />
    <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js"></script>

  <script src=\'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\'></script>
      <script id="rendered-js" >
$(".QRImage").attr("src", "https://chart.apis.google.com/chart?cht=qr&chl=http://medsoft.mir-crm.com/api/check/info?id={id}&chs=150x150&chld=H|0");
//# sourceURL=pen.js
    </script>

</div>	
	
	
	<div class="logo"><img src="/{firm.logo}"></div>
<div class="cnt">	
<h3>Результат исследования <b>№00{id}</b> от {data_vzyatiya_biomateriala}</h3>	
	<div class="dhd">
<p><b>ЛО-77-01-014944 от 04.10.2017 ООО “Лидер-1”</b><br>
срок действия лицензии бессрочно<br>
г. Москва, ул. Рудневка, 4<br>
Тел.: +7 (495) 902-77-88<br>
www.drclinica.ru</p>
</div>	
<div class="dleft">
	<p>Пациент: <b>{patient.name}</b><br>
Дата рождения: <b>{patient.god_rojdeniya}</b><br>
Пол: <b>{patient.pol}</b><br>
Дата регистрации: <b>{patient.zaregistrirovan}</b><br>
Дата взятия биоматериала: <b>{data_vzyatiya_biomateriala}</b><br>
Время взятия биоматериала: <b>{vremya_vzyatiya_biomateriala}</b><br>
Биоматериал: <b>Сыворотка крови</b>
</p>
</div>
<div class="dright">
	<p>Отделение:<br> 
Врач/№ карты: <br>
Страховая компания: <br>
№ страхового полиса:
</p>
</div>
<div style="clear: both;"></div>
<table>
	<tr>
		<td><b>Исследование</b></td>
		<td><b>Результат</b></td>
	</tr>
	<tr>
		<td>{zaklyuchenie}</td>
		<td>{zaklyuchenie}</td>
	</tr>
</table>

<div class="dleft2">
<p><b>Комментарий:</b><br>
Исследования проводил: Непомнящая Ю.М<br>
Выпускающий врач: Непомнящая Ю.М., Пронькина О.В.
</p>
</div>
<div class="dright2">
	<p>Дата: {data_vzyatiya_biomateriala}<br>
Время: {vremya_vydachi}
</p>
</div>
<div style=" clear: both; position: absolute; bottom: 100px; right: 80px;">
<img src="https://drclinica.ru//data/lider1.png" width="240px"></div>
</div>
</div>
<div style="margin: 30px 0"></div>
<!-- Check -->
<div class="container" style="padding: 30px; ">
<div style="clear: both; position: absolute; top: 230px; right: 280px; transform: rotate( -0.012turn );">
<img src="https://drclinica.ru/data/lider2.png" width="130px"></div>
		<div class="check">
		<div class="incheck">
ООО Фирма “ЛИДЕР-1”<br>
г. Москва, ул. Рудневка, 4<br>
Тел.: +7 (495) 902-77-88</p>
<br><br>
<b>КВИТАНЦИЯ</b><br>
к приходному кассому ордеру №{id}<br>
от {data_vydachi}<br>
Принято от: {patient.name}<br>
Основание: Анализ ПЦР<br>
Сумма: {amounts} Р.<br><br><br>
М.П (штампа)<br>
Гл.бухгалтер _____________________________<br>
Кассир. __________________________________<br>
</div></div>

</div>
' ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('help_template');
    }
}
