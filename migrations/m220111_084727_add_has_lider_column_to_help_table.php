<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%help}}`.
 */
class m220111_084727_add_has_lider_column_to_help_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('help', 'has_lider', $this->boolean()->defaultValue(false)->comment('Ставить печать'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('help', 'has_lider');
    }
}
