<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141309_create_store_table`.
 */
class m210811_141309_create_store_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
        ]);

        

$this->insert('store', [
            'name' => 'Склад 1',
        ]);
$this->insert('store', [
            'name' => 'Склад 2',
        ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('store');
    }
}
