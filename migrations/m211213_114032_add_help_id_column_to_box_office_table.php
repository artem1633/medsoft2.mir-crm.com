<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%box_office}}`.
 */
class m211213_114032_add_help_id_column_to_box_office_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('box_office', 'help_id', $this->integer()->comment('Справка'));

        $this->createIndex(
            'idx-box_office-help_id',
            'box_office',
            'help_id'
        );

        $this->addForeignKey(
            'fk-box_office-help_id',
            'box_office',
            'help_id',
            'help',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-box_office-help_id',
            'box_office'
        );

        $this->dropIndex(
            'idx-box_office-help_id',
            'box_office'
        );

        $this->dropColumn('box_office', 'help_id');
    }
}
