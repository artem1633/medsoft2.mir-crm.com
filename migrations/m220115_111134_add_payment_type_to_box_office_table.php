<?php

use yii\db\Migration;

/**
 * Class m220115_111134_add_payment_type_to_box_office_table
 */
class m220115_111134_add_payment_type_to_box_office_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('box_office', 'payment_type', $this->string()->comment('Способ оплаты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('box_office', 'payment_type');
    }
}
