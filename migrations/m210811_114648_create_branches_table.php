<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_114648_create_branches_table`.
 */
class m210811_114648_create_branches_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('branches', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'logo' => $this->string()->comment('Логотип'),
            'tel_1' => $this->string()->comment('Тел 1'),
            'tel_2' => $this->string()->comment('Тел 2'),
            'address' => $this->string()->comment('Адрес'),
            'firm_id' => $this->integer()->comment('Фирма'),
            'grafik_raboty' => $this->string()->comment('График работы'),
        ]);

        $this->createIndex(
            'idx-branches-firm_id',
            'branches',
            'firm_id'
        );
                        
        $this->addForeignKey(
            'fk-branches-firm_id',
            'branches',
            'firm_id',
            'firm',
            'id',
            'SET NULL'
        );
                        

$this->insert('branches', [ 'name' => 'Филиал основной' ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-branches-firm_id',
            'branches'
        );
                        
        $this->dropIndex(
            'idx-branches-firm_id',
            'branches'
        );
                        
                        
        $this->dropTable('branches');
    }
}
