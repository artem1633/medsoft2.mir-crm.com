<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_142209_create_store_salvage_item_table`.
 */
class m210811_142209_create_store_salvage_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_salvage_item', [
            'id' => $this->primaryKey(),
            'tovar_id' => $this->integer()->comment('Товар'),
            'amount' => $this->double()->comment('Кол-во'),
            'store_in' => $this->integer()->comment('Поступление'),
            'mesto_na_sklade' => $this->integer()->comment('Место на складе'),
        ]);

        $this->createIndex(
            'idx-store_salvage_item-tovar_id',
            'store_salvage_item',
            'tovar_id'
        );
                        
        $this->addForeignKey(
            'fk-store_salvage_item-tovar_id',
            'store_salvage_item',
            'tovar_id',
            'products',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_salvage_item-store_in',
            'store_salvage_item',
            'store_in'
        );
                        
        $this->addForeignKey(
            'fk-store_salvage_item-store_in',
            'store_salvage_item',
            'store_in',
            'store_salvage',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_salvage_item-mesto_na_sklade',
            'store_salvage_item',
            'mesto_na_sklade'
        );
                        
        $this->addForeignKey(
            'fk-store_salvage_item-mesto_na_sklade',
            'store_salvage_item',
            'mesto_na_sklade',
            'place_stock',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-store_salvage_item-tovar_id',
            'store_salvage_item'
        );
                        
        $this->dropIndex(
            'idx-store_salvage_item-tovar_id',
            'store_salvage_item'
        );
                        
                        $this->dropForeignKey(
            'fk-store_salvage_item-store_in',
            'store_salvage_item'
        );
                        
        $this->dropIndex(
            'idx-store_salvage_item-store_in',
            'store_salvage_item'
        );
                        
                        $this->dropForeignKey(
            'fk-store_salvage_item-mesto_na_sklade',
            'store_salvage_item'
        );
                        
        $this->dropIndex(
            'idx-store_salvage_item-mesto_na_sklade',
            'store_salvage_item'
        );
                        
                        
        $this->dropTable('store_salvage_item');
    }
}
