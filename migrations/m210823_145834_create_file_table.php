<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210823_145834_create_file_table`.
 */
class m210823_145834_create_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'folder' => $this->string()->comment('Папка'),
            'subfolder' => $this->string()->comment('Подпапка'),
            'type' => $this->string()->comment('Тип'),
            'name' => $this->string()->comment('Наименование'),
            'size' => $this->string()->comment('Размер'),
            'extension' => $this->string()->comment('Расширение'),
            'user_id' => $this->integer()->comment('Сотрудник'),
            'datetime' => $this->datetime()->comment('Загружен'),
            'opisanie_sten' => $this->string()->comment('Описание'),
            'path' => $this->string()->comment('Путь'),
        ]);

        $this->createIndex(
            'idx-file-user_id',
            'file',
            'user_id'
        );
                        
        $this->addForeignKey(
            'fk-file-user_id',
            'file',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-file-user_id',
            'file'
        );
                        
        $this->dropIndex(
            'idx-file-user_id',
            'file'
        );
                        
                        
        $this->dropTable('file');
    }
}
