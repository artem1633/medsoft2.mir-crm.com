<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141509_create_patient_table`.
 */
class m210811_141509_create_patient_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('patient', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('ФИО'),
            'god_rojdeniya' => $this->date()->comment('Год рождения'),
            'phone' => $this->string()->comment('Телефон'),
            'posledniy_vizit' => $this->datetime()->comment('Последний визит'),
            'zaregistrirovan' => $this->datetime()->comment('Зарегистрирован'),
            'branche_id' => $this->integer()->comment('Филиал'),
            'pol' => $this->string()->comment('Пол'),
        ]);

        $this->createIndex(
            'idx-patient-branche_id',
            'patient',
            'branche_id'
        );
                        
        $this->addForeignKey(
            'fk-patient-branche_id',
            'patient',
            'branche_id',
            'branches',
            'id',
            'SET NULL'
        );
                        

$this->insert('patient', [ 'name' => 'Пациент 1']);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-patient-branche_id',
            'patient'
        );
                        
        $this->dropIndex(
            'idx-patient-branche_id',
            'patient'
        );
                        
                        
        $this->dropTable('patient');
    }
}
