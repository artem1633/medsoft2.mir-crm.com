<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141109_create_ed_table`.
 */
class m210811_141109_create_ed_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ed', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);

        

$this->insert('ed', [
            'name' => 'кг',
        ]);
$this->insert('ed', [
            'name' => 'гр',
        ]);
$this->insert('ed', [
            'name' => 'мл',
        ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('ed');
    }
}
