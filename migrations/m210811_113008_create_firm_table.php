<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_113008_create_firm_table`.
 */
class m210811_113008_create_firm_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('firm', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'logo' => $this->string()->comment('Логотип'),
            'licenziya' => $this->string()->comment('Лицензия'),
            'organizaciya' => $this->string()->comment('Организация'),
            'create_at' => $this->date()->comment('Дата'),
            'srok' => $this->string()->comment('Срок'),
            'address' => $this->string()->comment('Адрес'),
            'tel' => $this->string()->comment('Тел'),
            'sayt' => $this->string()->comment('Сайт'),
            'license' => $this->string()->comment('License'),
            'date' => $this->string()->comment('Date'),
            'name' => $this->string()->comment('Name'),
            'adress' => $this->string()->comment('Adress'),
            'phone' => $this->string()->comment('Phone'),
            'site' => $this->string()->comment('Site'),
            'pechat' => $this->string()->comment('Печать'),
            'pechat_kvitanciyu' => $this->string()->comment('Печать квитанцию'),
        ]);

        

$this->insert('firm', [ 'name' => 'ООО Фирм 1' ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('firm');
    }
}
