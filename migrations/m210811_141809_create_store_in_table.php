<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141809_create_store_in_table`.
 */
class m210811_141809_create_store_in_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_in', [
            'id' => $this->primaryKey(),
            'nomer_zakaza' => $this->string()->comment('Номер заказа'),
            'warehouse_id' => $this->integer()->comment('Склад'),
            'status' => $this->string()->comment('Статус'),
            'create_at' => $this->datetime()->comment('Дата'),
            'the_supplier_id' => $this->integer()->comment('Поставщик'),
            'vkladka' => $this->string()->comment('Вкладка'),
        ]);

        $this->createIndex(
            'idx-store_in-warehouse_id',
            'store_in',
            'warehouse_id'
        );
                        
        $this->addForeignKey(
            'fk-store_in-warehouse_id',
            'store_in',
            'warehouse_id',
            'store',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_in-the_supplier_id',
            'store_in',
            'the_supplier_id'
        );
                        
        $this->addForeignKey(
            'fk-store_in-the_supplier_id',
            'store_in',
            'the_supplier_id',
            'suppliers',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-store_in-warehouse_id',
            'store_in'
        );
                        
        $this->dropIndex(
            'idx-store_in-warehouse_id',
            'store_in'
        );
                        
                        $this->dropForeignKey(
            'fk-store_in-the_supplier_id',
            'store_in'
        );
                        
        $this->dropIndex(
            'idx-store_in-the_supplier_id',
            'store_in'
        );
                        
                        
        $this->dropTable('store_in');
    }
}
