<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_140608_create_services_table`.
 */
class m210811_140608_create_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('services', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'price' => $this->double()->comment('Стоимость'),
            'artikul' => $this->string()->comment('Артикул'),
            'kod_804h' => $this->string()->comment('Код 804h'),
            'category_id' => $this->integer()->comment('Категория'),
            'specialnost' => $this->integer()->comment('Специальность'),
            'dlitelnost' => $this->double()->comment('Длительность'),
        ]);

        $this->createIndex(
            'idx-services-category_id',
            'services',
            'category_id'
        );
                        
        $this->addForeignKey(
            'fk-services-category_id',
            'services',
            'category_id',
            'service_category',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-services-specialnost',
            'services',
            'specialnost'
        );
                        
        $this->addForeignKey(
            'fk-services-specialnost',
            'services',
            'specialnost',
            'specialization',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-services-category_id',
            'services'
        );
                        
        $this->dropIndex(
            'idx-services-category_id',
            'services'
        );
                        
                        $this->dropForeignKey(
            'fk-services-specialnost',
            'services'
        );
                        
        $this->dropIndex(
            'idx-services-specialnost',
            'services'
        );
                        
                        
        $this->dropTable('services');
    }
}
