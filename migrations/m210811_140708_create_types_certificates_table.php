<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_140708_create_types_certificates_table`.
 */
class m210811_140708_create_types_certificates_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('types_certificates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'price' => $this->double()->comment('Стоимость '),
            'agentu' => $this->double()->comment('Агенту'),
            'zaklyuchenie' => $this->text()->comment('Заключение'),
            'vozmojnost_udalyat' => $this->boolean()->comment('Запрет удалять'),
            'shablon_qr_id' => $this->integer()->comment('Шаблон qr'),
            'shablon_spravki_id' => $this->integer()->comment('Шаблон справки'),
        ]);

        $this->createIndex(
            'idx-types_certificates-shablon_qr_id',
            'types_certificates',
            'shablon_qr_id'
        );
                        
        $this->addForeignKey(
            'fk-types_certificates-shablon_qr_id',
            'types_certificates',
            'shablon_qr_id',
            'template_qr',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-types_certificates-shablon_spravki_id',
            'types_certificates',
            'shablon_spravki_id'
        );
                        
        $this->addForeignKey(
            'fk-types_certificates-shablon_spravki_id',
            'types_certificates',
            'shablon_spravki_id',
            'help_template',
            'id',
            'SET NULL'
        );
                        

$this->insert('types_certificates', [ 'name' => 'ПЦР тест','shablon_spravki_id' =>1,'shablon_qr_id' =>1,'vozmojnost_udalyat'=>true ]);
$this->insert('types_certificates', [ 'name' => 'Медотвод','shablon_spravki_id' =>2,'shablon_qr_id' =>2,'vozmojnost_udalyat'=>true ]);
$this->insert('types_certificates', [ 'name' => 'Справка антитела','shablon_spravki_id' =>3,'shablon_qr_id' =>3,'vozmojnost_udalyat'=>true ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-types_certificates-shablon_qr_id',
            'types_certificates'
        );
                        
        $this->dropIndex(
            'idx-types_certificates-shablon_qr_id',
            'types_certificates'
        );
                        
                        $this->dropForeignKey(
            'fk-types_certificates-shablon_spravki_id',
            'types_certificates'
        );
                        
        $this->dropIndex(
            'idx-types_certificates-shablon_spravki_id',
            'types_certificates'
        );
                        
                        
        $this->dropTable('types_certificates');
    }
}
