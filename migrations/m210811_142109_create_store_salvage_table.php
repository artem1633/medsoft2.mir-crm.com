<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_142109_create_store_salvage_table`.
 */
class m210811_142109_create_store_salvage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_salvage', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->comment('Склад'),
            'status' => $this->string()->comment('Статус'),
            'create_at' => $this->datetime()->comment('Дата'),
            'vkladka' => $this->string()->comment('Вкладка'),
        ]);

        $this->createIndex(
            'idx-store_salvage-warehouse_id',
            'store_salvage',
            'warehouse_id'
        );
                        
        $this->addForeignKey(
            'fk-store_salvage-warehouse_id',
            'store_salvage',
            'warehouse_id',
            'store',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-store_salvage-warehouse_id',
            'store_salvage'
        );
                        
        $this->dropIndex(
            'idx-store_salvage-warehouse_id',
            'store_salvage'
        );
                        
                        
        $this->dropTable('store_salvage');
    }
}
