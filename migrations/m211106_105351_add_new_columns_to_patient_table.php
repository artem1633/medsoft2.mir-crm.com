<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%patient}}`.
 */
class m211106_105351_add_new_columns_to_patient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('patient', 'last_name', $this->string()->comment('Фамилия'));
        $this->addColumn('patient', 'patronymic', $this->string()->comment('Отчество'));
        $this->addColumn('patient', 'name_en', $this->string()->comment('ФИО латиницей'));
        // $this->addColumn('patient', 'date_bio', $this->date()->comment('Дата взятия биоматериала'));
        // $this->addColumn('patient', 'time_bio', $this->date()->comment('Время взятия биоматериала'));
        // $this->addColumn('patient', 'date_out', $this->date()->comment('Дата выдачи'));
        // $this->addColumn('patient', 'time_out', $this->date()->comment('Время выдачи'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('patient', 'last_name');
        $this->dropColumn('patient', 'patronymic');
        // $this->dropColumn('patient', 'name_en');
        // $this->dropColumn('patient', 'date_bio');
        // $this->dropColumn('patient', 'time_bio');
        // $this->dropColumn('patient', 'date_out');
        // $this->dropColumn('patient', 'time_out');
    }
}
