<?php

use yii\db\Migration;

/**
 * Class m220121_152633_add_name_en_to_firm_table
 */
class m220121_152633_add_name_en_to_firm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('firm', 'name_en', $this->string()->after('name')->comment('Наименование на английском'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('firm', 'name_en');
    }
}
