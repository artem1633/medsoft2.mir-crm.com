<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%patient}}`.
 */
class m220121_122034_add_new_columns_to_patient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('patient', 'passport', $this->string()->comment('Паспорт'));
        $this->addColumn('patient', 'address', $this->string()->comment('Адрес'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('patient', 'passport');
        $this->dropColumn('patient', 'address');
    }
}
