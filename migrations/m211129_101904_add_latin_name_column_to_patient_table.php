<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%patient}}`.
 */
class m211129_101904_add_latin_name_column_to_patient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('patient', 'latin_name', $this->string()->comment('Имя латиницей'));
    	$this->addColumn('patient', 'discovered', $this->integer()->comment('Обнаружен'));
    	$this->addColumn('help', 'conclusion', $this->integer()->comment('Заключение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('help', 'conclusion');
    	$this->dropColumn('patient', 'discovered');
    	$this->dropColumn('patient', 'latin_name');
    }
}
