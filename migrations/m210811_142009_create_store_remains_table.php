<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_142009_create_store_remains_table`.
 */
class m210811_142009_create_store_remains_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_remains', [
            'id' => $this->primaryKey(),
            'vkladka' => $this->string()->comment('Вкладка'),
            'warehouse_id' => $this->integer()->comment('Склад'),
            'tovar_id' => $this->integer()->comment('Товар'),
            'amount' => $this->double()->comment('Кол-во'),
            'mesto_na_sklade' => $this->integer()->comment('Место на складе'),
            'price' => $this->string()->comment('Стоимость'),
        ]);

        $this->createIndex(
            'idx-store_remains-warehouse_id',
            'store_remains',
            'warehouse_id'
        );
                        
        $this->addForeignKey(
            'fk-store_remains-warehouse_id',
            'store_remains',
            'warehouse_id',
            'store',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_remains-tovar_id',
            'store_remains',
            'tovar_id'
        );
                        
        $this->addForeignKey(
            'fk-store_remains-tovar_id',
            'store_remains',
            'tovar_id',
            'products',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_remains-mesto_na_sklade',
            'store_remains',
            'mesto_na_sklade'
        );
                        
        $this->addForeignKey(
            'fk-store_remains-mesto_na_sklade',
            'store_remains',
            'mesto_na_sklade',
            'place_stock',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-store_remains-warehouse_id',
            'store_remains'
        );
                        
        $this->dropIndex(
            'idx-store_remains-warehouse_id',
            'store_remains'
        );
                        
                        $this->dropForeignKey(
            'fk-store_remains-tovar_id',
            'store_remains'
        );
                        
        $this->dropIndex(
            'idx-store_remains-tovar_id',
            'store_remains'
        );
                        
                        $this->dropForeignKey(
            'fk-store_remains-mesto_na_sklade',
            'store_remains'
        );
                        
        $this->dropIndex(
            'idx-store_remains-mesto_na_sklade',
            'store_remains'
        );
                        
                        
        $this->dropTable('store_remains');
    }
}
