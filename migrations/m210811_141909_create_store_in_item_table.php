<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141909_create_store_in_item_table`.
 */
class m210811_141909_create_store_in_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_in_item', [
            'id' => $this->primaryKey(),
            'tovar_id' => $this->integer()->comment('Товар'),
            'amount' => $this->double()->comment('Кол-во'),
            'the_cost' => $this->double()->comment('Стоимость'),
            'store_in' => $this->integer()->comment('Поступление'),
            'mesto_na_sklade' => $this->integer()->comment('Место на складе'),
            'prinyato' => $this->double()->comment('Принято'),
        ]);

        $this->createIndex(
            'idx-store_in_item-tovar_id',
            'store_in_item',
            'tovar_id'
        );
                        
        $this->addForeignKey(
            'fk-store_in_item-tovar_id',
            'store_in_item',
            'tovar_id',
            'products',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_in_item-store_in',
            'store_in_item',
            'store_in'
        );
                        
        $this->addForeignKey(
            'fk-store_in_item-store_in',
            'store_in_item',
            'store_in',
            'store_in',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-store_in_item-mesto_na_sklade',
            'store_in_item',
            'mesto_na_sklade'
        );
                        
        $this->addForeignKey(
            'fk-store_in_item-mesto_na_sklade',
            'store_in_item',
            'mesto_na_sklade',
            'place_stock',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-store_in_item-tovar_id',
            'store_in_item'
        );
                        
        $this->dropIndex(
            'idx-store_in_item-tovar_id',
            'store_in_item'
        );
                        
                        $this->dropForeignKey(
            'fk-store_in_item-store_in',
            'store_in_item'
        );
                        
        $this->dropIndex(
            'idx-store_in_item-store_in',
            'store_in_item'
        );
                        
                        $this->dropForeignKey(
            'fk-store_in_item-mesto_na_sklade',
            'store_in_item'
        );
                        
        $this->dropIndex(
            'idx-store_in_item-mesto_na_sklade',
            'store_in_item'
        );
                        
                        
        $this->dropTable('store_in_item');
    }
}
