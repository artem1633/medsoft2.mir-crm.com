<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210823_145634_create_box_office_table`.
 */
class m210823_145634_create_box_office_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('box_office', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->comment('Тип'),
            'amounts' => $this->double()->comment('Сумма'),
            'service_id' => $this->integer()->comment('Услуга'),
            'status' => $this->string()->comment('Статус'),
            'comment' => $this->string()->comment('Комментарий'),
            'created_id' => $this->integer()->comment('Создал'),
            'created_at' => $this->datetime()->comment('Создан'),
            'patient_id' => $this->integer()->comment('Пациент'),
            'branch_id' => $this->integer()->comment('Филиал'),
        ]);

        $this->createIndex(
            'idx-box_office-service_id',
            'box_office',
            'service_id'
        );
                        
        $this->addForeignKey(
            'fk-box_office-service_id',
            'box_office',
            'service_id',
            'services',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-box_office-created_id',
            'box_office',
            'created_id'
        );
                        
        $this->addForeignKey(
            'fk-box_office-created_id',
            'box_office',
            'created_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-box_office-patient_id',
            'box_office',
            'patient_id'
        );
                        
        $this->addForeignKey(
            'fk-box_office-patient_id',
            'box_office',
            'patient_id',
            'patient',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-box_office-branch_id',
            'box_office',
            'branch_id'
        );
                        
        $this->addForeignKey(
            'fk-box_office-branch_id',
            'box_office',
            'branch_id',
            'branches',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-box_office-service_id',
            'box_office'
        );
                        
        $this->dropIndex(
            'idx-box_office-service_id',
            'box_office'
        );
                        
                        $this->dropForeignKey(
            'fk-box_office-created_id',
            'box_office'
        );
                        
        $this->dropIndex(
            'idx-box_office-created_id',
            'box_office'
        );
                        
                        $this->dropForeignKey(
            'fk-box_office-patient_id',
            'box_office'
        );
                        
        $this->dropIndex(
            'idx-box_office-patient_id',
            'box_office'
        );
                        
                        $this->dropForeignKey(
            'fk-box_office-branch_id',
            'box_office'
        );
                        
        $this->dropIndex(
            'idx-box_office-branch_id',
            'box_office'
        );
                        
                        
        $this->dropTable('box_office');
    }
}
