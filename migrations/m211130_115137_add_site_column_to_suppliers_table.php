<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%suppliers}}`.
 */
class m211130_115137_add_site_column_to_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('suppliers', 'site', $this->string()->comment('Сайт'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('suppliers', 'site');
    }
}
