<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141209_create_products_table`.
 */
class m210811_141209_create_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'artikul' => $this->string()->comment('Артикул'),
            'kriticheskoe_kol' => $this->double()->comment('Критическое кол-во'),
            'edinica_zapravki' => $this->integer()->comment('ед. из.'),
            'code' => $this->string()->comment('Code'),
        ]);

        $this->createIndex(
            'idx-products-edinica_zapravki',
            'products',
            'edinica_zapravki'
        );
                        
        $this->addForeignKey(
            'fk-products-edinica_zapravki',
            'products',
            'edinica_zapravki',
            'ed',
            'id',
            'SET NULL'
        );
                        

$this->insert('products', [
            'name' => 'Товар 1',
            'edinica_zapravki' => 1,
            'kriticheskoe_kol' => 10,
            'artikul' => '1',
        ]);
$this->insert('products', [
            'name' => 'Товар 2',
            'edinica_zapravki' => 2,
            'kriticheskoe_kol' => 5,
            'artikul' => '2',
        ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-products-edinica_zapravki',
            'products'
        );
                        
        $this->dropIndex(
            'idx-products-edinica_zapravki',
            'products'
        );
                        
                        
        $this->dropTable('products');
    }
}
