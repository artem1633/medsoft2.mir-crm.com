<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%help}}`.
 */
class m211222_162712_add_payment_type_column_to_help_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('help', 'payment_type', $this->integer()->comment('Тип оплаты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('help', 'payment_type');
    }
}
