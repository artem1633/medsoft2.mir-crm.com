<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%box_office}}`.
 */
class m211212_090628_add_specialist_id_column_to_box_office_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('box_office', 'specialist_id', $this->integer()->comment('Специалист'));

        $this->createIndex(
            'idx-box_office-specialist_id',
            'box_office',
            'specialist_id'
        );

        $this->addForeignKey(
            'fk-box_office-specialist_id',
            'box_office',
            'specialist_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-box_office-specialist_id',
            'box_office'
        );

        $this->dropIndex(
            'idx-box_office-specialist_id',
            'box_office'
        );

        $this->dropColumn('box_office', 'specialist_id');
    }
}
