<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141009_create_suppliers_table`.
 */
class m210811_141009_create_suppliers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('suppliers', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'address' => $this->string()->comment('Адрес'),
            'phone' => $this->string()->comment('Телефон'),
        ]);

        

$this->insert('suppliers', [
            'name' => 'Поставщик 2',
        ]);
$this->insert('suppliers', [
            'name' => 'Поставщик 1',
        ]);
$this->insert('suppliers', [
            'name' => 'Поставщик 3',
        ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('suppliers');
    }
}
