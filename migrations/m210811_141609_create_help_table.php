<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141609_create_help_table`.
 */
class m210811_141609_create_help_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('help', [
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer()->comment('Пациент'),
            'type_id' => $this->integer()->comment('Тип'),
            'create_at' => $this->datetime()->comment('Дата и время'),
            'user_id' => $this->integer()->comment('Сотрудник'),
            'agent_id' => $this->integer()->comment('Агент'),
            'oplata' => $this->boolean()->comment('Оплачен'),
            'amounts' => $this->double()->comment('Сумма'),
            'firm_id' => $this->integer()->comment('Фирма'),
            'branche_id' => $this->integer()->comment('Филиал'),
            'qr' => $this->string()->comment('QR'),
            'data_vzyatiya_biomateriala' => $this->date()->comment('Дата взятия биоматериала'),
            'vremya_vzyatiya_biomateriala' => $this->string()->comment('Время взятия биоматериала'),
            'data_vydachi' => $this->date()->comment('Дата выдачи'),
            'vremya_vydachi' => $this->string()->comment('Время выдачи'),
            'vrach_id' => $this->integer()->comment('Врач'),
            'srok' => $this->string()->comment('Срок'),
            'zaklyuchenie' => $this->integer()->comment('Заключение'),
            'kolichestvo' => $this->string()->comment('Количество'),
        ]);

        $this->createIndex(
            'idx-help-patient_id',
            'help',
            'patient_id'
        );
                        
        $this->addForeignKey(
            'fk-help-patient_id',
            'help',
            'patient_id',
            'patient',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-type_id',
            'help',
            'type_id'
        );
                        
        $this->addForeignKey(
            'fk-help-type_id',
            'help',
            'type_id',
            'types_certificates',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-user_id',
            'help',
            'user_id'
        );
                        
        $this->addForeignKey(
            'fk-help-user_id',
            'help',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-agent_id',
            'help',
            'agent_id'
        );
                        
        $this->addForeignKey(
            'fk-help-agent_id',
            'help',
            'agent_id',
            'agents',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-firm_id',
            'help',
            'firm_id'
        );
                        
        $this->addForeignKey(
            'fk-help-firm_id',
            'help',
            'firm_id',
            'firm',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-branche_id',
            'help',
            'branche_id'
        );
                        
        $this->addForeignKey(
            'fk-help-branche_id',
            'help',
            'branche_id',
            'branches',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-vrach_id',
            'help',
            'vrach_id'
        );
                        
        $this->addForeignKey(
            'fk-help-vrach_id',
            'help',
            'vrach_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-help-zaklyuchenie',
            'help',
            'zaklyuchenie'
        );
                        
        $this->addForeignKey(
            'fk-help-zaklyuchenie',
            'help',
            'zaklyuchenie',
            'types_certificates',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-help-patient_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-patient_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-type_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-type_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-user_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-user_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-agent_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-agent_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-firm_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-firm_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-branche_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-branche_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-vrach_id',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-vrach_id',
            'help'
        );
                        
                        $this->dropForeignKey(
            'fk-help-zaklyuchenie',
            'help'
        );
                        
        $this->dropIndex(
            'idx-help-zaklyuchenie',
            'help'
        );
                        
                        
        $this->dropTable('help');
    }
}
