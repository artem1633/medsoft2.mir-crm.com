<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210811_141709_create_honey_books_table`.
 */
class m210811_141709_create_honey_books_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('honey_books', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('ФИО'),
            'data_rojdeniya' => $this->date()->comment('Дата рождения'),
            'city' => $this->string()->comment('Город'),
            'ulica' => $this->string()->comment('Улица'),
            'home_id' => $this->string()->comment('Дом'),
            'obekt' => $this->string()->comment('Объект'),
            'role' => $this->string()->comment('Должность'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('honey_books');
    }
}
