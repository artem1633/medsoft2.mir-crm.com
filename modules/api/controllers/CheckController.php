<?php

namespace app\modules\api\controllers;

use app\components\AdminBalance;
use app\models\MobileUser;
use app\models\Settings;
use app\models\Transaction;
use app\models\TransactionSearch;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use app\models\Group;
use app\models\MobileUserGroup;
use yii\web\UploadedFile;

class CheckController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    // 'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     *
     */
    public function actionInfo($id)
    {

        $template = \app\models\Help::findOne($id);

        if (!isset($template->type->shablon_qr_id)) {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
        $templateHelp = \app\models\TemplateQr::findOne($template->type->shablon_qr_id);

        $content = $template->tags($templateHelp->verstka);

        return $this->renderPartial('index',['content' => $content]);

        $temp = \app\models\TemplateQr::findOne(1);
        // $this->renderPartial('@app/views/_print/print', [
        //     'content' => $content,
        // ]);
        return $this->renderPartial('index',['content' => $temp->verstka]);
    }

    public function actionHelp($id)
    {
        $template = \app\models\Help::findOne($id);

        if (!isset($template->type->shablon_spravki_id)) {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
        $templateHelp = \app\models\HelpTemplate::findOne($template->type->shablon_spravki_id);

        $content = $template->tags($templateHelp->verstka);

        return $this->renderPartial('help',['content' => $content]);
    }

    public function actionPrint($id)
    {
        // $template = \app\models\Help::findOne($id);

        // if (!isset($template->type->shablon_spravki_id)) {
        //     throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        // }
        // $templateHelp = \app\models\HelpTemplate::findOne($template->type->shablon_spravki_id);

        // $content = $template->tags($templateHelp->verstka);

        // return $this->renderPartial('print',['content' => $content]);

        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $certificate = \app\models\TypesCertificates::findOne($model->type_id);
        
        $patient = \app\models\Patient::findOne($model->patient_id);

        $template = \app\models\HelpTemplate::findOne($certificate->shablon_spravki_id);



        $template = \app\models\Help::findOne($id);

        if (!isset($template->type->shablon_spravki_id)) {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
        $templateHelp = \app\models\HelpTemplate::findOne($template->type->shablon_spravki_id);

        $content = $template->tags($templateHelp->verstka);

        return $this->renderPartial('print',['content' => $content]);
    }

    public function actionDown($id)
    {
        $template = \app\models\Help::findOne($id);

        if (!isset($template->type->shablon_spravki_id)) {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
        $templateHelp = \app\models\HelpTemplate::findOne($template->type->shablon_spravki_id);

        $content =  '<style>* { font-family:  DejaVu Sans !important; }</style>'.$template->tags($templateHelp->verstka);
        $content .= $this->renderPartial('help',['content' => $content]);



        $dompdf = new \Dompdf\Dompdf();

        $dompdf->load_html($content, 'utf-8');
        $dompdf->render();

        $pdf = $dompdf->output(); 
        file_put_contents(__DIR__.'schet-10.pdf', $pdf);

        Yii::$app->response->sendFile(__DIR__.'schet-10.pdf');

        // return $this->renderPartial('print',['content' => $content]);
    }

    /**
     *
     */
    public function actionPcr($id)
    {

        $temp = \app\models\TemplateQr::findOne(1);
        // $this->renderPartial('@app/views/_print/print', [
        //     'content' => $content,
        // ]);
        return $this->renderPartial('pcr',['content' => $temp->verstka]);
    }
}