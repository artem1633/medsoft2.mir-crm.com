<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "salary".
*
    * @property int $user_id Сотрудник
    * @property  $amounts Сумма
    * @property  $way Способ
    * @property  $status Статус
    * @property  $create_at Дата и время
    * @property int $branch_id Филиал
*/
class Salary extends \app\base\ActiveRecord
{

        const CASH = 'Нал';
    const NO_CASH = 'Без наз';
    const DONE = 'Оплачен';
    const JOB = 'Не оплаче';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'salary';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::className(), 'targetAttribute' => ['branch_id' => 'id']],
            [['user_id', 'branch_id'], 'integer'],
            [['amounts'], 'number'],
            [['way', 'status', 'create_at'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'Сотрудник'),
            'amounts' => Yii::t('app', 'Сумма'),
            'way' => Yii::t('app', 'Способ'),
            'status' => Yii::t('app', 'Статус'),
            'create_at' => Yii::t('app', 'Дата и время'),
            'branch_id' => Yii::t('app', 'Филиал'),
                                                                
];
    }

    public static function wayLabels() {
        return [
            self::CASH => "Нал",
            self::NO_CASH => "Без наз",
        ];
    }public static function statusLabels() {
        return [
            self::DONE => "Оплачен",
            self::JOB => "Не оплачен",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {

        if($this->isNewRecord){
            $this->create_at = date('Y-m-d H:i:s');
        }
        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }




    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranch()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branch_id']);
    }



    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}