<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "help".
*
    * @property string $test Тестирование
    * @property string $qr QR
    * @property int $patient_id Пациент
    * @property int $type_id Тип
    * @property  $create_at Дата и время
    * @property int $user_id Сотрудник
    * @property int $agent_id Агент
    * @property  $amounts Сумма
    * @property  $data_vzyatiya_biomateriala Дата взятия биоматериала
    * @property string $vremya_vzyatiya_biomateriala Время взятия биоматериала
    * @property  $data_vydachi Дата выдачи
    * @property string $vremya_vydachi Время выдачи
    * @property int $vrach_id Врач
    * @property string $srok Срок
    * @property int $zaklyuchenie Заключение
    * @property string $kolichestvo Количество
    * @property int $firm_id Фирма
    * @property int $branche_id Филиал
    * @property  $oplata Оплачен
*/
class Help extends \yii\db\ActiveRecord
{
    const PAYMENT_TYPE_CASH = 1;
    const PAYMENT_TYPE_CARD = 2;

    public $test;


    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'help';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['amounts', 'data_vzyatiya_biomateriala', 'vremya_vzyatiya_biomateriala,', 'data_vydachi', 'vremya_vydachi'], 'required'],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypesCertificates::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agents::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['vrach_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['vrach_id' => 'id']],
            [['zaklyuchenie'], 'exist', 'skipOnError' => true, 'targetClass' => TypesCertificates::className(), 'targetAttribute' => ['zaklyuchenie' => 'id']],
            [['firm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Firm::className(), 'targetAttribute' => ['firm_id' => 'id']],
            [['branche_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::className(), 'targetAttribute' => ['branche_id' => 'id']],
            [['test', 'qr', 'create_at', 'data_vzyatiya_biomateriala', 'vremya_vzyatiya_biomateriala', 'data_vydachi', 'vremya_vydachi', 'srok', 'kolichestvo'], 'string'],
            [['patient_id', 'type_id', 'user_id', 'agent_id', 'vrach_id', 'zaklyuchenie', 'firm_id', 'branche_id', 'oplata', 'conclusion', 'payment_type', 'has_lider'], 'integer'],
            [['amounts'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'test' => Yii::t('app', 'Тестирование'),
            'qr' => Yii::t('app', 'QR'),
            'patient_id' => Yii::t('app', 'Пациент'),
            'type_id' => Yii::t('app', 'Тип'),
            'create_at' => Yii::t('app', 'Дата и время'),
            'user_id' => Yii::t('app', 'Сотрудник'),
            'agent_id' => Yii::t('app', 'Агент'),
            'amounts' => Yii::t('app', 'Сумма'),
            'data_vzyatiya_biomateriala' => Yii::t('app', 'Дата взятия биоматериала'),
            'vremya_vzyatiya_biomateriala' => Yii::t('app', 'Время взятия биоматериала'),
            'data_vydachi' => Yii::t('app', 'Дата выдачи'),
            'vremya_vydachi' => Yii::t('app', 'Время выдачи'),
            'vrach_id' => Yii::t('app', 'Врач'),
            'srok' => Yii::t('app', 'Срок'),
            'zaklyuchenie' => Yii::t('app', 'Заключение'),
            'kolichestvo' => Yii::t('app', 'Количество'),
            'firm_id' => Yii::t('app', 'Фирма'),
            'branche_id' => Yii::t('app', 'Филиал'),
            'oplata' => Yii::t('app', 'Оплачено'),
            'conclusion' => Yii::t('app', 'Заключение'),
            'payment_type' => Yii::t('app', 'Способ оплаты'),
            'has_lider' => Yii::t('app', 'Ставить печать'),
        ];
    }

    public static function paymentTypeLabels()
    {
        return [
            self::PAYMENT_TYPE_CASH => 'Наличный',
            self::PAYMENT_TYPE_CARD => 'Безналичный',
        ];
    }

    public function getZaklyuchenieLabel()
    {
        $arr = [
            1 => 'РНК НЕ ОБНАРУЖЕНА',
            2 => 'ОБНАРУЖЕНО',
        ];


        return isset($arr[$this->zaklyuchenie]) ? $arr[$this->zaklyuchenie] : null;
    }

    public function getZaklyuchenieSmallLabel()
    {
        $arr = [
            1 => 'НЕ ОБНАРУЖЕНА',
            2 => 'ОБНАРУЖЕНО',
        ];


        return isset($arr[$this->zaklyuchenie]) ? $arr[$this->zaklyuchenie] : null;
    }



    public function getZaklyuchenieLabelEng()
    {
        $arr = [
            1 => 'NEGATIVE',
            2 => 'POSITIVE',
        ];


        return isset($arr[$this->zaklyuchenie]) ? $arr[$this->zaklyuchenie] : null;
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



                if ($this->isNewRecord){
            $this->user_id = Yii::$app->user->identity->id;
            $this->branche_id = Yii::$app->user->identity->branch_id;
            $this->create_at = date('Y-m-d H:i:s');
            $format = new \Da\QrCode\Format\BookMarkFormat(['title' => '2amigos', 'url' => 'http://medsoft.mir-crm.com/api/check/info?id='.$this->id]);
            $i = "uploads/".Yii::$app->security->generateRandomString().'.png';     
            $qrCode = new \Da\QrCode\QrCode($format);
            $qrCode->writeFile($i); 
            $this->qr = $i;
        }         
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($this->type_id == 1){
            if($insert){
                (new BoxOffice([
                    'patient_id' => $this->patient_id,
                    'type' => BoxOffice::IN_CASH,
                    'amounts' => $this->amounts,
                    'status' => $this->oplata ? "Оплачен" : "Не оплачен",
                    'created_id' => Yii::$app->user->getId(),
                    'specialist_id' => Yii::$app->user->getId(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'branch_id' => $this->branche_id,
                    'help_id' => $this->id,
                    'service_id' => 3,
                    'payment_type' => $this->payment_type,
                ]))->save(false);
            } else {
                $boxOffice = BoxOffice::find()->where(['help_id' => $this->id])->one();

                if($boxOffice){
                    $boxOffice->amounts = $this->amounts;
                    $boxOffice->status = $this->oplata ? "Оплачен" : "Не оплачен";
                    $boxOffice->payment_type = $this->payment_type;
                    $boxOffice->save(false);
                }
            }

        }
    }


    public function getLider()
    {
        $liderUrl = null;

        $firm = Firm::findOne($this->firm_id);

        if($firm){
            $liderUrl = '/'.$firm->pechat;
        }

        return $this->has_lider ? '<img src="'.$liderUrl.'" width="240px"></div>' : '<img src="" width="240px" style="position: absolute; left: -10000px;"></div>';
    }
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getType()
    {
        return $this->hasOne(TypesCertificates::className(), ['id' => 'type_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAgent()
    {
        return $this->hasOne(Agents::className(), ['id' => 'agent_id']);
    }

    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getVrach()
    {
        return $this->hasOne(User::className(), ['id' => 'vrach_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getZaklyuchenie()
    {
        return $this->hasOne(TypesCertificates::className(), ['id' => 'zaklyuchenie']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getFirm()
    {
        return $this->hasOne(Firm::className(), ['id' => 'firm_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranche()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branche_id']);
    }

    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);

        $dateAttributes = ['data_vydachi', 'patient.god_rojdeniya', 'patient.zaregistrirovan', 'data_vzyatiya_biomateriala'];     

        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);

            $attr = $value2;

            if(in_array($attr, $dateAttributes)){
                $text = str_replace($value, \Yii::$app->formatter->asDate(yii\helpers\ArrayHelper::getValue($this, $value2), 'php:d.m.Y'), $text);
            } elseif($attr == 'patient.name'){
                $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, 'patient.name').' '.yii\helpers\ArrayHelper::getValue($this, 'patient.last_name').' '.yii\helpers\ArrayHelper::getValue($this, 'patient.patronymic'), $text);
            } else {
                $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
            }

        }

        return $text;
    }  

}