<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "store_in".
*
    * @property string $nomer_zakaza Номер заказа
    * @property int $warehouse_id Склад
    * @property  $status Статус
    * @property  $create_at Дата
    * @property int $the_supplier_id Поставщик
    * @property string $vkladka Вкладка
    * @property  $position Позиции
*/
class StoreIn extends \yii\db\ActiveRecord
{

        const PRE_ORDER = 'Предзаказ';
    const IN_MONITOR = 'В движение';
    const DONE = 'Доставлено';
    public $position;


    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'store_in';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
            [['the_supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Suppliers::className(), 'targetAttribute' => ['the_supplier_id' => 'id']],
            [['nomer_zakaza', 'status', 'create_at', 'vkladka'], 'string'],
            [['warehouse_id'], 'required'],
            [['warehouse_id', 'the_supplier_id'], 'integer'],
            [['position'], 'safe'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'nomer_zakaza' => Yii::t('app', 'Номер заказа'),
            'warehouse_id' => Yii::t('app', 'Склад'),
            'status' => Yii::t('app', 'Статус'),
            'create_at' => Yii::t('app', 'Дата'),
            'the_supplier_id' => Yii::t('app', 'Поставщик'),
            'vkladka' => Yii::t('app', 'Вкладка'),
            'position' => Yii::t('app', 'Позиции'),
                                                                        
];
    }

    public static function statusLabels() {
        return [
            self::PRE_ORDER => "Предзаказ",
            self::IN_MONITOR => "В движение",
            self::DONE => "Доставлено",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        if ($this->isNewRecord){
          $this->create_at = date('Y-m-d H:i:s');
       }        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->position != null){
            $bankAll = StoreInItem::find()->where(['store_in' => $this->id])->all();
            foreach ($bankAll as $item1) {
                $a = false;
                foreach ($this->position as $item3) {
                    if ($item3['id'] == $item1->id) {
                        $a = true;
                    } else {
                        $a = true;
                    }
                }
                if ($a) {
                    $item1->delete();
                }
            }
            foreach ($this->position as $item) {
                $bank = StoreInItem::find()->where(['id' => $item['id']])->one();

                $selection = isset($item['selection']) ? implode(',', $item['selection']) : null;

                if (!$bank) {
                    (new StoreInItem([
                        'store_in' => $this->id,
                        'tovar_id' => isset($item['tovar_id']) ? $item['tovar_id'] : null,
					'amount' => isset($item['amount']) ? $item['amount'] : null,
					'the_cost' => isset($item['the_cost']) ? $item['the_cost'] : null,
					'mesto_na_sklade' => isset($item['mesto_na_sklade']) ? $item['mesto_na_sklade'] : null,
					'prinyato' => isset($item['prinyato']) ? $item['prinyato'] : null
                    ]))->save(false);
                } else {
                    $bank->tovar_id = isset($item['tovar_id']) ? $item['tovar_id'] : null;
				$bank->amount = isset($item['amount']) ? $item['amount'] : null;
				$bank->the_cost = isset($item['the_cost']) ? $item['the_cost'] : null;
				$bank->mesto_na_sklade = isset($item['mesto_na_sklade']) ? $item['mesto_na_sklade'] : null;
				$bank->prinyato = isset($item['prinyato']) ? $item['prinyato'] : null;

                    $bank->save(false);
                }
            }

        } else {
             
StoreInItem::deleteAll(['store_in' => $this->id]);        }

    }


    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getWarehouse()
    {
        return $this->hasOne(Store::className(), ['id' => 'warehouse_id']);
    }

    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTheSupplier()
    {
        return $this->hasOne(Suppliers::className(), ['id' => 'the_supplier_id']);
    }

    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreInItems()
    {
        return $this->hasMany(StoreInItem::className(), ['store_in' => 'id']);
    }

}