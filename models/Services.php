<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "services".
*
    * @property string $name Название
    * @property string $artikul Артикул
    * @property string $kod_804h Код 804h
    * @property int $category_id Категория
    * @property int $specialnost Специальность
    * @property  $dlitelnost Длительность
    * @property  $price Стоимость
*/
class Services extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'services';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['specialnost'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::className(), 'targetAttribute' => ['specialnost' => 'id']],
            [['name', 'artikul', 'kod_804h'], 'string'],
            [['category_id', 'specialnost'], 'integer'],
            [['dlitelnost', 'price'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
            'artikul' => Yii::t('app', 'Артикул'),
            'kod_804h' => Yii::t('app', 'Код 804h'),
            'category_id' => Yii::t('app', 'Категория'),
            'specialnost' => Yii::t('app', 'Специальность'),
            'dlitelnost' => Yii::t('app', 'Длительность'),
            'price' => Yii::t('app', 'Стоимость'),
                                                                        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCategory()
    {
        return $this->hasOne(ServiceCategory::className(), ['id' => 'category_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getSpecialnost()
    {
        return $this->hasOne(Specialization::className(), ['id' => 'specialnost']);
    }

    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}