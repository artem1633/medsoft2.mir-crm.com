<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "help_template".
*
    * @property string $name Наименование
    * @property  $verstka Верстка
*/
class HelpTemplate extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'help_template';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'verstka'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'verstka' => Yii::t('app', 'Верстка'),
                                
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTypesCertificatess()
    {
        return $this->hasMany(TypesCertificates::className(), ['shablon_spravki_id' => 'id']);
    }

}