<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "store_salvage_item".
*
    * @property int $tovar_id Товар
    * @property int $mesto_na_sklade Место на складе
    * @property  $amount Кол-во
    * @property int $store_in Поступление
*/
class StoreSalvageItem extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'store_salvage_item';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['tovar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['tovar_id' => 'id']],
            [['mesto_na_sklade'], 'exist', 'skipOnError' => true, 'targetClass' => PlaceStock::className(), 'targetAttribute' => ['mesto_na_sklade' => 'id']],
            [['store_in'], 'exist', 'skipOnError' => true, 'targetClass' => StoreSalvage::className(), 'targetAttribute' => ['store_in' => 'id']],
            [['tovar_id', 'mesto_na_sklade', 'store_in'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'tovar_id' => Yii::t('app', 'Товар'),
            'mesto_na_sklade' => Yii::t('app', 'Место на складе'),
            'amount' => Yii::t('app', 'Кол-во'),
            'store_in' => Yii::t('app', 'Поступление'),
                                                
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



                if ($this->storeIn->status == 'Доставлено') {
            $position = StoreRemains::find()->where(['tovar_id' =>  $this->tovar_id, 'warehouse_id' => $this->storeIn->warehouse_id,'mesto_na_sklade' => $this->mesto_na_sklade])->one();
            if ($position) {
                $position->amount = $position->amount - $this->amount;
                $position->save(false);
            } else {
                $position = new StoreRemains([
                    'tovar_id' => $this->tovar_id,
                    'amount' => $this->amount,
                    'warehouse_id' => $this->storeIn->warehouse_id,
                    'price' => 0,
                    'mesto_na_sklade' => $this->mesto_na_sklade,
                ]);
                $position->save(false);
            }  
        }          
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTovar()
    {
        return $this->hasOne(Products::className(), ['id' => 'tovar_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getMestoNaSklade()
    {
        return $this->hasOne(PlaceStock::className(), ['id' => 'mesto_na_sklade']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreIn()
    {
        return $this->hasOne(StoreSalvage::className(), ['id' => 'store_in']);
    }

    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}