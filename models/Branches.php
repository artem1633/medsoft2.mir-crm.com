<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "branches".
*
    * @property  $logo Логотип
    * @property string $name Название
    * @property string $tel_1 Тел 1
    * @property string $tel_2 Тел 2
    * @property string $address Адрес
    * @property int $firm_id Фирма
    * @property string $grafik_raboty График работы
*/
class Branches extends \yii\db\ActiveRecord
{

        public $fileLogo;


    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'branches';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['firm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Firm::className(), 'targetAttribute' => ['firm_id' => 'id']],
            [['logo'], 'safe'],
            [['name', 'tel_1', 'tel_2', 'address', 'grafik_raboty'], 'string'],
            [['firm_id'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'logo' => Yii::t('app', 'Логотип'),
            'name' => Yii::t('app', 'Название'),
            'tel_1' => Yii::t('app', 'Тел 1'),
            'tel_2' => Yii::t('app', 'Тел 2'),
            'address' => Yii::t('app', 'Адрес'),
            'firm_id' => Yii::t('app', 'Фирма'),
            'grafik_raboty' => Yii::t('app', 'График работы'),
                            'fileLogo' => Yii::t('app', 'Логотип'),
                                                        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {
        $fileLogo = UploadedFile::getInstance($this, 'fileLogo');
        if($fileLogo ){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$fileLogo->extension;

            $fileLogo->saveAs($path);
            $this->logo = $path;
        }



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getFirm()
    {
        return $this->hasOne(Firm::className(), ['id' => 'firm_id']);
    }

    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPatients()
    {
        return $this->hasMany(Patient::className(), ['branche_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHelps()
    {
        return $this->hasMany(Help::className(), ['branche_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['branch_id' => 'id']);
    }

}