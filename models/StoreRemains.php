<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "store_remains".
*
    * @property string $vkladka Вкладка
    * @property int $warehouse_id Склад
    * @property int $tovar_id Товар
    * @property  $amount Кол-во
    * @property string $price Стоимость
    * @property int $mesto_na_sklade Место на складе
*/
class StoreRemains extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'store_remains';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
            [['tovar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['tovar_id' => 'id']],
            [['mesto_na_sklade'], 'exist', 'skipOnError' => true, 'targetClass' => PlaceStock::className(), 'targetAttribute' => ['mesto_na_sklade' => 'id']],
            [['vkladka', 'price'], 'string'],
            [['warehouse_id', 'tovar_id', 'mesto_na_sklade'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'vkladka' => Yii::t('app', 'Вкладка'),
            'warehouse_id' => Yii::t('app', 'Склад'),
            'tovar_id' => Yii::t('app', 'Товар'),
            'amount' => Yii::t('app', 'Кол-во'),
            'price' => Yii::t('app', 'Стоимость'),
            'mesto_na_sklade' => Yii::t('app', 'Место на складе'),
                                                                
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getWarehouse()
    {
        return $this->hasOne(Store::className(), ['id' => 'warehouse_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTovar()
    {
        return $this->hasOne(Products::className(), ['id' => 'tovar_id']);
    }

    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getMestoNaSklade()
    {
        return $this->hasOne(PlaceStock::className(), ['id' => 'mesto_na_sklade']);
    }

    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}