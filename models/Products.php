<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "products".
*
    * @property string $code Code
    * @property string $name Название
    * @property int $edinica_zapravki ед. из.
    * @property string $artikul Артикул
    * @property  $kriticheskoe_kol Критическое кол-во
*/
class Products extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'products';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['edinica_zapravki'], 'exist', 'skipOnError' => true, 'targetClass' => Ed::className(), 'targetAttribute' => ['edinica_zapravki' => 'id']],
            [['code', 'name', 'artikul'], 'string'],
            [['name', 'edinica_zapravki', 'artikul'], 'required'],
            [['edinica_zapravki'], 'integer'],
            [['kriticheskoe_kol'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Название'),
            'edinica_zapravki' => Yii::t('app', 'ед. из.'),
            'artikul' => Yii::t('app', 'Артикул'),
            'kriticheskoe_kol' => Yii::t('app', 'Критическое кол-во'),
                                                        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getEdinicaZapravki()
    {
        return $this->hasOne(Ed::className(), ['id' => 'edinica_zapravki']);
    }

    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreInItems()
    {
        return $this->hasMany(StoreInItem::className(), ['tovar_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreRemainss()
    {
        return $this->hasMany(StoreRemains::className(), ['tovar_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreSalvageItems()
    {
        return $this->hasMany(StoreSalvageItem::className(), ['tovar_id' => 'id']);
    }

}