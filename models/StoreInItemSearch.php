<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StoreInItem;

/**
 * StoreInItemSearch represents the model behind the search form about `StoreInItem`.
 */
class StoreInItemSearch extends StoreInItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tovar_id', 'amount', 'prinyato', 'the_cost', 'store_in', 'mesto_na_sklade'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreInItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tovar_id' => $this->tovar_id,
            'store_in' => $this->store_in,
            'mesto_na_sklade' => $this->mesto_na_sklade,
        ]);

        $query->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'prinyato', $this->prinyato])
            ->andFilterWhere(['like', 'the_cost', $this->the_cost]);

      
        return $dataProvider;
    }
}
