<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Help;

/**
 * HelpSearch represents the model behind the search form about `Help`.
 */
class HelpSearch extends Help
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qr', 'vremya_vzyatiya_biomateriala', 'vremya_vydachi', 'srok', 'kolichestvo'], 'string'],
            [['patient_id', 'type_id', 'create_at', 'user_id', 'agent_id', 'amounts', 'data_vzyatiya_biomateriala', 'data_vydachi', 'vrach_id', 'zaklyuchenie', 'firm_id', 'branche_id', 'oplata'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Help::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'patient_id' => $this->patient_id,
            'type_id' => $this->type_id,
            'user_id' => $this->user_id,
            'agent_id' => $this->agent_id,
            'vrach_id' => $this->vrach_id,
            'zaklyuchenie' => $this->zaklyuchenie,
            'firm_id' => $this->firm_id,
            'branche_id' => $this->branche_id,
            'oplata' => $this->oplata,
        ]);

        $query->andFilterWhere(['like', 'qr', $this->qr])
            ->andFilterWhere(['like', 'amounts', $this->amounts])
            ->andFilterWhere(['like', 'vremya_vzyatiya_biomateriala', $this->vremya_vzyatiya_biomateriala])
            ->andFilterWhere(['like', 'vremya_vydachi', $this->vremya_vydachi])
            ->andFilterWhere(['like', 'srok', $this->srok])
            ->andFilterWhere(['like', 'kolichestvo', $this->kolichestvo]);

        if($this->create_at){
             $date = explode(' - ', $this->create_at);
             $query->andWhere(['between', 'create_at', $date[0].' 00:00:00', $date[1].' 23:59:59']);
         }
              
        return $dataProvider;
    }
}
