<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoxOffice;

/**
 * BoxOfficeSearch represents the model behind the search form about `BoxOffice`.
 */
class BoxOfficeSearch extends BoxOffice
{
    public $paymentType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id', 'type', 'service_id', 'amounts', 'status', 'created_id', 'created_at', 'branch_id', 'specialist_id', 'payment_type'], 'safe'],
            [['comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BoxOffice::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['help']);


        $query->andFilterWhere([
            'specialist_id' => $this->specialist_id,
            'patient_id' => $this->patient_id,
            'type' => $this->type,
            'service_id' => $this->service_id,
            // 'status' => $this->status,
            'created_id' => $this->created_id,
            'branch_id' => $this->branch_id,
            'box_office.payment_type' => $this->payment_type,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'amounts', $this->amounts])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if($this->created_at){
            $date = explode(' - ', $this->created_at);
            $query->andWhere(['between', 'created_at', $date[0].' 00:00:00', $date[1].' 23:59:59']);
         }
 
        return $dataProvider;
    }
}
