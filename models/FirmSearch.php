<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Firm;

/**
 * FirmSearch represents the model behind the search form about `Firm`.
 */
class FirmSearch extends Firm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'create_at', 'pechat', 'pechat_kvitanciyu'], 'safe'],
            [['name', 'licenziya', 'organizaciya', 'srok', 'address', 'tel', 'sayt', 'license', 'date', 'name', 'adress', 'phone', 'site'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Firm::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'licenziya', $this->licenziya])
            ->andFilterWhere(['like', 'organizaciya', $this->organizaciya])
            ->andFilterWhere(['like', 'srok', $this->srok])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'tel', $this->tel])
            ->andFilterWhere(['like', 'sayt', $this->sayt])
            ->andFilterWhere(['like', 'license', $this->license])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'adress', $this->adress])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'site', $this->site]);

                 
        return $dataProvider;
    }
}
