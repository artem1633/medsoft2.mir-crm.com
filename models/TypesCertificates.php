<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "types_certificates".
*
    * @property string $name Название
    * @property  $price Стоимость 
    * @property  $agentu Агенту
    * @property  $zaklyuchenie Заключение
    * @property int $shablon_qr_id Шаблон qr
    * @property int $shablon_spravki_id Шаблон справки
    * @property  $vozmojnost_udalyat Запрет удалять
*/
class TypesCertificates extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'types_certificates';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['shablon_qr_id'], 'exist', 'skipOnError' => true, 'targetClass' => TemplateQr::className(), 'targetAttribute' => ['shablon_qr_id' => 'id']],
            [['shablon_spravki_id'], 'exist', 'skipOnError' => true, 'targetClass' => HelpTemplate::className(), 'targetAttribute' => ['shablon_spravki_id' => 'id']],
            [['name'], 'string'],
            [['price', 'agentu'], 'number'],
            [['zaklyuchenie'], 'safe'],
            [['shablon_qr_id', 'shablon_spravki_id', 'vozmojnost_udalyat'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
            'price' => Yii::t('app', 'Стоимость '),
            'agentu' => Yii::t('app', 'Агенту'),
            'zaklyuchenie' => Yii::t('app', 'Заключение'),
            'shablon_qr_id' => Yii::t('app', 'Шаблон qr'),
            'shablon_spravki_id' => Yii::t('app', 'Шаблон справки'),
            'vozmojnost_udalyat' => Yii::t('app', 'Запрет удалять'),
                                                                        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {

        if(is_array($this->zaklyuchenie)){
            $this->zaklyuchenie = implode(',', $this->zaklyuchenie);
    }


        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getShablonQr()
    {
        return $this->hasOne(TemplateQr::className(), ['id' => 'shablon_qr_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getShablonSpravki()
    {
        return $this->hasOne(HelpTemplate::className(), ['id' => 'shablon_spravki_id']);
    }

    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHelps()
    {
        return $this->hasMany(Help::className(), ['type_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHelps0()
    {
        return $this->hasMany(Help::className(), ['zaklyuchenie' => 'id']);
    }

}