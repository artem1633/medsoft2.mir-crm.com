<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "firm".
*
    * @property  $logo Логотип
    * @property string $name Name
    * @property string $licenziya Лицензия
    * @property string $organizaciya Организация
    * @property  $create_at Дата
    * @property string $srok Срок
    * @property string $address Адрес
    * @property string $tel Тел
    * @property string $sayt Сайт
    * @property string $license License
    * @property string $date Date
    * @property string $adress Adress
    * @property string $phone Phone
    * @property string $site Site
    * @property  $pechat Печать
    * @property  $pechat_kvitanciyu Печать квитанцию
*/
class Firm extends \yii\db\ActiveRecord
{

        public $fileLogo;
public $filePechat;
public $filePechatKvitanciyu;


    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'firm';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['logo', 'pechat', 'pechat_kvitanciyu'], 'safe'],
            [['name', 'name_en', 'licenziya', 'organizaciya', 'create_at', 'srok', 'address', 'tel', 'sayt', 'license', 'date', 'name', 'adress', 'phone', 'site'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'logo' => Yii::t('app', 'Логотип'),
            'name' => Yii::t('app', 'Наименование'),
            'name_en' => Yii::t('app', 'name'),
            'licenziya' => Yii::t('app', 'Лицензия'),
            'organizaciya' => Yii::t('app', 'Организация'),
            'create_at' => Yii::t('app', 'Дата'),
            'srok' => Yii::t('app', 'Срок'),
            'address' => Yii::t('app', 'Адрес'),
            'tel' => Yii::t('app', 'Тел'),
            'sayt' => Yii::t('app', 'Сайт'),
            'license' => Yii::t('app', 'License'),
            'date' => Yii::t('app', 'Date'),
            'adress' => Yii::t('app', 'Adress'),
            'phone' => Yii::t('app', 'Phone'),
            'site' => Yii::t('app', 'Site'),
            'pechat' => Yii::t('app', 'Печать'),
            'pechat_kvitanciyu' => Yii::t('app', 'Печать квитанцию'),
                            'fileLogo' => Yii::t('app', 'Логотип'),
                                                                                                                                    'filePechat' => Yii::t('app', 'Печать'),
                    'filePechatKvitanciyu' => Yii::t('app', 'Печать квитанцию'),
        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {
        $fileLogo = UploadedFile::getInstance($this, 'fileLogo');
        if($fileLogo ){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$fileLogo->extension;

            $fileLogo->saveAs($path);
            $this->logo = $path;
        }
        $filePechat = UploadedFile::getInstance($this, 'filePechat');
        if($filePechat ){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$filePechat->extension;

            $filePechat->saveAs($path);
            $this->pechat = $path;
        }
        $filePechatKvitanciyu = UploadedFile::getInstance($this, 'filePechatKvitanciyu');
        if($filePechatKvitanciyu ){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$filePechatKvitanciyu->extension;

            $filePechatKvitanciyu->saveAs($path);
            $this->pechat_kvitanciyu = $path;
        }



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHelps()
    {
        return $this->hasMany(Help::className(), ['firm_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranchess()
    {
        return $this->hasMany(Branches::className(), ['firm_id' => 'id']);
    }

}