<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\File;

/**
 * FileSearch represents the model behind the search form about `File`.
 */
class FileSearch extends File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folder', 'subfolder', 'size', 'extension', 'opisanie_sten', 'path', 'name'], 'string'],
            [['datetime', 'type', 'user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'type' => $this->type,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'folder', $this->folder])
            ->andFilterWhere(['like', 'subfolder', $this->subfolder])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'extension', $this->extension])
            ->andFilterWhere(['like', 'opisanie_sten', $this->opisanie_sten])
            ->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'name', $this->name]);

              if($this->datetime){
             $date = explode(' - ', $this->datetime);
             $query->andWhere(['between', 'datetime', $date[0], $date[1]]);
         }
     
        return $dataProvider;
    }
}
