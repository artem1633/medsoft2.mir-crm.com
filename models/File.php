<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "file".
*
    * @property string $folder Папка
    * @property string $subfolder Подпапка
    * @property string $size Размер
    * @property string $extension Расширение
    * @property  $datetime Загружен
    * @property string $opisanie_sten Описание
    * @property string $path Путь
    * @property string $name Наименование
    * @property  $type Тип
    * @property int $user_id Сотрудник
*/
class File extends \app\base\ActiveRecord
{

        const TYPE_FOLDER = 'Папка';
    const TYPE_FILE = 'Файл';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'file';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['folder', 'subfolder', 'size', 'extension', 'datetime', 'opisanie_sten', 'path', 'name', 'type'], 'string'],
            [['user_id'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'folder' => Yii::t('app', 'Папка'),
            'subfolder' => Yii::t('app', 'Подпапка'),
            'size' => Yii::t('app', 'Размер'),
            'extension' => Yii::t('app', 'Расширение'),
            'datetime' => Yii::t('app', 'Загружен'),
            'opisanie_sten' => Yii::t('app', 'Описание'),
            'path' => Yii::t('app', 'Путь'),
            'name' => Yii::t('app', 'Наименование'),
            'type' => Yii::t('app', 'Тип'),
            'user_id' => Yii::t('app', 'Сотрудник'),
                                                                                                
];
    }

    public static function typeLabels() {
        return [
            self::TYPE_FOLDER => "Папка",
            self::TYPE_FILE => "Файл",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }




    
    
    
    
    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

            const IMG_EXTENSIONS = [
        'jpg',
        'png',
        'tiff',
        'gif',
    ];

    const TXT_EXTENSIONS = [
        'txt',
        'json',
    ];

    const VIDEO_EXTENSIONS = [
        'mp4',
        'flv',
        'avi',
    ];

/**
     * @return bool
     */
    public function isImg()
    {
        return in_array($this->extension, self::IMG_EXTENSIONS);
    }


    /**
     * @return bool
     */
    public function isVideo()
    {
        return in_array($this->extension, self::VIDEO_EXTENSIONS);
    }

    /**
     * @return bool
     */
    public function isText()
    {
        return in_array($this->extension, self::TXT_EXTENSIONS);
    }

    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}