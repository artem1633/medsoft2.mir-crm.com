<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StoreSalvageItem;

/**
 * StoreSalvageItemSearch represents the model behind the search form about `StoreSalvageItem`.
 */
class StoreSalvageItemSearch extends StoreSalvageItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tovar_id', 'mesto_na_sklade', 'amount', 'store_in'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreSalvageItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tovar_id' => $this->tovar_id,
            'mesto_na_sklade' => $this->mesto_na_sklade,
            'store_in' => $this->store_in,
        ]);

        $query->andFilterWhere(['like', 'amount', $this->amount]);

    
        return $dataProvider;
    }
}
