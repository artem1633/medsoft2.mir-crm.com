<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "patient".
*
    * @property string $name ФИО
    * @property  $pol Пол
    * @property  $god_rojdeniya Год рождения
    * @property string $phone Телефон
    * @property  $posledniy_vizit Последний визит
    * @property  $zaregistrirovan Зарегистрирован
    * @property int $branche_id Филиал
*/
class Patient extends \yii\db\ActiveRecord
{

        const V10 = 'М';
    const V11 = 'Ж';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'patient';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            // [['name', 'name_en', 'last_name', 'patronymic', 'pol', 'god_rojdeniya'], 'required'],
            [['name_en', 'pol', 'god_rojdeniya'], 'required'],
            [['branche_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::className(), 'targetAttribute' => ['branche_id' => 'id']],
            [['name', 'name_en', 'pol', 'god_rojdeniya', 'phone', 'posledniy_vizit', 'zaregistrirovan', 'last_name', 'patronymic', 'latin_name', 'passport', 'address'], 'string'],
            [['branche_id', 'discovered'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Имя'),
            'name_en' => Yii::t('app', 'ФИО латиницей'),
            'latin_name' => Yii::t('app', 'Паспорт'),
            'discovered' => Yii::t('app', 'Обнаружен'),
            'pol' => Yii::t('app', 'Пол'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'patronymic' => Yii::t('app', 'Отчество'),
            'god_rojdeniya' => Yii::t('app', 'Год рождения'),
            'phone' => Yii::t('app', 'Телефон'),
            'posledniy_vizit' => Yii::t('app', 'Последний визит'),
            'zaregistrirovan' => Yii::t('app', 'Зарегистрирован'),
            'branche_id' => Yii::t('app', 'Филиал'),
            'passport' => Yii::t('app', 'Паспорт'),
            'address' => Yii::t('app', 'Адрес'),
                                                                        
];
    }

    public static function polLabels() {
        return [
            self::V10 => "М",
            self::V11 => "Ж",
        ];
    }

    public function getFio()
    {
        $fio = [];

        if($this->last_name){
            $fio[] = $this->last_name;
        }

        if($this->name) {
            $fio[] = $this->name;
        }

        if($this->patronymic){
            $fio[] = $this->patronymic;
        }

        return implode(' ', $fio);
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {

        if($this->isNewRecord){
            $this->zaregistrirovan = date('Y-m-d H:i:s');
        }
        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranche()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branche_id']);
    }

    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);

        $dateAttributes = ['zaregistrirovan'];
        $dateBirthAttributes = ['god_rojdeniya'];

        Yii::$app->formatter->locale = 'ru-RU';

        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            if(in_array($value2, $dateAttributes)){
                $arr = [
                  'января',
                  'февраля',
                  'марта',
                  'апреля',
                  'мая',
                  'июня',
                  'июля',
                  'августа',
                  'сентября',
                  'октября',
                  'ноября',
                  'декабря'
                ];

                $month = date('n')-1;
                // $arr[$month].' '.date('d, Y');

                $text = str_replace($value, Yii::$app->formatter->asDate(yii\helpers\ArrayHelper::getValue($this, $value2), 'php:«d» '.$arr[$month].' Y'), $text);
            } elseif(in_array($value2, $dateBirthAttributes)){
                $text = str_replace($value, Yii::$app->formatter->asDate(yii\helpers\ArrayHelper::getValue($this, $value2), 'php:d.m.Y'), $text);
            } else {
                $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
            }
        }

        return $text;
    }  
    
    public function getPolEng()
    {
        if($this->pol == self::V10){
            return "M";
        } elseif($this->pol == self::V11){
            return "F";
        }
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHelps()
    {
        return $this->hasMany(Help::className(), ['patient_id' => 'id']);
    }

}