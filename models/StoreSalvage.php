<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "store_salvage".
*
    * @property int $warehouse_id Склад
    * @property  $status Статус
    * @property  $create_at Дата
    * @property string $vkladka Вкладка
    * @property  $position Позиции
*/
class StoreSalvage extends \yii\db\ActiveRecord
{

        const PRE_ORDER = 'Предзаказ';
    const IN_MONITOR = 'В движение';
    const DONE = 'Доставлено';
    public $position;


    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'store_salvage';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
            [['warehouse_id'], 'required'],
            [['warehouse_id'], 'integer'],
            [['status', 'create_at', 'vkladka'], 'string'],
            [['position'], 'safe'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'warehouse_id' => Yii::t('app', 'Склад'),
            'status' => Yii::t('app', 'Статус'),
            'create_at' => Yii::t('app', 'Дата'),
            'vkladka' => Yii::t('app', 'Вкладка'),
            'position' => Yii::t('app', 'Позиции'),
                                                        
];
    }

    public static function statusLabels() {
        return [
            self::PRE_ORDER => "Предзаказ",
            self::IN_MONITOR => "В движение",
            self::DONE => "Доставлено",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        if ($this->isNewRecord){
          $this->create_at = date('Y-m-d H:i:s');
       }        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->position != null){
            $bankAll = StoreSalvageItem::find()->where(['store_in' => $this->id])->all();
            foreach ($bankAll as $item1) {
                $a = false;
                foreach ($this->position as $item3) {
                    if ($item3['id'] == $item1->id) {
                        $a = true;
                    } else {
                        $a = true;
                    }
                }
                if ($a) {
                    $item1->delete();
                }
            }
            foreach ($this->position as $item) {
                $bank = StoreSalvageItem::find()->where(['id' => $item['id']])->one();

                $selection = isset($item['selection']) ? implode(',', $item['selection']) : null;

                if (!$bank) {
                    (new StoreSalvageItem([
                        'store_in' => $this->id,
                        'tovar_id' => isset($item['tovar_id']) ? $item['tovar_id'] : null,
					'amount' => isset($item['amount']) ? $item['amount'] : null,
					'mesto_na_sklade' => isset($item['mesto_na_sklade']) ? $item['mesto_na_sklade'] : null
                    ]))->save(false);
                } else {
                    $bank->tovar_id = isset($item['tovar_id']) ? $item['tovar_id'] : null;
				$bank->amount = isset($item['amount']) ? $item['amount'] : null;
				$bank->mesto_na_sklade = isset($item['mesto_na_sklade']) ? $item['mesto_na_sklade'] : null;

                    $bank->save(false);
                }
            }

        } else {
             
StoreSalvageItem::deleteAll(['store_in' => $this->id]);        }

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getWarehouse()
    {
        return $this->hasOne(Store::className(), ['id' => 'warehouse_id']);
    }

    
    
    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreSalvageItems()
    {
        return $this->hasMany(StoreSalvageItem::className(), ['store_in' => 'id']);
    }

}