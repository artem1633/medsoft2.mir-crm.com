<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "box_office".
*
* @property int $patient_id Пациент
* @property  $type Тип
* @property int $service_id Услуга
* @property  $amounts Сумма
* @property  $status Статус
* @property string $comment Комментарий
* @property int $created_id Создал
* @property  $created_at Создан
* @property int $branch_id Филиал
* @property int $help_id Справка
*/
class BoxOffice extends \app\base\ActiveRecord
{

        const  IN_CASH = 'Поступление';
    const OUT_CASH = 'Списание';
    const DONE = 'Оплачен';
    const JOB = 'Не оплачен';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'box_office';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['created_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_id' => 'id']],
            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::className(), 'targetAttribute' => ['branch_id' => 'id']],
            [['patient_id', 'service_id', 'created_id', 'branch_id', 'specialist_id', 'help_id'], 'integer'],
            [['type', 'status', 'comment', 'created_at', 'payment_type'], 'string'],
            [['amounts'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'patient_id' => Yii::t('app', 'Пациент'),
            'type' => Yii::t('app', 'Тип'),
            'service_id' => Yii::t('app', 'Услуга'),
            'amounts' => Yii::t('app', 'Сумма'),
            'status' => Yii::t('app', 'Статус'),
            'comment' => Yii::t('app', 'Комментарий'),
            'created_id' => Yii::t('app', 'Создал'),
            'created_at' => Yii::t('app', 'Создан'),
            'branch_id' => Yii::t('app', 'Филиал'),
            'help_id' => Yii::t('app', 'Справка'),
            'specialist_id' => Yii::t('app', 'Специалист'),
            'payment_type' => 'Способ оплаты',
                                                                                        
];
    }

    public static function typeLabels() {
        return [
            self::IN_CASH => "Поступление",
            self::OUT_CASH => "Списание",
        ];
    }public static function statusLabels() {
        return [
            self::DONE => "Оплачен",
            self::JOB => "Не оплачен",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



                if ($this->isNewRecord){
            $this->created_id = Yii::$app->user->identity->id;
            $this->branch_id = Yii::$app->user->identity->branch_id;
            $this->created_at = date('Y-m-d H:i:s');
        }         
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }




    public function getHelp()
    {
        return $this->hasOne(Help::className(), ['id' => 'help_id']);
    }
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranch()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branch_id']);
    }



    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}