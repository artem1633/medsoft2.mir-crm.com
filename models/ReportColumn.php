<?php 
namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name Название
 * @property integer $patient_name Пациент ФИО
 * @property integer $patient_pol Пациент Пол
 * @property integer $patient_god_rojdeniya Пациент Год рождения
 * @property integer $patient_phone Пациент Телефон
 * @property integer $patient_posledniy_vizit Пациент Последний визит
 * @property integer $patient_zaregistrirovan Пациент Зарегистрирован
 * @property integer $patient_branche_id Пациент Филиал
 * @property integer $help_test Справки Тестирование
 * @property integer $help_qr Справки QR
 * @property integer $help_patient_id Справки Пациент
 * @property integer $help_type_id Справки Тип
 * @property integer $help_create_at Справки Дата и время
 * @property integer $help_user_id Справки Сотрудник
 * @property integer $help_agent_id Справки Агент
 * @property integer $help_amounts Справки Сумма
 * @property integer $help_data_vzyatiya_biomateriala Справки Дата взятия биоматериала
 * @property integer $help_vremya_vzyatiya_biomateriala Справки Время взятия биоматериала
 * @property integer $help_data_vydachi Справки Дата выдачи
 * @property integer $help_vremya_vydachi Справки Время выдачи
 * @property integer $help_vrach_id Справки Врач
 * @property integer $help_srok Справки Срок
 * @property integer $help_zaklyuchenie Справки Заключение
 * @property integer $help_kolichestvo Справки Количество
 * @property integer $help_firm_id Справки Фирма
 * @property integer $help_branche_id Справки Филиал
 * @property integer $help_oplata Справки Оплачен
 * @property integer $honey_books_name Мед книжки ФИО
 * @property integer $honey_books_data_rojdeniya Мед книжки Дата рождения
 * @property integer $honey_books_city Мед книжки Город
 * @property integer $honey_books_ulica Мед книжки Улица
 * @property integer $honey_books_home_id Мед книжки Дом
 * @property integer $honey_books_obekt Мед книжки Объект
 * @property integer $honey_books_role Мед книжки Должность
 * @property integer $store_in_nomer_zakaza Поступления Номер заказа
 * @property integer $store_in_warehouse_id Поступления Склад
 * @property integer $store_in_status Поступления Статус
 * @property integer $store_in_create_at Поступления Дата
 * @property integer $store_in_the_supplier_id Поступления Поставщик
 * @property integer $store_in_vkladka Поступления Вкладка
 * @property integer $store_in_position Поступления Позиции
 * @property integer $store_in_item_tovar_id Позиции поступления Товар
 * @property integer $store_in_item_amount Позиции поступления Кол-во
 * @property integer $store_in_item_prinyato Позиции поступления Принято
 * @property integer $store_in_item_the_cost Позиции поступления Стоимость
 * @property integer $store_in_item_store_in Позиции поступления Поступление
 * @property integer $store_in_item_mesto_na_sklade Позиции поступления Место на складе
 * @property integer $store_remains_vkladka Остатки Вкладка
 * @property integer $store_remains_warehouse_id Остатки Склад
 * @property integer $store_remains_tovar_id Остатки Товар
 * @property integer $store_remains_amount Остатки Кол-во
 * @property integer $store_remains_price Остатки Стоимость
 * @property integer $store_remains_mesto_na_sklade Остатки Место на складе
 * @property integer $store_data_vkladka Сводные данные Вкладка
 * @property integer $store_salvage_warehouse_id Списание Склад
 * @property integer $store_salvage_status Списание Статус
 * @property integer $store_salvage_create_at Списание Дата
 * @property integer $store_salvage_vkladka Списание Вкладка
 * @property integer $store_salvage_position Списание Позиции
 * @property integer $store_salvage_item_tovar_id Позиции списания Товар
 * @property integer $store_salvage_item_mesto_na_sklade Позиции списания Место на складе
 * @property integer $store_salvage_item_amount Позиции списания Кол-во
 * @property integer $store_salvage_item_store_in Позиции списания Поступление
 * @property integer $books Справочники
 *
 * @property User[] $users
 */
class ReportColumn extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_column';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_name', 'patient_pol', 'patient_god_rojdeniya', 'patient_phone', 'patient_posledniy_vizit', 'patient_zaregistrirovan', 'patient_branche_id', 'help_test', 'help_qr', 'help_patient_id', 'help_type_id', 'help_create_at', 'help_user_id', 'help_agent_id', 'help_amounts', 'help_data_vzyatiya_biomateriala', 'help_vremya_vzyatiya_biomateriala', 'help_data_vydachi', 'help_vremya_vydachi', 'help_vrach_id', 'help_srok', 'help_zaklyuchenie', 'help_kolichestvo', 'help_firm_id', 'help_branche_id', 'help_oplata', 'honey_books_name', 'honey_books_data_rojdeniya', 'honey_books_city', 'honey_books_ulica', 'honey_books_home_id', 'honey_books_obekt', 'honey_books_role', 'store_in_nomer_zakaza', 'store_in_warehouse_id', 'store_in_status', 'store_in_create_at', 'store_in_the_supplier_id', 'store_in_vkladka', 'store_in_position', 'store_in_item_tovar_id', 'store_in_item_amount', 'store_in_item_prinyato', 'store_in_item_the_cost', 'store_in_item_store_in', 'store_in_item_mesto_na_sklade', 'store_remains_vkladka', 'store_remains_warehouse_id', 'store_remains_tovar_id', 'store_remains_amount', 'store_remains_price', 'store_remains_mesto_na_sklade', 'store_data_vkladka', 'store_salvage_warehouse_id', 'store_salvage_status', 'store_salvage_create_at', 'store_salvage_vkladka', 'store_salvage_position', 'store_salvage_item_tovar_id', 'store_salvage_item_mesto_na_sklade', 'store_salvage_item_amount', 'store_salvage_item_store_in'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'patient_name' => 'ФИО',
            'patient_pol' => 'Пол',
            'patient_god_rojdeniya' => 'Год рождения',
            'patient_phone' => 'Телефон',
            'patient_posledniy_vizit' => 'Последний визит',
            'patient_zaregistrirovan' => 'Зарегистрирован',
            'patient_branche_id' => 'Филиал',
            'help_test' => 'Тестирование',
            'help_qr' => 'QR',
            'help_patient_id' => 'Пациент',
            'help_type_id' => 'Тип',
            'help_create_at' => 'Дата и время',
            'help_user_id' => 'Сотрудник',
            'help_agent_id' => 'Агент',
            'help_amounts' => 'Сумма',
            'help_data_vzyatiya_biomateriala' => 'Дата взятия биоматериала',
            'help_vremya_vzyatiya_biomateriala' => 'Время взятия биоматериала',
            'help_data_vydachi' => 'Дата выдачи',
            'help_vremya_vydachi' => 'Время выдачи',
            'help_vrach_id' => 'Врач',
            'help_srok' => 'Срок',
            'help_zaklyuchenie' => 'Заключение',
            'help_kolichestvo' => 'Количество',
            'help_firm_id' => 'Фирма',
            'help_branche_id' => 'Филиал',
            'help_oplata' => 'Оплачен',
            'honey_books_name' => 'ФИО',
            'honey_books_data_rojdeniya' => 'Дата рождения',
            'honey_books_city' => 'Город',
            'honey_books_ulica' => 'Улица',
            'honey_books_home_id' => 'Дом',
            'honey_books_obekt' => 'Объект',
            'honey_books_role' => 'Должность',
            'store_in_nomer_zakaza' => 'Номер заказа',
            'store_in_warehouse_id' => 'Склад',
            'store_in_status' => 'Статус',
            'store_in_create_at' => 'Дата',
            'store_in_the_supplier_id' => 'Поставщик',
            'store_in_vkladka' => 'Вкладка',
            'store_in_position' => 'Позиции',
            'store_in_item_tovar_id' => 'Товар',
            'store_in_item_amount' => 'Кол-во',
            'store_in_item_prinyato' => 'Принято',
            'store_in_item_the_cost' => 'Стоимость',
            'store_in_item_store_in' => 'Поступление',
            'store_in_item_mesto_na_sklade' => 'Место на складе',
            'store_remains_vkladka' => 'Вкладка',
            'store_remains_warehouse_id' => 'Склад',
            'store_remains_tovar_id' => 'Товар',
            'store_remains_amount' => 'Кол-во',
            'store_remains_price' => 'Стоимость',
            'store_remains_mesto_na_sklade' => 'Место на складе',
            'store_data_vkladka' => 'Вкладка',
            'store_salvage_warehouse_id' => 'Склад',
            'store_salvage_status' => 'Статус',
            'store_salvage_create_at' => 'Дата',
            'store_salvage_vkladka' => 'Вкладка',
            'store_salvage_position' => 'Позиции',
            'store_salvage_item_tovar_id' => 'Товар',
            'store_salvage_item_mesto_na_sklade' => 'Место на складе',
            'store_salvage_item_amount' => 'Кол-во',
            'store_salvage_item_store_in' => 'Поступление',
            
        ];
    }


    /**
     * @return array
     */
    public function getColumnsAttributes(){
        return [
     'patient_name',
     'patient_pol',
     'patient_god_rojdeniya',
     'patient_phone',
     'patient_posledniy_vizit',
     'patient_zaregistrirovan',
     'patient_branche_id',
     'help_test',
     'help_qr',
     'help_patient_id',
     'help_type_id',
     'help_create_at',
     'help_user_id',
     'help_agent_id',
     'help_amounts',
     'help_data_vzyatiya_biomateriala',
     'help_vremya_vzyatiya_biomateriala',
     'help_data_vydachi',
     'help_vremya_vydachi',
     'help_vrach_id',
     'help_srok',
     'help_zaklyuchenie',
     'help_kolichestvo',
     'help_firm_id',
     'help_branche_id',
     'help_oplata',
     'honey_books_name',
     'honey_books_data_rojdeniya',
     'honey_books_city',
     'honey_books_ulica',
     'honey_books_home_id',
     'honey_books_obekt',
     'honey_books_role',
     'store_in_nomer_zakaza',
     'store_in_warehouse_id',
     'store_in_status',
     'store_in_create_at',
     'store_in_the_supplier_id',
     'store_in_vkladka',
     'store_in_position',
     'store_in_item_tovar_id',
     'store_in_item_amount',
     'store_in_item_prinyato',
     'store_in_item_the_cost',
     'store_in_item_store_in',
     'store_in_item_mesto_na_sklade',
     'store_remains_vkladka',
     'store_remains_warehouse_id',
     'store_remains_tovar_id',
     'store_remains_amount',
     'store_remains_price',
     'store_remains_mesto_na_sklade',
     'store_data_vkladka',
     'store_salvage_warehouse_id',
     'store_salvage_status',
     'store_salvage_create_at',
     'store_salvage_vkladka',
     'store_salvage_position',
     'store_salvage_item_tovar_id',
     'store_salvage_item_mesto_na_sklade',
     'store_salvage_item_amount',
     'store_salvage_item_store_in',
    
        ];
    }


    /**
     * @inheritdoc
     */
    public function getColumns()
    {
        $columns = [];


        foreach ($this->getColumnsAttributes() as $attr){
            if($this->$attr != null){
                $arr = explode('_', $attr);
  
                if($arr[0] == 'doctor_office'){
                    $str = 'doctor_office_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Кабинет врача</span>';
                }
  
                if($arr[0] == 'patient'){
                    $str = 'patient_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Пациент</span>';
                }
  
                if($arr[0] == 'help'){
                    $str = 'help_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Справки</span>';
                }
  
                if($arr[0] == 'honey_books'){
                    $str = 'honey_books_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Мед книжки</span>';
                }
  
                if($arr[0] == 'store_manager'){
                    $str = 'store_manager_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Управление складом</span>';
                }
  
                if($arr[0] == 'store_in'){
                    $str = 'store_in_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Поступления</span>';
                }
  
                if($arr[0] == 'store_in_item'){
                    $str = 'store_in_item_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Позиции поступления</span>';
                }
  
                if($arr[0] == 'store_remains'){
                    $str = 'store_remains_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Остатки</span>';
                }
  
                if($arr[0] == 'store_data'){
                    $str = 'store_data_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Сводные данные</span>';
                }
  
                if($arr[0] == 'store_salvage'){
                    $str = 'store_salvage_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Списание</span>';
                }
  
                if($arr[0] == 'store_salvage_item'){
                    $str = 'store_salvage_item_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Позиции списания</span>';
                }
  
                $columns[] = [
                    'attribute' => $str,
                    'header' => "<div style='width: 100%; text-align: center;'>{$header}</div>".$this->getAttributeLabel($attr),
                ];
            }
        }

        return $columns;
    }

}
