<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "place_stock".
*
    * @property string $name Наименование
*/
class PlaceStock extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'place_stock';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
                        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreInItems()
    {
        return $this->hasMany(StoreInItem::className(), ['mesto_na_sklade' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreRemainss()
    {
        return $this->hasMany(StoreRemains::className(), ['mesto_na_sklade' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreSalvageItems()
    {
        return $this->hasMany(StoreSalvageItem::className(), ['mesto_na_sklade' => 'id']);
    }

}