<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Branches;

/**
 * BranchesSearch represents the model behind the search form about `Branches`.
 */
class BranchesSearch extends Branches
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'firm_id'], 'safe'],
            [['name', 'tel_1', 'tel_2', 'address', 'grafik_raboty'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Branches::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'firm_id' => $this->firm_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'tel_1', $this->tel_1])
            ->andFilterWhere(['like', 'tel_2', $this->tel_2])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'grafik_raboty', $this->grafik_raboty]);

       
        return $dataProvider;
    }
}
