<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "honey_books".
*
    * @property string $name ФИО
    * @property  $data_rojdeniya Дата рождения
    * @property string $city Город
    * @property string $ulica Улица
    * @property string $home_id Дом
    * @property string $obekt Объект
    * @property string $role Должность
*/
class HoneyBooks extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'honey_books';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'data_rojdeniya', 'city', 'ulica', 'home_id', 'obekt', 'role'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'ФИО'),
            'data_rojdeniya' => Yii::t('app', 'Дата рождения'),
            'city' => Yii::t('app', 'Город'),
            'ulica' => Yii::t('app', 'Улица'),
            'home_id' => Yii::t('app', 'Дом'),
            'obekt' => Yii::t('app', 'Объект'),
            'role' => Yii::t('app', 'Должность'),
                                                                        
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
    
    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}