<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "store_in_item".
*
    * @property int $tovar_id Товар
    * @property  $amount Кол-во
    * @property  $prinyato Принято
    * @property  $the_cost Стоимость
    * @property int $store_in Поступление
    * @property int $mesto_na_sklade Место на складе
*/
class StoreInItem extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'store_in_item';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['tovar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['tovar_id' => 'id']],
            [['store_in'], 'exist', 'skipOnError' => true, 'targetClass' => StoreIn::className(), 'targetAttribute' => ['store_in' => 'id']],
            [['mesto_na_sklade'], 'exist', 'skipOnError' => true, 'targetClass' => PlaceStock::className(), 'targetAttribute' => ['mesto_na_sklade' => 'id']],
            [['tovar_id', 'store_in', 'mesto_na_sklade'], 'integer'],
            [['amount', 'prinyato', 'the_cost'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'tovar_id' => Yii::t('app', 'Товар'),
            'amount' => Yii::t('app', 'Кол-во'),
            'prinyato' => Yii::t('app', 'Принято'),
            'the_cost' => Yii::t('app', 'Стоимость'),
            'store_in' => Yii::t('app', 'Поступление'),
            'mesto_na_sklade' => Yii::t('app', 'Место на складе'),
                                                                
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        if ($this->storeIn->status == 'Доставлено') {
            $position = StoreRemains::find()->where(['tovar_id' =>  $this->tovar_id, 'warehouse_id' => $this->storeIn->warehouse_id,'mesto_na_sklade' => $this->mesto_na_sklade])->one();
            if ($position) {
                $position->amount = $position->amount + $this->prinyato;
                $position->save(false);
            } else {
                $position = new StoreRemains([
                    'tovar_id' => $this->tovar_id,
                    'amount' => $this->prinyato,
                    'warehouse_id' => $this->storeIn->warehouse_id,
                    'price' => $this->the_cost,
                    'mesto_na_sklade' => $this->mesto_na_sklade,
                ]);
                $position->save(false);
            }  
        }           
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTovar()
    {
        return $this->hasOne(Products::className(), ['id' => 'tovar_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStoreIn()
    {
        return $this->hasOne(StoreIn::className(), ['id' => 'store_in']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getMestoNaSklade()
    {
        return $this->hasOne(PlaceStock::className(), ['id' => 'mesto_na_sklade']);
    }

    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}