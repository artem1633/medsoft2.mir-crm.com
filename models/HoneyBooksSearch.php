<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HoneyBooks;

/**
 * HoneyBooksSearch represents the model behind the search form about `HoneyBooks`.
 */
class HoneyBooksSearch extends HoneyBooks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city', 'ulica', 'home_id', 'obekt', 'role'], 'string'],
            [['data_rojdeniya'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HoneyBooks::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'ulica', $this->ulica])
            ->andFilterWhere(['like', 'home_id', $this->home_id])
            ->andFilterWhere(['like', 'obekt', $this->obekt])
            ->andFilterWhere(['like', 'role', $this->role]);

       
        return $dataProvider;
    }
}
