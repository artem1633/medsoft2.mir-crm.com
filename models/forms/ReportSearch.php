<?php 
namespace app\models\forms;


use app\models\DoctorOffice; 
use app\models\Patient; 
use app\models\Help; 
use app\models\HoneyBooks; 
use app\models\StoreManager; 
use app\models\StoreIn; 
use app\models\StoreInItem; 
use app\models\StoreRemains; 
use app\models\StoreData; 
use app\models\StoreSalvage; 
use app\models\StoreSalvageItem; 
  

use app\models\ReportColumn;
use app\components\ComponentReport;
use Codeception\PHPUnit\ResultPrinter\Report;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class ReportSearch
 * @package app\models\forms
 */
class ReportSearch extends Model
{
    /**
     * @var array
     */
    public $setting;
    public $oldSetting;
    public $patient_name;
    public $patient_pol;
    public $patient_god_rojdeniya;
    public $patient_phone;
    public $patient_posledniy_vizit;
    public $patient_zaregistrirovan;
    public $patient_branche_id;
    public $help_test;
    public $help_qr;
    public $help_patient_id;
    public $help_type_id;
    public $help_create_at;
    public $help_user_id;
    public $help_agent_id;
    public $help_amounts;
    public $help_data_vzyatiya_biomateriala;
    public $help_vremya_vzyatiya_biomateriala;
    public $help_data_vydachi;
    public $help_vremya_vydachi;
    public $help_vrach_id;
    public $help_srok;
    public $help_zaklyuchenie;
    public $help_kolichestvo;
    public $help_firm_id;
    public $help_branche_id;
    public $help_oplata;
    public $honey_books_name;
    public $honey_books_data_rojdeniya;
    public $honey_books_city;
    public $honey_books_ulica;
    public $honey_books_home_id;
    public $honey_books_obekt;
    public $honey_books_role;
    public $store_in_nomer_zakaza;
    public $store_in_warehouse_id;
    public $store_in_status;
    public $store_in_create_at;
    public $store_in_the_supplier_id;
    public $store_in_vkladka;
    public $store_in_position;
    public $store_in_item_tovar_id;
    public $store_in_item_amount;
    public $store_in_item_prinyato;
    public $store_in_item_the_cost;
    public $store_in_item_store_in;
    public $store_in_item_mesto_na_sklade;
    public $store_remains_vkladka;
    public $store_remains_warehouse_id;
    public $store_remains_tovar_id;
    public $store_remains_amount;
    public $store_remains_price;
    public $store_remains_mesto_na_sklade;
    public $store_data_vkladka;
    public $store_salvage_warehouse_id;
    public $store_salvage_status;
    public $store_salvage_create_at;
    public $store_salvage_vkladka;
    public $store_salvage_position;
    public $store_salvage_item_tovar_id;
    public $store_salvage_item_mesto_na_sklade;
    public $store_salvage_item_amount;
    public $store_salvage_item_store_in;
  
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_name','patient_pol','patient_god_rojdeniya','patient_phone','patient_posledniy_vizit','patient_zaregistrirovan','patient_branche_id','help_test','help_qr','help_patient_id','help_type_id','help_create_at','help_user_id','help_agent_id','help_amounts','help_data_vzyatiya_biomateriala','help_vremya_vzyatiya_biomateriala','help_data_vydachi','help_vremya_vydachi','help_vrach_id','help_srok','help_zaklyuchenie','help_kolichestvo','help_firm_id','help_branche_id','help_oplata','honey_books_name','honey_books_data_rojdeniya','honey_books_city','honey_books_ulica','honey_books_home_id','honey_books_obekt','honey_books_role','store_in_nomer_zakaza','store_in_warehouse_id','store_in_status','store_in_create_at','store_in_the_supplier_id','store_in_vkladka','store_in_position','store_in_item_tovar_id','store_in_item_amount','store_in_item_prinyato','store_in_item_the_cost','store_in_item_store_in','store_in_item_mesto_na_sklade','store_remains_vkladka','store_remains_warehouse_id','store_remains_tovar_id','store_remains_amount','store_remains_price','store_remains_mesto_na_sklade','store_data_vkladka','store_salvage_warehouse_id','store_salvage_status','store_salvage_create_at','store_salvage_vkladka','store_salvage_position','store_salvage_item_tovar_id','store_salvage_item_mesto_na_sklade','store_salvage_item_amount','store_salvage_item_store_in', 
    ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting' => 'Колонки',
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->from('drivers');


        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
        ]);





        return $dataProvider;
    }

    public static function fieldLabels()
    {
        return [

        ];
    }

}