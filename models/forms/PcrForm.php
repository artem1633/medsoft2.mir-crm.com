<?php

namespace app\models\forms;

use yii\base\Model;

class PcrForm extends Model
{
	public $name;

	public $lastName;

	public $patronymic;

	public $fioLat;

	public $dateBirth;

	public $sex;

	public $dateTakeBio;

	public $timeTakeBio;

	public $dateOut;

	public $timeOut;

	public function rules()
	{
		return [
			[['name', 'lastName', 'patronymic', 'fioLat', 'dateBirth', 'sex', 'dateTakeBio', 'timeTakeBio', 'dateOut', 'timeOut'], 'safe'];
		];
	}

	public function attributeLabels()
	{
		'name' => 'Имя',
		'lastName' => 'Фамилия',
		'patronymic' => 'Отчество',
		'fioLat' => 'ФИО латиницей',
		'dateBirth' => '',
		'sex' => '',
		'dateTakeBio' => '',
		'timeTakeBio' => '',
		'dateOut' => '',
		'timeOut' => '',
	}
}
